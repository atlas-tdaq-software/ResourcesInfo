package ResourcesInfo;

  /**
   *  The <b>Utils</b> class is used to help a user of the ResourcesInfo package
   *  encoding parameters of the RESET command.
   *
   *  @author  <http://consult.cern.ch/xwho/people/432778>
   *  @since   online release 05-03-00
   */

public class Utils {

  /**
   *  Encode ResourcesInfoProvider RESET command argument.
   *
   *  Note, new line is used as field separator in encoded string.
   *
   *  @param timestamp   input timestamp (UTC nanoseconds since 1970)
   *  @param disable     set of resources to be disabled
   *  @param enable      set of resources to be enabled
   *  
   *  @return the encoded string
   */

  public static String encode_user_cmd(long timestamp, String[] disable, String[] enable)
    {
      StringBuilder sb = new StringBuilder();
      
      sb.append(String.valueOf(timestamp));
      sb.append('\n');

      vec2string(sb, disable);
      vec2string(sb, enable);

      return sb.toString();
    }

  
  private static void vec2string(StringBuilder sb, String[] v)
    {
      sb.append(String.valueOf(v.length));
      sb.append('\n');
      
      for(String x : v)
        {
          sb.append(x);
          sb.append('\n');
        }
    }

}
