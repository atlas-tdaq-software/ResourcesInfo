#include <set>
#include <string>
#include <vector>

#include <boost/program_options.hpp>

#include <ResourcesInfo/utils.h>

namespace po = boost::program_options;

int
main(int argc, char ** argv)
{
  try
    {
      po::options_description desc("resources info user command encoder test");

      std::vector<std::string> disable;
      std::vector<std::string> enable;
      uint64_t timestamp;

      desc.add_options()
        (
          "disable-ids,d",
          po::value<std::vector<std::string> >(&disable)->multitoken(),
          "IDs of objects to be disabled"
        )
        (
          "enable-ids,e",
          po::value<std::vector<std::string> >(&enable)->multitoken(),
          "IDs of objects to be enabled"
        )
        (
          "timestamp,t",
          po::value<uint64_t>(&timestamp)->required(),
          "Action Timestamp"
        )
        (
          "help,h",
          "Print help message"
        );

      po::variables_map vm;
      po::store(po::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      po::notify(vm);

      if (disable.empty() && enable.empty())
        {
          throw std::runtime_error("disable and enable parameters may not be empty simultaneously");
        }

      std::string result = daq::ResourcesInfo::encode_user_cmd(
          timestamp,
          std::set<std::string>(disable.begin(), disable.end()),
          std::set<std::string>(enable.begin(), enable.end()));

      std::cout << result;

      return 0;
    }
  catch (std::exception& ex)
    {
      std::cerr << "error: " << ex.what() << std::endl;
      return 1;
    }

  return 0;
}
