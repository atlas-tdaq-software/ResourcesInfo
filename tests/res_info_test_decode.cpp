#include <set>
#include <string>

#include <boost/program_options.hpp>

#include <ResourcesInfo/utils.h>

namespace po = boost::program_options;

int
main(int argc, char ** argv)
{
  try
    {
      po::options_description desc("resources info user command decoder test");

      std::string command;

      desc.add_options()
        (
          "command,c",
          po::value<std::string>(&command)->required(),
          "Sleep interval"
        )
        (
          "help,h",
          "Print help message"
        );

      po::variables_map vm;
      po::store(po::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      po::notify(vm);

      uint64_t timestamp(0);
      std::set<std::string> disable;
      std::set<std::string> enable;

      daq::ResourcesInfo::decode_user_cmd(command, timestamp, disable, enable);

      std::cout << "==> timestamp: " << timestamp << std::endl;
      std::cout << "==> disable resources:\n";
      for(auto & x : disable) std::cout << x << std::endl;
      std::cout << "==> enable resources:\n";
      for(auto & x : enable) std::cout << x << std::endl;

      return 0;
    }
  catch (std::exception& ex)
    {
      std::cerr << "error: " << ex.what() << std::endl;
      return 1;
    }

  return 0;
}
