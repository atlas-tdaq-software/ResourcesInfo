#! /bin/sh

################################################################################

timestamp=`date +'%s%N'`

enable=(e01 e02 e03)
disable=(d01 d02 d03 d04)

cmd="-t $timestamp"

cmd+=' -d'
for i in ${disable[@]}
do
  cmd+=" ${i}"
done

cmd+=' -e'
for i in ${enable[@]}
do
  cmd+=" ${i}"
done

################################################################################

echo '// check C++ encoder'
echo "\$ ${1}/res_info_test_encode ${cmd}"
res_cpp="`${1}/res_info_test_encode ${cmd}`"
if [ $? -eq 0 ]
then
  echo "${res_cpp}"
  echo '# OK (c++ encoder check passed)'
  echo ''
else
  echo "${res_cpp}"
  echo '# ERROR: c++ encoder check failed'
  exit 1
fi

################################################################################

echo '// check Java encoder'
echo "\$ $TDAQ_JAVA_HOME/bin/java -classpath $1/test_java_encoder.jar:$1/res-info-utils.jar:\$CLASSPATH test_res_info.TestCmdEncoder ${cmd}"
res_java=`$TDAQ_JAVA_HOME/bin/java -classpath $1/test_java_encoder.jar:$1/res-info-utils.jar:$CLASSPATH test_res_info.TestCmdEncoder ${cmd}`
if [ $? -eq 0 ]
then
  echo "${res_java}"
  echo '# OK (Java encoder check passed)'
  echo ''
else
  echo "${res_java}"
  echo '# ERROR: Java encoder check failed'
  exit 1
fi

################################################################################

if [ "${res_cpp}" != "${res_java}" ]
then
  echo '# ERROR: results of C++ and Java encoders are different'
  exit 1
else
  echo '# OK: results of C++ and Java encoders are equal'
  echo ''
fi

################################################################################

echo '// check C++ decoder'
echo "\$ ${1}/res_info_test_decode -c ${res_cpp}"
${1}/res_info_test_decode -c "${res_cpp}
"
if [ $? -eq 0 ]
then
  echo '# OK (c++ decoder check passed)'
  echo ''
else
  echo '# ERROR: c++ decoder check failed'
  exit 1
fi

################################################################################
