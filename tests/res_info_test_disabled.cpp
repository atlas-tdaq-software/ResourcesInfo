#include <signal.h>
#include <sys/resource.h>

#include "ResourcesInfo/utils.h"
#include <ers/ers.h>
#include <ipc/core.h>

#include <boost/program_options.hpp>

#include <config/ConfigObject.h>
#include <config/Configuration.h>

#include <dal/Partition.h>
#include <dal/Component.h>
#include <dal/util.h>

namespace po = boost::program_options;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::map<const daq::core::Component *,bool> state_info;  // keep information about enabled/disabled components

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

::Configuration * p_db(0);
const daq::core::Partition * partition(0);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern "C" void signal_handler(int sig)
{
  std::cout << "caught signal " << sig << std::endl;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static double
get_time_interval(const rusage * t1, const rusage * t2)
{
  return (t1->ru_utime.tv_sec - t2->ru_utime.tv_sec) + (t1->ru_utime.tv_usec - t2->ru_utime.tv_usec)/1000000.;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // this test only iterates known resources and check their new enabled/disabled status

void fast_test()
{
  struct rusage t1, t2;

  getrusage( RUSAGE_SELF, &t1 );

  unsigned long count(0);

  for(std::map<const daq::core::Component *,bool>::const_iterator i = state_info.begin(); i != state_info.end(); ++i) {
    if(i->first->disabled(*partition)) count++;
  }

  getrusage( RUSAGE_SELF, &t2 );

  std::cout << " * fast test found " << count << " disabled components (of " << state_info.size() << " total) in " << get_time_interval(&t2, &t1) << " seconds" << std::endl;
}


  // this test works much slower (~10 times) than above one, since it asks database about all resources (e.g. talks with RDB server),
  // performs extra search in relatively big map of all know resources and possibly updates it

void report_changes(bool check_removed)
{
  if(check_removed) {
    for(std::map<const daq::core::Component *,bool>::iterator i = state_info.begin(); i != state_info.end();) {
      if(i->first->config_object().is_deleted()) {
        std::cout << " * an object was deleted" << std::endl;
        state_info.erase(i++);
      }
      else {
        ++i;
      }
    }
  }

  std::vector<const daq::core::Component *> objs;

  p_db->get(objs);
    
  for(std::vector<const daq::core::Component *>::const_iterator i = objs.begin(); i != objs.end(); ++i) {
    bool result = (*i)->disabled(*partition);
    std::map<const daq::core::Component *,bool>::iterator j = state_info.find(*i);

    if(j == state_info.end()) {
      std::cout << " * insert new object " << *i << " in state " << result << std::endl;
      state_info[*i] = result;
    }
    else if(result != j->second) {
      std::cout << " * disabled(" << partition << ") on object " << *i << " returns " << result << std::endl;
      j->second = result;
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void db_cb(const std::vector<ConfigurationChange *> &, void*)
{
  std::cout << "The database was reloaded. Got the following changes:" << std::endl;
  report_changes(true);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct res_info_cb
{
   res_info_cb() {;}

   void operator()() {
     std::cout << "The online stopless recovery took place. Got the following changes:" << std::endl;
     fast_test();
     report_changes(false);
   }
};
/*
boost::function<void> res_info_cb()
{
  std::cout << "The online stopless recovery took place. Got the following changes:" << std::endl;
  fast_test();
  report_changes(false);
}
*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    // initialise IPC

  try {
    IPCCore::init(argc, argv);
  }
  catch(ers::Issue & ex) {
    ers::warning(ers::Message(ERS_HERE, ex));
  }


    // prepare command line parser

  po::options_description desc("test utility for Resource Info subscribe callback");

  std::string data;
  std::string partition_name;

  desc.add_options()
    (
      "data,d",
      po::value<std::string>(&data),
      "name of the database"
    )
    (
      "partition,p",
      po::value<std::string>(&partition_name)->required(),
      "name of the partition object"
    )
    (
      "help,h",
      "Print help message"
    );

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);

  if (vm.count("help"))
    {
      std::cout << desc << std::endl;
      return 0;
    }

  po::notify(vm);


    // work with configuration

  try {

    ::Configuration db(data);
    p_db = &db;

      // find partition and register variables converter
      // exit, if there is no partition object

    partition = daq::core::get_partition(db, partition_name);
    if(!partition) return 1;
    db.register_converter(new daq::core::SubstituteVariables(*partition));

    std::vector<const daq::core::Component *> objs;

    db.get(objs);
    
    std::cout << "Found " << objs.size() << " objects of classes derived from Component class." << std::endl;
    std::cout << std::boolalpha;

    for(std::vector<const daq::core::Component *>::const_iterator i = objs.begin(); i != objs.end(); ++i) {
      bool result = (*i)->disabled(*partition);
      ERS_DEBUG( 1, "disabled(" << partition << ") on object " << *i << " returns " << result );
      state_info[*i] = result;
    }

    signal(SIGINT,signal_handler);
    signal(SIGTERM,signal_handler);

      // subscribe online stopless recovery changes
    uint32_t id = daq::ResourcesInfo::subscribe(partition_name, db, *partition, res_info_cb());

      // subscribe database changes

    ::ConfigurationSubscriptionCriteria c;
    db.subscribe(c, db_cb, 0);


      // sleep

    try {
      pause();
    }
    catch (...) {
      ;
    }

    std::cout << "Exiting ..." << std::endl;

    db.unsubscribe();

    daq::ResourcesInfo::unsubscribe(id);
  }
  catch (ers::Issue & ex) {
    std::cerr << "Caught " << ex << std::endl;
    return (EXIT_FAILURE);
  }


  return 0;
}
