#ifndef UPDATERESOURCEINFO_H
#define UPDATERESOURCEINFO_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * An object of such class is updated, when all objects of InfoResource class are updated in result of RC stopless recovery action.
 * 
 * @author  generated by the IS tool
 * @version 18/11/13
 */

class UpdateResourceInfo : public ISInfo {
public:

    /**
     * Timestamp, when info resources were updated last time
     */
    uint64_t                      Timestamp;


    static const ISType & type() {
	static const ISType type_ = UpdateResourceInfo( ).ISInfo::type();
	return type_;
    }

    virtual std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "Timestamp: " << Timestamp << "\t// Timestamp, when info resources were updated last time";
	return out;
    }

    UpdateResourceInfo( )
      : ISInfo( "UpdateResourceInfo" )
    {
	initialize();
    }

    ~UpdateResourceInfo(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    UpdateResourceInfo( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << Timestamp;
    }

    void refreshGuts( ISistream & in ){
	in >> Timestamp;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const UpdateResourceInfo & info ) {
    info.print( out );
    return out;
}

#endif // UPDATERESOURCEINFO_H
