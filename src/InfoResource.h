#ifndef INFORESOURCE_H
#define INFORESOURCE_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * ID of resource enabled or disabled by RC stopless recovery.
 * 
 * @author  generated by the IS tool
 * @version 18/11/13
 */

class InfoResource : public ISInfo {
public:

    /**
     * Timestamp, when the resources was updated
     */
    uint64_t                      Timestamp;


    static const ISType & type() {
	static const ISType type_ = InfoResource( ).ISInfo::type();
	return type_;
    }

    virtual std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "Timestamp: " << Timestamp << "\t// Timestamp, when the resources was updated";
	return out;
    }

    InfoResource( )
      : ISInfo( "InfoResource" )
    {
	initialize();
    }

    ~InfoResource(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    InfoResource( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << Timestamp;
    }

    void refreshGuts( ISistream & in ){
	in >> Timestamp;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const InfoResource & info ) {
    info.print( out );
    return out;
}

#endif // INFORESOURCE_H
