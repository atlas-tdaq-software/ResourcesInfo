#include <stdlib.h>
#include <errno.h>

#include <stdexcept>
#include <iostream>
#include <string>
#include <cstdlib>
#include <limits>

#include <sys/resource.h>

#include <boost/program_options.hpp>
#include <boost/regex.hpp>

#include <boost/spirit/include/karma.hpp>

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/date_time/posix_time/time_parsers.hpp>
#include <boost/date_time/local_time/local_time.hpp>

#include <CoralBase/Attribute.h>
#include <CoralBase/AttributeList.h>
#include <CoralBase/AttributeSpecification.h>
#include <CoralBase/Exception.h>
#include "CoralKernel/Context.h"

#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/ISessionProxy.h"
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITransaction.h>
#include <RelationalAccess/ITableDescription.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/SchemaException.h>
#include <CoolKernel/FolderSpecification.h>
#include <CoolKernel/IFolderSet.h>
#include <CoolKernel/DatabaseId.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IObject.h>
#include <CoolKernel/IObjectIterator.h>
#include <CoolApplication/Application.h>

#include <ers/ers.h>

#include <oks/tz.h>
#include <rn/rn.h>

#include "ResourcesInfo/errors.h"
#include "ResourcesInfoUtils.h"

namespace po = boost::program_options;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));
boost::local_time::time_zone_ptr tz_ptr;
bool show_nanoseconds(false);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

enum ColorScheme
{
  TERM_color = 0, HTML_color = 1, NO_color
};

enum ColorValue
{
  RED = 0, GREEN = 1, BLACK = 2
};

const char * term_codes[] =
  { "31", "32", "30" };

const char * html_codes[] =
  { "darkred", "darkgreen", "black" };

static void
print_color_value(std::ostream& s, const std::string& val, ColorScheme scheme, ColorValue color)
{
  if (scheme == NO_color || color == BLACK)
    {
      s << val;
    }
  else if (scheme == TERM_color)
    {
      s << '\x1B' << "[0;" << term_codes[static_cast<int>(color)] << 'm' << val << '\x1B' << "[m";
    }
  else
    {
      s << "<span style=\"color: " << html_codes[static_cast<int>(color)] << ";\">" << val << "</span>";
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct TableRow
{

  char m_fill;
  char m_separator;

  std::string m_items[9];

  TableRow(const char fill, const char separator) :
      m_fill(fill), m_separator(separator)
  {
    ;
  }

  inline static void
  append2str(std::string& s, unsigned long i)
  {
    char buf[12] = ",000000000";
    char * ptr(buf);
    if      (i > 100000000) ptr = buf + 1;
    else if (i > 10000000)  ptr = buf + 2;
    else if (i > 1000000)   ptr = buf + 3;
    else if (i > 100000)    ptr = buf + 4;
    else if (i > 10000)     ptr = buf + 5;
    else if (i > 1000)      ptr = buf + 6;
    else if (i > 100)       ptr = buf + 7;
    else if (i > 10)        ptr = buf + 8;
    else                    ptr = buf + 9;
    boost::spirit::karma::generate(ptr, boost::spirit::ulong_, i);
    s.append(buf, 10);
  }

  static void
  key_2_ts_string(cool::ValidityKey key, std::string& v, ColorScheme scheme, ColorValue color)
  {
    const static std::string p_inf("+inf");

    if (key == cool::ValidityKeyMax)
      {
        v = p_inf;
        return;
      }

    std::string out;

      {
        boost::posix_time::ptime t = boost::posix_time::from_time_t(key / 1000000000);

        if (tz_ptr)
          {
            out = boost::local_time::local_date_time(t, tz_ptr).to_string();
          }
        else
          {
            out = boost::posix_time::to_simple_string(t);
          }

        if (show_nanoseconds)
          {
            uint64_t nsec = key % 1000000000;
            if (nsec)
              {
                append2str(out, nsec);
              }
          }
      }

      {
        std::ostringstream s;
        print_color_value(s, out, scheme, color);
        v = s.str();
      }
  }


  static void
  key_2_rnlb_string(cool::ValidityKey key, std::string& v, ColorScheme scheme, ColorValue color)
  {
    const static std::string p_inf("+inf");
    const static std::string p_null("");

    if (key == cool::ValidityKeyMax)
      {
        v = p_inf;
        return;
      }
    else if (key == 0)
      {
        v = p_null;
        return;

      }

    std::string out;

      {
        std::ostringstream s;
        uint32_t rn(key >> 32);
        uint32_t lb(key & 0xffffffff);
        s << '(' << rn << ':' << lb << ')';
        out = s.str();
      }

      {
        std::ostringstream s;
        print_color_value(s, out, scheme, color);
        v = s.str();
      }
  }

  TableRow(ColorScheme scheme, const char fill, const char separator, const std::string& p, const std::string& d, const std::string& f, const std::string& n, cool::ChannelId id, cool::ValidityKey vk_s, ColorValue vk_s_color, cool::ValidityKey vk_t,
      ColorValue vk_t_color, cool::ValidityKey vk_rnlb_s, cool::ValidityKey vk_rnlb_t) :
      m_fill(fill), m_separator(separator)
  {

    m_items[0] = p; // partition
    m_items[1] = d; // detector
    m_items[2] = f; // folder
    m_items[3] = n; // channel name
      {
        std::ostringstream s;
        s << id;
        m_items[4] = s.str();
      } // channel ID

    key_2_ts_string(vk_s, m_items[5], scheme, vk_s_color);
    key_2_ts_string(vk_t, m_items[6], scheme, vk_t_color);

    if (vk_rnlb_t != cool::ValidityKeyMin)
      {
        key_2_rnlb_string(vk_rnlb_s, m_items[7], scheme, vk_s_color);
        key_2_rnlb_string(vk_rnlb_t, m_items[8], scheme, vk_t_color);
      }
  }

  TableRow(const char fill, const char separator, const std::string& s1, const std::string& s2, const std::string& s3, const std::string& s4, const std::string& s5, const std::string& s6, const std::string& s7, const std::string& s8, const std::string& s9) :
      m_fill(fill), m_separator(separator)
  {
    m_items[0] = s1;
    m_items[1] = s2;
    m_items[2] = s3;
    m_items[3] = s4;
    m_items[4] = s5;
    m_items[5] = s6;
    m_items[6] = s7;
    m_items[7] = s8;
    m_items[8] = s9;
  }

};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void
parse_ts(const std::string& in, const char * what, uint64_t& value, std::string& iso_value, bool use_raw_data)
{
  if (use_raw_data == false)
    {
      boost::posix_time::ptime t = oks::tz::str_2_posix_time(in, tz_ptr, what);
      value = oks::tz::posix_time_2_to_ns(t);
      iso_value = oks::tz::posix_time_2_iso_string(t);
    }
  else
    {
      char * sanity;
      value = std::strtoull(in.c_str(), &sanity, 10);
      if (*sanity != 0 || errno == ERANGE)
        {
          std::ostringstream text;
          text << "function std::strtoull(\'" << in << "\') has failed for parameter --" << what;
          if (*sanity)
            text << " on unrecognized characters \'" << sanity << "\'";
          if (errno)
            text << " with code " << errno << ", reason = \'" << strerror(errno) << '\'';
          throw std::runtime_error(text.str().c_str());
        }

      boost::posix_time::ptime t = boost::posix_time::from_time_t(value / 1000000000);
      iso_value = oks::tz::posix_time_2_iso_string(t);
    }
}

static cool::ValidityKey
up_bound(std::map<cool::ValidityKey, cool::ValidityKey>& cvs_map, cool::ValidityKey key)
{
  std::map<cool::ValidityKey, cool::ValidityKey>::iterator x = cvs_map.find(key);
  if (x == cvs_map.end())
    {
      x = cvs_map.upper_bound(key);
    }
  return x->second;
}

static cool::ValidityKey
low_bound(std::map<cool::ValidityKey, cool::ValidityKey>& cvs_map, cool::ValidityKey key)
{
  std::map<cool::ValidityKey, cool::ValidityKey>::iterator x = cvs_map.find(key);
  if (x == cvs_map.end())
    {
      x = cvs_map.upper_bound(key);
      if (x != cvs_map.begin())
        --x;
    }
  return x->second;
}

int
main(int argc, char **argv)
{
  setenv("COOL_DISABLE_CORALCONNECTIONPOOLCLEANUP", "1", 0);

  // shared y different COOL databases
  cool::Application cool_app;

  // mapping of run-number:lumi-block IOVs to time-based IOVs and reversed
  std::map<cool::ValidityKey, cool::ValidityKey> lumi2ts;
  std::map<cool::ValidityKey, cool::ValidityKey> ts2lumi;
  std::set<uint32_t> run_numbers;

  // command line parameters
  std::string cool_db("COOLONL_TDAQ/CONDBR2");
  std::string cool_trigger_lb_db("COOLONL_TRIGGER/CONDBR2");
  std::string run_num_db_connect("RUN_NUMBER");
  std::string run_num_db_schema("atlas_run_number");
  std::string partition, detector, folder;
  uint64_t since(0), till(0);
  std::string since_iso, till_iso, tz;
  uint32_t since_rn(0), till_rn(0);
  uint32_t since_lb(0), till_lb(cool::UInt32Max);
  bool list_folders(false), force_full_db_scan(false), print_statistics(false), print_html_statistics(false), show_updated_only(false), no_table_alignment(false), use_raw_data(false), show_rn_lb_info(false);
  boost::regex * re(0);
  ColorScheme use_color(NO_color);
  std::string ts_since, ts_till;
  std::string color_updated;
  std::string channels;

  // in case of html statistics print all in one line at the end
  std::ostringstream html_stat_text;

  try
    {
      po::options_description desc("This program prints out contents of Resources Info COOL folders.");

      desc.add_options()
        ("res-info-db,i"  , po::value<std::string>(&cool_db)->default_value(cool_db)                       , "name of resources-info COOL database")
        ("lumi-block-db,m", po::value<std::string>(&cool_trigger_lb_db)->default_value(cool_trigger_lb_db) , "name of lumi-block COOL database (required if run-number / lumi-block is used)")
        ("rn-db-connect,c", po::value<std::string>(&run_num_db_connect)->default_value(run_num_db_connect) , "run-number db connect (required if run-number / lumi-block is used)")
        ("rn-db-schema,w" , po::value<std::string>(&run_num_db_schema)->default_value(run_num_db_schema)   , "run-number db working schema (required if run-number / lumi-block is used)")
        ("partition,p"    , po::value<std::string>(&partition)          , "name of partition (required except listing folders)")
        ("detector,d"     , po::value<std::string>(&detector)           , "name of detector")
        ("folder,f"       , po::value<std::string>(&folder)             , "name of detector folder")
        ("channel,n"      , po::value<std::string>(&channels)           , "name of channels (can be regular expression)")
        ("since,s"        , po::value<std::string>(&ts_since)           , "show channels containing objects with IoV since given timestamp")
        ("till,t"         , po::value<std::string>(&ts_till)            , "show channels containing objects with IoV till given timestamp")
        ("time-zone,z"    , po::value<std::string>(&tz)                 , "time zone name (run with \"-z list-regions\" to see all supported time zones)")
        ("use-raw-data,W"                                               , "input and output timestamps as number of seconds since Epoch")
        ("show-nanoseconds,N"                                           , "show nanoseconds in output")
        ("since-run,r"    , po::value<uint32_t>(&since_rn)              , "show channels containing objects with IoV since given run")
        ("till-run,R"     , po::value<uint32_t>(&till_rn)               , "show channels containing objects with IoV till given run")
        ("since-lb,b"     , po::value<uint32_t>(&since_lb)              , "luminosity block for run\'s since IoV (default 0)")
        ("till-lb,B"      , po::value<uint32_t>(&till_lb)               , "luminosity block for run\'s until IoV (default cool::UInt32Max)")
        ("show-run-number-lumi-block,U"                                 , "if possible, map timestamp on run-number and lumi-block (run-number and lumi-block DB are required)")
        ("force-full-run-number-lumi-block-database-read,F"             , "force full read of run number and lumi-block DBs to map any timestamp")
        ("show-updated-only,u"                                          , "only show channels updated for given IoV")
        ("color-updated,C", po::value<std::string>(&color_updated)      , "show in color channels updated for given IoV (values: \"term\", \"html\")")
        ("no-table-alignment,g"                                         , "do not align output table (also ON by -C \"html\" option)")
        ("list,l"                                                       , "list folders tree (database, partition, detector)")
        ("print-statistics,a"                                           , "print statistics info (processing time, numbers of folders, objects, etc.)")
        ("print-html-statistics,A"                                      , "print final statistics info for HTML parser")
        ("help,h"                                                       , "Print help message");

      po::variables_map vm;
      po::store(po::parse_command_line(argc, argv, desc), vm);
      po::notify(vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
        }

      if (vm.count("show-updated-only"))
        {
          show_updated_only = true;
        }

      if (vm.count("no-table-alignment"))
        {
          no_table_alignment = true;
        }

      if (vm.count("list"))
        {
          list_folders = true;
        }
      else if (vm.count("partition") == false)
        {
          throw std::runtime_error("Either --list or --partition parameter is required");
        }

      if (vm.count("use-raw-data"))
        {
          use_raw_data = true;
        }

      if (vm.count("show-nanoseconds"))
        {
          show_nanoseconds = true;
        }

      if (vm.count("show-run-number-lumi-block"))
        {
          show_rn_lb_info = true;
        }

      if (vm.count("print-statistics"))
        {
          print_statistics = true;
        }

      if (vm.count("print-html-statistics"))
        {
          print_html_statistics = true;
        }

      if (vm.count("force-full-run-number-lumi-block-database-read"))
        {
          lumi2ts[0] = 0;
          lumi2ts[cool::ValidityKeyMax] = cool::ValidityKeyMax;
          force_full_db_scan = true;
        }

      if (cool_db.empty())
        {
          throw std::runtime_error("Missing COOL db");
        }

      if (since_lb && !since_rn)
        {
          throw std::runtime_error("Parameter --since-lb cannot be used without --since-rn");
        }

      if (till_lb != cool::UInt32Max && !till_rn)
        {
          throw std::runtime_error("Parameter --till-lb cannot be used without --till-rn");
        }

      if ((since_rn > 0 || till_rn > 0 || show_rn_lb_info) && (cool_trigger_lb_db.empty() || run_num_db_connect.empty() || run_num_db_schema.empty()))
        {
          throw std::runtime_error("If --show-run-number-lumi-block, --since-rn or --till-rn is used, then --lumi-block-db, --rn-db-connect and --rn-db-schema may not be empty");
        }

      if (since_rn > 0 && since > 0)
        {
          throw std::runtime_error("--since-rn and --since may not be used simultaneously");
        }

      if (till_rn > 0 && till > 0)
        {
          throw std::runtime_error("--till-rn and --till may not be used simultaneously");
        }

      if (since_rn && till_rn && (since_rn > till_rn))
        {
          throw std::runtime_error("Parameter --since-rn is greater than --till-rn");
        }

      if (since_rn == till_rn && (since_lb > till_lb))
        {
          throw std::runtime_error("Parameter --since-lb is greater than --till-lb for equal since and till run numbers");
        }

      if (!tz.empty() && tz != "UTC")
        {
          if (use_raw_data)
            {
              throw std::runtime_error("Parameter --time-zone may not be used with --use-raw-data (forces UTC usage)");
            }

          try
            {
              oks::tz::DB tz_db;

              if (tz == "list-regions")
                {
                  for (auto & i : tz_db.get_regions())
                    {
                      std::cout << i << std::endl;
                    }
                  return 0;
                }

              tz_ptr = tz_db.get_tz_ptr(tz);
            }
          catch (const std::exception& ex)
            {
              ers::fatal(daq::ResourcesInfo::TimeZoneDBError(ERS_HERE, ex));
              return EXIT_FAILURE;
            }
        }

      if (!ts_since.empty())
        {
          parse_ts(ts_since, "enabled-since", since, since_iso, use_raw_data);
        }

      if (!ts_till.empty())
        {
          parse_ts(ts_till, "enabled-till", till, till_iso, use_raw_data);
        }
    }
  catch (std::exception& ex)
    {
      ers::fatal(daq::ResourcesInfo::CommandLineError(ERS_HERE, ex.what()));
      return EXIT_FAILURE;
    }

  try
    {
      if (since_rn > 0 || till_rn > 0 || show_rn_lb_info)
        {
          // read run-number database
            {
              std::string local_since_iso(since_iso), local_till_iso(till_iso);

              coral::ConnectionService * connection = 0;
              std::unique_ptr<coral::ISessionProxy> session(tdaq::RunNumber::get_session(run_num_db_connect, static_cast<int>(coral::ReadOnly), connection));

              coral::ITable& table = session->schema(run_num_db_schema).tableHandle(tdaq::RunNumber::s_table_name);

              ERS_DEBUG(0, "open run-number database \'" << run_num_db_connect << '\'');

              uint64_t rn_iovs_num(0); // number of IOVs read from run-number DB

              // calculate max-equal-or-less than since
              if (!since_iso.empty() || since_rn)
                {
                  struct rusage t1, t2;

                  getrusage( RUSAGE_SELF, &t1);

                  std::ostringstream log_text;

                  std::unique_ptr<coral::IQuery> query(table.newQuery());

                  std::string output_str(std::string("MAX(") + tdaq::RunNumber::s_start_at_column + ")");

                  query->addToOutputList(output_str);
                  query->defineOutputType(output_str, "string");
                  coral::AttributeList conditions;
                  std::string conditions_str;

                  if (partition.empty() == false)
                    {
                      conditions.extend<std::string>("prt");
                      conditions["prt"].data<std::string>() = partition;

                      conditions_str = tdaq::RunNumber::s_partition_name_column;
                      conditions_str.append(" = :prt");
                    }

                  if (since_rn)
                    {
                      conditions.extend<int>("rn");
                      conditions["rn"].data<int>() = since_rn;
                      if (conditions_str.empty() == false)
                        conditions_str.append(" AND ");
                      conditions_str.append(tdaq::RunNumber::s_run_number_column);
                      conditions_str.append(" < :rn");
                      log_text << since_rn;
                    }

                  if (!since_iso.empty())
                    {
                      conditions.extend<std::string>("ts");
                      conditions["ts"].data<std::string>() = since_iso;
                      if (conditions_str.empty() == false)
                        conditions_str.append(" AND ");
                      conditions_str.append(tdaq::RunNumber::s_start_at_column);
                      conditions_str.append(" < :ts");
                      log_text << '\"' << ts_since << '\"';
                    }

                  query->setCondition(conditions_str, conditions);

                  coral::ICursor& cursor = query->execute();

                  if (cursor.next() && !cursor.currentRow().begin()->isNull())
                    {
                      local_since_iso = cursor.currentRow().begin()->data<std::string>();

                      if (print_statistics)
                        {
                          getrusage( RUSAGE_SELF, &t2);

                          double interval((t2.ru_utime.tv_sec - t1.ru_utime.tv_sec) + (t2.ru_utime.tv_usec - t1.ru_utime.tv_usec) / 1000000.);

                          ERS_LOG("Run before " << log_text.str() << " in partition \"" << partition << "\" started at \"" << boost::posix_time::to_simple_string(boost::posix_time::from_iso_string(local_since_iso)) << "\" (read in " << interval << "\")");
                        }
                    }
                }

              // calculate min-equal-or-great than till
              if (!till_iso.empty() || till_rn)
                {
                  struct rusage t1, t2;

                  getrusage( RUSAGE_SELF, &t1);

                  std::ostringstream log_text;

                  std::unique_ptr<coral::IQuery> query(table.newQuery());

                  std::string output_str(std::string("MIN(") + tdaq::RunNumber::s_start_at_column + ")");

                  query->addToOutputList(output_str);
                  query->defineOutputType(output_str, "string");

                  coral::AttributeList conditions;
                  std::string conditions_str;

                  if (partition.empty() == false)
                    {
                      conditions.extend<std::string>("prt");
                      conditions["prt"].data<std::string>() = partition;

                      conditions_str = tdaq::RunNumber::s_partition_name_column;
                      conditions_str.append(" = :prt");
                    }

                  if (till_rn)
                    {
                      conditions.extend<int>("rn");
                      conditions["rn"].data<int>() = till_rn;
                      if (conditions_str.empty() == false)
                        conditions_str.append(" AND ");
                      conditions_str.append(tdaq::RunNumber::s_run_number_column);
                      conditions_str.append(" > :rn");
                      log_text << till_rn;
                    }

                  if (!till_iso.empty())
                    {
                      conditions.extend<std::string>("ts");
                      conditions["ts"].data<std::string>() = till_iso;
                      if (conditions_str.empty() == false)
                        conditions_str.append(" AND ");
                      conditions_str.append(tdaq::RunNumber::s_start_at_column);
                      conditions_str.append(" > :ts");
                      log_text << '\"' << till_iso << '\"';
                    }

                  query->setCondition(conditions_str, conditions);

                  coral::ICursor& cursor = query->execute();

                  if (cursor.next() && !cursor.currentRow().begin()->isNull())
                    {
                      local_till_iso = cursor.currentRow().begin()->data<std::string>();

                      if (print_statistics)
                        {
                          getrusage( RUSAGE_SELF, &t2);

                          double interval((t2.ru_utime.tv_sec - t1.ru_utime.tv_sec) + (t2.ru_utime.tv_usec - t1.ru_utime.tv_usec) / 1000000.);

                          ERS_LOG("Run after " << log_text.str() << " in partition \"" << partition << "\" started at \"" << boost::posix_time::to_simple_string(boost::posix_time::from_iso_string(local_till_iso)) << "\" (read in " << interval << "\")");
                        }
                    }
                }

              // read run-number database for given IoV (can be defined by time and rn/lb)
              struct rusage t1, t2;

              getrusage( RUSAGE_SELF, &t1);

              std::unique_ptr<coral::IQuery> query(table.newQuery());
              query->setRowCacheSize(10);

              std::string conditions_str;

              coral::AttributeList conditions;

              if (!partition.empty())
                {
                  conditions.extend<std::string>("prt");
                  conditions["prt"].data<std::string>() = partition;
                  conditions_str = (std::string(tdaq::RunNumber::s_partition_name_column) + " =" + " :prt");
                }

              if (!local_since_iso.empty() && force_full_db_scan == false)
                {
                  conditions.extend<std::string>("ss");
                  conditions["ss"].data<std::string>() = local_since_iso;
                  if (conditions_str.empty() == false)
                    conditions_str.append(" AND ");
                  conditions_str.append(std::string(tdaq::RunNumber::s_start_at_column));
                  conditions_str.append(" >= :ss");
                }

              if (!local_till_iso.empty() && force_full_db_scan == false)
                {
                  conditions.extend<std::string>("st");
                  conditions["st"].data<std::string>() = local_till_iso;
                  if (conditions_str.empty() == false)
                    conditions_str.append(" AND ");
                  conditions_str.append(std::string(tdaq::RunNumber::s_start_at_column));
                  conditions_str.append(" <= :st");
                }

              query->setCondition(conditions_str, conditions);

              coral::ICursor& c = query->execute();

              while (c.next())
                {
                  const coral::AttributeList& row = c.currentRow();

                  int run_number = row[tdaq::RunNumber::s_run_number_column].data<int>();
                  std::string started = row[tdaq::RunNumber::s_start_at_column].data<std::string>();
                  boost::posix_time::ptime t;

                  run_numbers.insert(run_number);
                  ERS_DEBUG(1, "insert run number " << run_number);

                  try
                    {
                      t = boost::posix_time::from_iso_string(started);
                    }
                  catch (std::exception& ex)
                    {
                      std::ostringstream text;
                      text << "cannot parse time \'" << started << "\': \"" << ex.what() << '\"';
                      throw std::runtime_error(text.str().c_str());
                    }

                  uint64_t value = (t - epoch).total_nanoseconds();
                  uint64_t key = daq::ResourcesInfo::mk_uint64_t(run_number, 0);

                  lumi2ts[key] = value;
                  ts2lumi[value] = key;
                  ERS_DEBUG(1, key << " (rn: " << run_number << ", lb: 0) => " << value);
                }

              rn_iovs_num = lumi2ts.size();

              if (rn_iovs_num == 0)
                {
                  throw std::runtime_error("found no data in run number database; check selection parameters");
                }

              if (print_statistics || print_html_statistics)
                {
                  getrusage( RUSAGE_SELF, &t2);

                  double interval((t2.ru_utime.tv_sec - t1.ru_utime.tv_sec) + (t2.ru_utime.tv_usec - t1.ru_utime.tv_usec) / 1000000.);

                  std::ostringstream text;

                  text << "Read " << rn_iovs_num << " IOVs from run number database in " << interval << '\"';

                  if (print_statistics)
                    {
                      ERS_LOG(text.str());
                    }
                  else
                    {
                      html_stat_text << text.str() << "<br>";
                    }
                }
            }

          // read trigger DB
            {
              struct rusage t1, t2;

              getrusage( RUSAGE_SELF, &t1);

              cool::IDatabasePtr db;

              const std::string lumi_db_root("/TRIGGER/LUMI/LBLB");

              try
                {
                  db = cool_app.databaseService().openDatabase(cool_trigger_lb_db, true);
                  ERS_DEBUG(0, "open lumi-block database \'" << cool_trigger_lb_db << '\'');
                }
              catch (cool::DatabaseDoesNotExist& ex)
                {
                  std::ostringstream text;
                  text << "database \'" << cool_trigger_lb_db << "\' does not exist";
                  throw std::runtime_error(text.str().c_str());
                }

              if (db->existsFolder(lumi_db_root) == false)
                {
                  std::ostringstream text;
                  text << "database \'" << cool_trigger_lb_db << "\' has no folder \'" << lumi_db_root << "\'";
                  throw std::runtime_error(text.str().c_str());
                }

              cool::IFolderPtr folder = db->getFolder(lumi_db_root);

              cool::ValidityKey skey(lumi2ts.begin()->first);
              cool::ValidityKey tkey(lumi2ts.rbegin()->first | 0xffffffff);

              uint64_t rn_iovs_num = lumi2ts.size();

              cool::IObjectIteratorPtr p = folder->browseObjects(skey, tkey, 0);

              while (p->goToNext())
                {
                  const cool::IObject& obj(p->currentRef());
                  cool::ValidityKey k(obj.since());
                  cool::ValidityKey v(obj.payload()["StartTime"].data<cool::ValidityKey>());

                  uint32_t rn = (k >> 32);

                  if (run_numbers.find(rn) == run_numbers.end())
                    {
                      ERS_DEBUG(1, "skip " << k << " (wrong run number " << rn << ')');
                      continue;
                    }

                  lumi2ts[k] = v;
                  ts2lumi[v] = k;
                  ERS_DEBUG(1, k << " (rn: " << rn << ", lb: " << (k&0xffffffff) << ") => " << v);
                }

              if (print_statistics || print_html_statistics)
                {
                  getrusage( RUSAGE_SELF, &t2);

                  double interval((t2.ru_utime.tv_sec - t1.ru_utime.tv_sec) + (t2.ru_utime.tv_usec - t1.ru_utime.tv_usec) / 1000000.);

                  std::ostringstream text;

                  text << "Read " << (lumi2ts.size() - rn_iovs_num) << " IOVs in lumi-block database for [" << (skey >> 32) << ':' << (skey & 0xffffffff) << ',' << (tkey >> 32) << ':' << (tkey & 0xffffffff) << "] interval in " << interval << '\"';

                  if (print_statistics)
                    {
                      ERS_LOG(text.str());
                    }
                  else
                    {
                      html_stat_text << text.str() << "<br>";
                    }
                }
            }

          lumi2ts[0] = 0;
          lumi2ts[cool::ValidityKeyMax] = cool::ValidityKeyMax;

          ts2lumi[0] = 0;
          ts2lumi[cool::ValidityKeyMax] = cool::ValidityKeyMax;
        }

      if (!channels.empty())
        {
          try
            {
              re = new boost::regex(channels);
            }
          catch (std::exception& ex)
            {
              std::ostringstream text;
              text << "ERROR: cannot create regular expression from string \'" << channels << "\':\n" << ex.what() << std::ends;
              throw std::runtime_error(text.str().c_str());
            }
        }

      if (!color_updated.empty())
        {
          if (color_updated == "term")
            {
              use_color = TERM_color;
            }
          else if (color_updated == "html")
            {
              use_color = HTML_color;
              no_table_alignment = true;
            }
          else
            {
              std::ostringstream text;
              text << "ERROR: wrong value \'" << color_updated << "\' for --color-updated option; allowed values are \"term\" and \"html\"" << std::ends;
              throw std::runtime_error(text.str().c_str());
            }
        }
    }
  catch (std::exception& ex)
    {
      ers::fatal(daq::ResourcesInfo::ApplicationError(ERS_HERE, ex.what()));
      return EXIT_FAILURE;
    }

  // open COOL database

  cool::IDatabasePtr db;

  try
    {
      db = cool_app.databaseService().openDatabase(cool_db);
      ERS_DEBUG(0, "open COOL database \'" << cool_db << '\'');
    }
  catch (std::exception& ex)
    {
      ers::fatal(daq::ResourcesInfo::CoolError(ERS_HERE, ex.what()));
      return EXIT_FAILURE;
    }

  // several variables for statistics

  unsigned int folders_count(0), channels_count(0), total_objects_count(0), objects_count(0);
  struct rusage t1, t2;

  // fill folders structure

  std::map<std::string, std::map<std::string, std::set<std::string> > > folders; // partition[detector[folder]]

  getrusage( RUSAGE_SELF, &t1);

  // read partition folder sets

  if (partition.empty())
    {
      std::string root_folder_name(daq::ResourcesInfo::RootFolder);
      root_folder_name.erase(root_folder_name.size() - 1);

      try
        {
          ERS_DEBUG(1, "read folder sets of \'" << root_folder_name << '\'');
          cool::IFolderSetPtr fs_ptr = db->getFolderSet(root_folder_name);
          std::vector<std::string> fs = fs_ptr->listFolderSets();
          for (std::vector<std::string>::const_iterator i = fs.begin(); i != fs.end(); ++i)
            {
              folders[(*i)] = std::map<std::string, std::set<std::string> >();
            }
        }
      catch (std::exception& ex)
        {
          std::ostringstream text;
          text << "getFolderSet(\"" << root_folder_name << "\") has failed";
          ers::fatal(daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str(), ex));
          return EXIT_FAILURE;
        }
    }
  else
    {
      std::string base_name(daq::ResourcesInfo::RootFolder + partition);

      try
        {
          if (!db->existsFolderSet(base_name))
            {
              std::ostringstream text;
              text << "folder set " << base_name << "\" does not exist (possibly command line parameter partition=\"" << partition << "\" is wrong)";
              ers::fatal(daq::ResourcesInfo::CommandLineError(ERS_HERE, text.str().c_str()));
              return EXIT_FAILURE;
            }
        }
      catch (std::exception& ex)
        {
          std::ostringstream text;
          text << "existsFolderSet(\"" << base_name << "\") has failed";
          ers::fatal(daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str(), ex));
          return EXIT_FAILURE;
        }

      folders[base_name] = std::map<std::string, std::set<std::string> >();
    }

  // read detector folder sets

  for (auto& i : folders)
    {
      std::map<std::string, std::set<std::string> >& pf(i.second);

      if (detector.empty())
        {

          try
            {
              ERS_DEBUG(1, "read folder sets of \'" << i.first << '\'');
              cool::IFolderSetPtr fs_ptr = db->getFolderSet(i.first);
              std::vector<std::string> fs = fs_ptr->listFolderSets();
              for (const auto& j : fs)
                {
                  pf[j] = std::set<std::string>();
                }
            }
          catch (std::exception& ex)
            {
              std::ostringstream text;
              text << "getFolderSet(\"" << i.first << "\") has failed";
              ers::fatal(daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str(), ex));
              return EXIT_FAILURE;
            }
        }
      else
        {
          std::string df_name(i.first + '/' + detector);

          try
            {
              if (!db->existsFolderSet(df_name))
                {
                  std::ostringstream text;
                  text << "folder set " << df_name << "\" does not exist (possibly command line parameter detector=\"" << detector << "\" is wrong)";
                  ers::fatal(daq::ResourcesInfo::CommandLineError(ERS_HERE, text.str().c_str()));
                  return EXIT_FAILURE;
                }
            }
          catch (std::exception& ex)
            {
              std::ostringstream text;
              text << "existsFolderSet(\"" << df_name << "\") has failed";
              ers::fatal(daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str(), ex));
              return EXIT_FAILURE;
            }

          pf[df_name] = std::set<std::string>();
        }
    }

  // read detector folders

  for (auto& i : folders)
    {
      for (auto& j : i.second)
        {

          std::set<std::string>& df(j.second);

          if (folder.empty())
            {

              try
                {
                  ERS_DEBUG(1, "read folders of \'" << j.first << '\'');
                  cool::IFolderSetPtr fs_ptr = db->getFolderSet(j.first);
                  std::vector<std::string> fs = fs_ptr->listFolders();
                  for (std::vector<std::string>::const_iterator x = fs.begin(); x != fs.end(); ++x)
                    {
                      df.insert(*x);
                    }
                }
              catch (std::exception& ex)
                {
                  std::ostringstream text;
                  text << "getFolderSet(\"" << j.first << "\") has failed";
                  ers::fatal(daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str(), ex));
                  return EXIT_FAILURE;
                }
            }
          else
            {
              std::string df_name(j.first + '/' + folder);

              try
                {
                  if (!db->existsFolder(df_name))
                    {
                      if (!detector.empty())
                        {
                          std::ostringstream text;
                          text << "folder " << df_name << "\" does not exist (possibly command line parameter detector=\"" << detector << "\" and folder=\"" << folder << "\" is wrong)";
                          ers::fatal(daq::ResourcesInfo::CommandLineError(ERS_HERE, text.str().c_str()));
                          return EXIT_FAILURE;
                        }
                      else
                        {
                          ERS_DEBUG(1, "folder " << df_name << "\" does not exist");
                        }
                    }
                  else
                    {
                      df.insert(df_name);
                    }
                }
              catch (std::exception& ex)
                {
                  std::ostringstream text;
                  text << "existsFolder(\"" << df_name << "\") has failed";
                  ers::fatal(daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str(), ex));
                  return EXIT_FAILURE;
                }
            }
        }
    }

  if (list_folders)
    {
      for (const auto& i : folders)
        {
          if (!detector.empty() && i.second.empty())
            continue;

          std::cout << i.first.substr(daq::ResourcesInfo::RootFolder.size()) << std::endl;

          for (const auto& j : i.second)
            {
              if (!folder.empty() && j.second.empty())
                continue;

              std::cout << "  " << j.first.substr(i.first.size() + 1) << std::endl;

              for (const auto& x : j.second)
                {
                  std::cout << "    " << x.substr(j.first.size() + 1) << std::endl;
                }
            }
        }

      return EXIT_SUCCESS;
    }

  std::vector<TableRow> rows;

  // fill the table

  const char * since_column_name;
  const char * until_column_name;

  if (use_raw_data == false)
    {
      if (tz_ptr)
        {
          since_column_name = "Since (local time)";
          until_column_name = "Until (local time)";
        }
      else
        {
          since_column_name = "Since (UTC)";
          until_column_name = "Until (UTC)";
        }
    }
  else
    {
      since_column_name = "Since (ns since Epoch)";
      until_column_name = "Until (ns since Epoch)";
    }

  rows.push_back(TableRow('=', '='));
  rows.push_back(TableRow(' ', '|', "Partition", "Detector", "Folder", "Channel Name", "ID", since_column_name, until_column_name, "Since (RN:LB)", "Until (RN:LB)"));
  rows.push_back(TableRow('=', '='));

  cool::ValidityKey skey(since), tkey(till ? till : cool::ValidityKeyMax);

  if (since_rn)
    {
      uint64_t key = daq::ResourcesInfo::mk_uint64_t(since_rn, since_lb);
      ERS_DEBUG(0, "since key: " << key << " (" << since_rn << "," << since_lb << ')');
      skey = low_bound(lumi2ts, key);
    }

  if (till_rn)
    {
      uint64_t key = daq::ResourcesInfo::mk_uint64_t(till_rn, till_lb);
      ERS_DEBUG(0, "till key: " << key << " (" << till_rn << "," << till_lb << ')');
      tkey = up_bound(lumi2ts, key) - 1;
    }

  cool::ChannelSelection all;

  bool printed_obj(false);

  // search objects

  for (const auto& i : folders)
    {
      std::string pname(i.first.substr(daq::ResourcesInfo::RootFolder.size()));
      for (const auto& j : i.second)
        {
          std::string dname(j.first.substr(i.first.size() + 1));
          for (const auto& x : j.second)
            {
              std::string fname(x.substr(j.first.size() + 1));
              cool::IFolderPtr folder;

              try
                {
                  folder = db->getFolder(x);
                  folders_count++;
                }
              catch (std::exception& ex)
                {
                  std::ostringstream text;
                  text << "COOL getFolder(" << x << ") failed";
                  ers::fatal(daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str(), ex));
                  return EXIT_FAILURE;
                }

              cool::IObjectIteratorPtr p;

              try
                {
                  p = folder->browseObjects(skey, tkey, all);
                }
              catch (std::exception& ex)
                {
                  std::ostringstream text;
                  text << "COOL browseObjects(ValidityKeys: since=" << skey << ", till=" << tkey << ") for folder " << x << " failed";
                  ers::fatal(daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str(), ex));
                  return EXIT_FAILURE;
                }

              std::map<cool::ChannelId, std::string> channels_id2name;

              try
                {
                  channels_id2name = folder->listChannelsWithNames();
                }
              catch (std::exception& ex)
                {
                  std::ostringstream text;
                  text << "COOL listChannelsWithNames(" << x << ") failed";
                  ers::fatal(daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str(), ex));
                  return EXIT_FAILURE;
                }

              cool::ChannelId last_id(0);
              const std::string * last_name(0);

              while (p->goToNext())
                {
                  const cool::IObject& obj(p->currentRef());
                  total_objects_count++;

                  if (last_id != obj.channelId())
                    {
                      channels_count++;
                      last_id = obj.channelId();
                      last_name = &channels_id2name.find(last_id)->second;
                    }

                  if (re)
                    {
                      if (boost::regex_match(*last_name, *re) == false)
                        {
                          ERS_DEBUG(2, "skip channel " << *last_name << " (unmatched regex)");
                          continue;
                        }
                    }

                  if (show_updated_only)
                    {
                      if (((obj.since() > skey && obj.since() < tkey) || (obj.until() > skey && obj.until() < tkey)) == false)
                        {
                          ERS_DEBUG(2, "skip channel " << *last_name << " with IoV [" << obj.since() << ',' << obj.until() << "[ out of show updated only in interval [" << skey << ',' << tkey << '[');
                          continue;
                        }
                    }

                  objects_count++;

                  ColorValue s_cv(BLACK), t_cv(BLACK);

                  if (use_color != NO_color)
                    {
                      if (obj.since() >= skey)
                        s_cv = GREEN;

                      if (obj.until() <= tkey)
                        t_cv = RED;
                    }

                  cool::ValidityKey rnlb_s(cool::ValidityKeyMin);
                  cool::ValidityKey rnlb_t(cool::ValidityKeyMin);

                  if (show_rn_lb_info)
                    {
                      rnlb_s = low_bound(ts2lumi, obj.since());
                      rnlb_t = up_bound(ts2lumi, obj.until());
                    }

                  rows.push_back(TableRow(use_color, ' ', '|', pname, dname, fname, *last_name, last_id, obj.since(), s_cv, obj.until(), t_cv, rnlb_s, rnlb_t));
                  printed_obj = true;
                }

              if (printed_obj)
                {
                  printed_obj = false;
                  rows.push_back(TableRow('-', '+'));
                }
            }

          if (printed_obj)
            {
              printed_obj = false;
              rows.push_back(TableRow('-', '+'));
            }
        }

      if (printed_obj)
        {
          printed_obj = false;
          rows.push_back(TableRow('-', '+'));
        }
    }

  rows.pop_back();

  rows.push_back(TableRow('=', '='));

  getrusage( RUSAGE_SELF, &t2);

  const unsigned short num_of_columns(show_rn_lb_info ? 9 : 7);

  // detect columns widths

  unsigned short cw[] =
    { 1, 1, 1, 1, 1, 1, 1, 1, 1 };

  if (!no_table_alignment)
    {
      for (auto & i : rows)
        {
          for (unsigned short j = 0; j < num_of_columns; ++j)
            {
              const std::string& v(i.m_items[j]);
              unsigned short len = v.size();
              if (len > 10 && v[0] == '\x1B' && v[1] == '[')
                len -= 10;
              if (len > cw[j])
                cw[j] = len;
            }
        }
    }

  // print table

    {
      bool align_left[] =
        { true, true, true, true, false, false, false, false, false };

      for (auto & i : rows)
        {
          for (unsigned short j = 0; j < num_of_columns; ++j)
            {
              const std::string& v(i.m_items[j]);

              if (!no_table_alignment)
                {
                  unsigned int dx = cw[j];
                  unsigned int len = v.size();
                  if (len > 10 && v[0] == '\x1B' && v[1] == '[')
                    dx += 10;
                  dx -= len;

                  std::cout << i.m_separator << i.m_fill;

                  if (align_left[j])
                    {
                      std::cout << v;
                      while (dx-- > 0)
                        std::cout << i.m_fill;
                    }
                  else
                    {
                      while (dx-- > 0)
                        std::cout << i.m_fill;
                      std::cout << v;
                    }
                  std::cout << i.m_fill;
                }
              else
                {
                  std::cout << i.m_separator << v;
                }
            }
          std::cout << i.m_separator << '\n';
        }

      std::cout.flush();
    }

  if (print_statistics || print_html_statistics)
    {
      double interval((t2.ru_utime.tv_sec - t1.ru_utime.tv_sec) + (t2.ru_utime.tv_usec - t1.ru_utime.tv_usec) / 1000000.);

      std::ostringstream text;

      text << "Got " << objects_count << " objects ";
      if (total_objects_count != objects_count)
        text << "(of " << total_objects_count << ") ";
      text << "from " << channels_count << " channels stored on " << folders_count << " folders in " << interval << "\"\n";

      if (print_statistics)
        {
          ERS_LOG(text.str());
        }
      else
        {
          std::cout << html_stat_text.str() << text.str();
        }
    }

  return EXIT_SUCCESS;
}
