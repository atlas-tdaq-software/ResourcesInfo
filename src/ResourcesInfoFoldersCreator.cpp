#include <stdexcept>
#include <iostream>
#include <string>


#include <boost/program_options.hpp>

#include <CoolKernel/FolderSpecification.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolApplication/Application.h>

#include <ers/ers.h>
#include <ipc/core.h>

#include <config/Configuration.h>

#include <dal/Partition.h>
#include <dal/ResourcesInfoConfig.h>
#include <dal/ResourcesInfoDetectorConfig.h>
#include <dal/ResourcesInfoDetectorFolderConfig.h>
#include <dal/util.h>

#include "ResourcesInfo/errors.h"
#include "ResourcesInfoUtils.h"

namespace po = boost::program_options;


static void
add_folders(
  std::map< std::string , std::set<std::string> >& folders,
  const daq::core::ResourcesInfoConfig& config,
  const daq::core::ResourcesInfoDetectorConfig& det_config
)
{
  std::set<std::string>& sub_folders = folders[det_config.get_FolderName()];

  for(std::vector<const daq::core::ResourcesInfoDetectorFolderConfig*>::const_iterator j = config.get_DefaultDetectorSubFolders().begin(); j != config.get_DefaultDetectorSubFolders().end(); ++j) {
    sub_folders.insert((*j)->get_FolderName());
    ERS_DEBUG(1, "read config: add default sub-folder \"" << (*j)->get_FolderName() << "\" to \"" << det_config.get_FolderName() << '\"');
  }

            // insert detector-specific folders
  for(std::vector<const daq::core::ResourcesInfoDetectorFolderConfig*>::const_iterator j = det_config.get_SubFolders().begin(); j != det_config.get_SubFolders().end(); ++j) {
    sub_folders.insert((*j)->get_FolderName());
    ERS_DEBUG(1, "read config: add detector sub-folder \"" << (*j)->get_FolderName() << "\" to \"" << det_config.get_FolderName() << '\"');
  }
}


int main (int argc, char **argv)
{
    // initialise IPC which probably will be used for rdbconfig plugin

  try {
    IPCCore::init(argc, argv);
  }
  catch(ers::Issue & ex) {
    ers::warning(ers::Message(ERS_HERE, ex));
  }


    // parse command line

  std::string cool_db, data, pname;

  try {
    po::options_description desc("This program creates COOL folders used by Resources Info archiver.");

    desc.add_options()
      ("cool-db,c"         , po::value<std::string>(&cool_db)  , "name of COOL database")
      ("data,d"            , po::value<std::string>(&data)     , "name of the database (ignore TDAQ_DB variable)")
      ("partition-name,p"  , po::value<std::string>(&pname)    , "name of the partition object")
      ("help,h"                                                , "Print help message")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if(vm.count("help")) {
      std::cout << desc << std::endl;
      return EXIT_SUCCESS;
    }

    if(pname.empty()) {
      throw std::runtime_error("Missing partition name" );
    }

    if(cool_db.empty()) {
      throw std::runtime_error("Missing COOL db" );
    }
  }
  catch(std::exception& ex) {
    ers::fatal(daq::ResourcesInfo::CommandLineError(ERS_HERE, ex.what()));
    return EXIT_FAILURE;
  }


    // read configuration information

  std::map< std::string , std::set<std::string> > folders;

  try {
    Configuration db(data);
    if(const daq::core::Partition * p = daq::core::get_partition(db, pname)) {
      db.register_converter(new daq::core::SubstituteVariables(*p));
      if(const daq::core::ResourcesInfoConfig * ric = p->get_ResourcesInfoConfiguration()) {
        for(std::vector<const daq::core::ResourcesInfoDetectorConfig*>::const_iterator i = ric->get_DetectorFolders().begin(); i != ric->get_DetectorFolders().end(); ++i) {
          add_folders(folders, *ric, **i);
        }

        add_folders(folders, *ric, *ric->get_GlobalFolder());
      }
      else {
        std::ostringstream text;
        text << "partition \'" << pname << "\' has no ResourcesInfoConfiguration";
        ers::fatal(daq::ResourcesInfo::ConfigError(ERS_HERE, text.str().c_str()));
        return EXIT_FAILURE;
      }
    }
    else {
      std::ostringstream text;
      text << "cannot find partition \'" << pname << "\' in \'" << db.get_impl_spec() << '\'';
      ers::fatal(daq::ResourcesInfo::ConfigError(ERS_HERE, text.str().c_str()));
      return EXIT_FAILURE;
    }
  }
  catch(ers::Issue& ex) {
    ers::fatal(daq::ResourcesInfo::ConfigError(ERS_HERE, "exception", ex));
    return EXIT_FAILURE;
  }
  catch(std::exception& ex) {
    ers::fatal(daq::ResourcesInfo::ConfigError(ERS_HERE, ex.what()));
    return EXIT_FAILURE;
  }


    // create COOL database if needed

  cool::IDatabasePtr db;
  cool::Application cool_app;
  
  bool db_exists(false);

  try {
    ERS_DEBUG( 1 , "try to open COOL database \'" << cool_db << '\'' );
    db = cool_app.databaseService().openDatabase( cool_db, false );
    db_exists = true;
  }
  catch(cool::DatabaseDoesNotExist& ex) {
    ERS_DEBUG( 1, "COOL database \'" << cool_db << "\' does not exist");
  }
  catch(std::exception& ex) {
    ers::fatal(daq::ResourcesInfo::CoolError(ERS_HERE, ex.what()));
    return EXIT_FAILURE;
  }


    // create database, folder-sets, and detector folders if needed

  try {
    if(!db_exists) {
      ERS_LOG("creating COOL database \'" << cool_db << "\'...");
      db = cool_app.databaseService().createDatabase( cool_db );
    }

    std::string base_name(daq::ResourcesInfo::RootFolder + pname);

    if(!db->existsFolderSet(base_name)) {
      ERS_LOG("creating folder set \'" << base_name << "\'...");
      std::ostringstream description;
      description << "active resources for partition " << pname;
      db->createFolderSet(base_name, description.str(), true);
    }

    for(std::map< std::string , std::set<std::string> >::const_iterator i = folders.begin(); i != folders.end(); ++i) {
      std::string detector_base_name(base_name + '/' + i->first);

      if(!db->existsFolderSet(detector_base_name)) {
        ERS_LOG("creating folder set \'" << detector_base_name << "\'...");
        std::ostringstream description;
        description << "active resources for detector " << i->first << " from partition " << pname;
        db->createFolderSet(detector_base_name, description.str(), true);
      }

      for(std::set<std::string>::const_iterator j = i->second.begin(); j != i->second.end(); ++j) {
        std::string folder_name(detector_base_name + '/' + *j);
        bool existsFolder;

        try {
          ERS_DEBUG( 1 , "check folder \'" << folder_name << "\' existence" );
          existsFolder = db->existsFolder( folder_name );
        }
        catch(std::exception& ex) {
          std::ostringstream text;
          text << "COOL existsFolder(\"" << folder_name << "\") failed: " << ex.what();
          throw std::runtime_error( text.str().c_str() );
        }

        if(!existsFolder) {
          try {
            cool::RecordSpecification payloadSpec;
            payloadSpec.extend( "class", cool::StorageType::String255 );
            cool::FolderSpecification folderSpec( cool::FolderVersioning::SINGLE_VERSION, payloadSpec );
            ERS_LOG("creating folder \'" << folder_name << "\'...");
            db->createFolder(
              folder_name ,
              folderSpec ,
              daq::ResourcesInfo::FolderDescription
            );
          }
          catch(std::exception& ex) {
            std::ostringstream text;
            text << "COOL createFolder(\"" << folder_name << "\") failed: " << ex.what();
            throw std::runtime_error( text.str().c_str() );
          }
        }
      }
    }
  }
  catch(std::exception& ex) {
    ers::fatal(daq::ResourcesInfo::CoolError(ERS_HERE, ex.what()));
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
