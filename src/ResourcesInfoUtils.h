#ifndef RESOURCES_INFO_UTILS_H_INCLUDED
#define RESOURCES_INFO_UTILS_H_INCLUDED

#include <string.h>
#include <stdint.h>
#include <errno.h>

#include <set>
#include <string>
#include <sstream>

namespace daq
{
  namespace ResourcesInfo
  {

    // Name of root folder in COOL
    static const std::string RootFolder("/TDAQ/Resources/");

    static const std::string FolderDescription(
        "<timeStamp>time</timeStamp>"
        "<addrHeader><address_header service_type=\"71\" clid=\"1238547719\"/></addrHeader>"
        "<typeName>CondAttrListCollection</typeName>"
        "<named/>");

    // Prefixes for information published in IS
    static const std::string s_enabled_resoures("Resources.Enabled:");
    static const std::string s_stopless_recovery_enabled_resoures("Resources.StoplessRecoveryEnabled:");
    static const std::string s_stopless_recovery_disabled_resoures("Resources.StoplessRecoveryDisabled:");
    static const std::string s_stopless_recovery_updated_resoures("Resources.StoplessRecoveryUpdated");

    inline uint64_t
    mk_uint64_t(uint32_t rn, uint32_t lb)
    {
      return ((uint64_t) rn << 32) + (uint64_t) lb;
    }

    inline void
    add_stopless_recovery_name(const std::string &prefix, std::string::size_type prefix_len, const std::string &value, std::set<std::string> &s)
    {
      if (value.find(prefix) == 0)
        {
          if (prefix_len)
            {
              std::string name(value, prefix_len);
              s.insert(name);
            }
          else
            {
              s.insert(value);
            }
        }
    }

    inline std::string
    ex2text(const std::exception &ex)
    {
      std::ostringstream text;

      text << ex.what();
      if (errno)
        text << " (error code: " << errno << ", reason: " << strerror(errno) << ')';

      return text.str();
    }

  }
}

#endif
