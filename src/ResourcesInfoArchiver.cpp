#include <dirent.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <set>
#include <map>

#include <fstream>
#include <stdexcept>
#include <sstream>

#include <boost/program_options.hpp>
#include <boost/interprocess/sync/file_lock.hpp>

#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IFolderSet.h>
#include <CoolKernel/IObject.h>
#include <CoolKernel/IObjectIterator.h>
#include <CoolKernel/Record.h>
#include <CoolApplication/Application.h>

#include <system/Host.h>
#include <system/User.h>

#include <ipc/core.h>

#include <ers/ers.h>

#include "ResourcesInfo/errors.h"
#include "ResourcesInfoUtils.h"


////////////////////////////////////////////////////////////////////////////////////////////////////

namespace daq {
  namespace ResourcesInfo {

    class Archiver {
      public:

        Archiver(const std::string& dir, unsigned long timeout);
        ~Archiver() { unlock_dir(); }

        void check_db_open(const std::string& connect = "");
        void run();


      public:

        static bool s_interrupted;


      private:

        std::string p_dir;
        std::string p_dir_lock_file;
        boost::interprocess::file_lock p_lock;
        cool::IDatabasePtr p_db;
        cool::Application p_cool_app;

        unsigned long p_idle_count;
        unsigned long p_idle_timeout; // in 10 seconds (e.g. value 6 => 1 minute, 360 => 1 hour, etc.)


      private:

        void archive_file(const std::string& file);

        void check_db_close();
        void reset_idle_counter() { p_idle_count = 0; }

        void lock_dir();
        void unlock_dir();

        void read_file(const std::string& file, std::string& partition, uint64_t& timestamp, std::map<std::string, std::map<std::string, std::map<std::string,std::string> > >& resources);
        void get_folders(const std::string& partition, std::map< std::string , std::set<std::string> >& existing_folders);

    };
  }
}


bool daq::ResourcesInfo::Archiver::s_interrupted = false;

////////////////////////////////////////////////////////////////////////////////////////////////////

extern "C" void
signal_handler(int num)
{
  std::cout << "got signal " << num << ", stopping..." << std::endl; 
  daq::ResourcesInfo::Archiver::s_interrupted = true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void daq::ResourcesInfo::Archiver::lock_dir()
{
  ERS_LOG( "locking directory \"" << p_dir_lock_file << "\"..." );

    {
      std::ifstream f(p_dir_lock_file.c_str());

      if (f)
        {
          std::string text;
          std::getline(f, text);

          boost::interprocess::file_lock lock(p_dir_lock_file.c_str());

          if (lock.try_lock() == false)
            {
              std::ostringstream s;
              s << "the directory is already locked by \'" << text << '\'';
              throw daq::ResourcesInfo::DirectoryLockError(ERS_HERE, "lock", p_dir, s.str());
            }
          else
            {
              try
                {
                  lock.unlock();
                }
              catch(std::exception& ex)
                {
                  throw daq::ResourcesInfo::CannotUnlockFile(ERS_HERE, p_dir_lock_file, ex);
                }

              ers::warning(daq::ResourcesInfo::RemoveObsoleteLock(ERS_HERE, p_dir_lock_file, text));

              if(unlink(p_dir_lock_file.c_str()) != 0)
                {
                  throw daq::ResourcesInfo::CannotRemoveLockFile(ERS_HERE, p_dir_lock_file, errno, strerror(errno));
                }

            }
        }
    }

  try
    {
      System::User myself;
      std::ofstream f(p_dir_lock_file.c_str());
      f.exceptions(std::istream::eofbit | std::istream::failbit | std::istream::badbit);
      f << "process " << getpid() << " on " << System::LocalHost::instance()->full_local_name() << " started by " << myself.name_safe() << " (" << RES_INFO_VERSION << " version from " << RELEASE_VERSION << ')';
      f.close();
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "cannot write to file \"" << p_dir_lock_file << "\" : " << daq::ResourcesInfo::ex2text(ex);
      throw daq::ResourcesInfo::DirectoryLockError(ERS_HERE, "lock", p_dir, text.str());
    }

  try
    {
      p_lock = boost::interprocess::file_lock(p_dir_lock_file.c_str());

      if (p_lock.try_lock() == false)
        {
          throw std::runtime_error("try_lock() failed");
        }
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "cannot lock file \"" << p_dir_lock_file << "\" : " << ex.what();
      throw daq::ResourcesInfo::DirectoryLockError(ERS_HERE, "lock", p_dir, text.str());
    }

}

void daq::ResourcesInfo::Archiver::unlock_dir()
{
  ERS_LOG( "unlocking directory \"" << p_dir_lock_file << "\"..." );

  try
    {
      p_lock.unlock();
    }
  catch(const std::exception& ex)
    {
      ers::error(daq::ResourcesInfo::CannotUnlockFile(ERS_HERE, p_dir_lock_file, ex));
    }

  errno = 0;
  if(int status = unlink(p_dir_lock_file.c_str())) {
    std::ostringstream text;
    text << "cannot remove directory lock file since unlink(\'" << p_dir_lock_file << "\') failed with code " << status << ": " << strerror(errno);
    ers::error(daq::ResourcesInfo::DirectoryLockError(ERS_HERE, "unlock", p_dir, text.str()));
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void daq::ResourcesInfo::Archiver::check_db_close()
{
  if(p_idle_count++ > p_idle_timeout) {
    try {
      if(!p_db->isOpen()) {
        ERS_DEBUG( 1 , "COOL database is open" );
        return;
      }
    }
    catch(std::exception& ex) {
      std::ostringstream text;
      text << "COOL database isOpen() failed: " << ex.what();
      throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
    }

    ERS_LOG("Close COOL database because of inactivity timeout ...");

    try {
      p_db->closeDatabase();
    }
    catch(std::exception& ex) {
      std::ostringstream text;
      text << "COOL database closeDatabase() failed: " << ex.what();
      ers::error(daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str()));
    }
  }
}

void daq::ResourcesInfo::Archiver::check_db_open(const std::string& connect)
{
  if(p_db.get()) {
    try {
      if(p_db->isOpen()) {
        ERS_DEBUG( 1 , "COOL database is open" );
        return;
      }
    }
    catch(std::exception& ex) {
      std::ostringstream text;
      text << "COOL database isOpen() failed: " << ex.what();
      throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
    }

    try {
      ERS_LOG("re-open COOL database ...");
      p_db->openDatabase();
      reset_idle_counter();
    }
    catch(std::exception& ex) {
      std::ostringstream text;
      text << "COOL openDatabase() failed: " << ex.what();
      throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
    }
  }
  else {
    try {
      ERS_LOG("open COOL database \'" << connect << "\' ...");
      p_db = p_cool_app.databaseService().openDatabase( connect, false );
      reset_idle_counter();
    }
    catch(std::exception& ex) {
      std::ostringstream text;
      text << "failed to open COOL database \'" << connect << "\' : " << ex.what();
      throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
    }
  }
}


////////////////////////////////////////////////////////////////////////////////////////////////////

daq::ResourcesInfo::Archiver::Archiver(const std::string& dir, unsigned long timeout)
{
  p_idle_timeout = timeout * 6;

  ERS_DEBUG( 1, "Set p_idle_timeout counter = " << p_idle_timeout );

    // validate directory
  {
    char resolved_name[PATH_MAX];

    if(realpath(dir.c_str(), resolved_name) == 0) {
      std::ostringstream text;
      text << "realpath(\'" << dir << "\') has failed with code " << errno << ": \'" << strerror(errno) << '\'';
      throw daq::ResourcesInfo::FileSystemError(ERS_HERE, "test", "directory", dir.c_str(), text.str().c_str());
    }

    p_dir = resolved_name;

    if(p_dir != dir) {
      ERS_LOG("The \'" << dir << "\' is non-fully-qualified path. Will use \'" << p_dir << "\' instead.");
    }

    p_dir_lock_file = p_dir + '/' + ".lock";

    lock_dir();
  }
}

void
daq::ResourcesInfo::Archiver::run()
{

  std::set<std::string> bad_names;       // bad names of files, which cannot be parsed
  std::set<uint64_t> bad_files;          // bad files (no partition object, non-oks file, etc.)
  std::set<uint64_t> bad_files_pre;      // firts attempt to mark file as "bad": sometimes NFS shows file as empty and sync it later
  std::set<uint64_t> unremovable_files;  // file has been archived but cannot be removed
  std::set<uint64_t> problematic_files;  // cannot archive (e.g. require new base version, more space; to be re-tested with low frequency)

  const unsigned int sleep_interval = 10;   // sleep this amount of seconds before reading directory next time
  const unsigned int low_frequency = 180;   // test problemitic files once per given value (e.g. 10" x 180 = 30')

  unsigned int count = 0;

  while(!s_interrupted) {
    count++;

    errno = 0;
    if(DIR * dir = opendir(p_dir.c_str())) {
      std::set<uint64_t> files;

      for(struct dirent * d = readdir(dir); d != 0; d = readdir(dir)) {
        std::string test(d->d_name);
        
        if(test.size() < 10) {
	  ERS_DEBUG(2, "skip \"" << test << "\" (file name is shorter 10 characters)");
	  continue;
        }

        if(test[0] == '.') {
	  ERS_DEBUG(2, "skip \"" << test << "\" (temporal file)");
	  continue;
        }

	if(bad_names.find(test) != bad_names.end()) {
	  ERS_DEBUG(2, "skip file with bad name \"" << test << '\"');
	  continue;
	}

        uint64_t val(0L);

        {
          char * sanity;
          errno = 0;
          val = static_cast<uint64_t>(strtoull(test.c_str(), &sanity, 0));
          if(*sanity != 0 || errno == ERANGE) {
            std::ostringstream text;
            text << "function strtoull(\'" << test << "\') has failed";
            if(*sanity) text << " on unrecognized characters \'" << sanity << "\'";
            if(errno) text << " with code " << errno << ", reason = \'" << strerror(errno) << '\'';
            ers::error(daq::ResourcesInfo::BadFileName(ERS_HERE, test.c_str(), text.str().c_str()));
            bad_names.insert(test);
	    continue;
          }
        }


	if(bad_files.find(val) != bad_files.end()) {
	  ERS_DEBUG(2, "skip file with bad contents \"" << test << '\"');
	  continue;
	}

	if(unremovable_files.find(val) != unremovable_files.end()) {
	  ERS_DEBUG(2, "skip unremovable file \"" << test << "\" (it was archived already)");
	  continue;
	}

	if(problematic_files.find(val) != problematic_files.end()) {
	  if((count%low_frequency) == 0) {
	    ERS_DEBUG(2, "add problematic file \"" << test << "\" (counter = " << count << ')');
	  }
	  else {
	    ERS_DEBUG(2, "skip problematic file \"" << test << '\"');
	    continue;
	  }
	}

        files.insert(val);
      }

      closedir(dir);

      for(std::set<uint64_t>::const_iterator i = files.begin(); (i != files.end()) && !s_interrupted; ++i) {
        try {
          ERS_LOG("Trying to archive file \"" << *i << "\"...");
          std::ostringstream the_file;
          the_file << p_dir << '/' << *i;
	  archive_file(the_file.str());
	  ERS_INFO("File \'" << the_file.str() << "\' has been archived");

          errno = 0;
	  if(unlink(the_file.str().c_str())) {
            std::ostringstream text;
            text << "unlink() function failed: \'" << strerror(errno) << '\'';
            ers::error(daq::ResourcesInfo::CannotRemoveFile(ERS_HERE, the_file.str().c_str(), text.str().c_str()));
	    unremovable_files.insert(*i);
	  }
	}
	catch(daq::ResourcesInfo::BadFile & e) {
	  ers::error(e);
	  if(bad_files_pre.find(*i) == bad_files_pre.end()) {
	    bad_files_pre.insert(*i);
	    ERS_LOG("Once ignore bad file \"" << *i << '\"');
	  }
	  else {
	    bad_files.insert(*i);
	    ERS_LOG("Contents of file \'" << *i << "\' is bad; ignore it in future");
	  }
	}
	catch(daq::ResourcesInfo::FileArchivationFailed & e) {
	  ers::error(e);
	  problematic_files.insert(*i);
	  ERS_LOG("Cannot archive file \'" << *i << "\'; try again later (wait " << (sleep_interval * low_frequency) << " seconds)");
	}
      }
    }
    else {
      std::ostringstream text;
      text << "opendir() function failed: \'" << strerror(errno) << '\'';
      ers::error(daq::ResourcesInfo::FileSystemError(ERS_HERE, "read", "directory", p_dir.c_str(), text.str().c_str()));
    }

    check_db_close(); // close database connection, if there is no activity during pre-defined timeout

    sleep(sleep_interval);
  }
}


////////////////////////////////////////////////////////////////////////////////////////////////////

void
daq::ResourcesInfo::Archiver::read_file(
  const std::string& file,
  std::string& partition,
  uint64_t& timestamp,
  std::map<std::string, std::map<std::string, std::map<std::string,std::string> > >& resources
)
{
  std::map<std::string, std::map<std::string,std::string> > * detector_ptr(0);
  std::map<std::string,std::string> * folder_ptr(0);

  std::ifstream f(file.c_str());

  if(!f) {
    throw daq::ResourcesInfo::BadFile(ERS_HERE, file.c_str(), "cannot open file for reading");
  }

  f >> timestamp >> partition;

  if(!timestamp) {
    throw daq::ResourcesInfo::BadFile(ERS_HERE, file.c_str(), "failed to read timestamp");
  }

  if(partition.empty()) {
    throw daq::ResourcesInfo::BadFile(ERS_HERE, file.c_str(), "failed to read partition name");
  }

  std::string s;

  std::string detector;
  std::string folder;

  unsigned long line(4);

  char buf[2048];
  char *val(buf + 1);

  while(f.good() && !f.getline(buf, sizeof(buf), '\n').eof()) {
    char c(buf[0]);

    if(c == '#') {
      detector = val;
      folder.clear();
      detector_ptr = &resources[detector];
      folder_ptr = 0;
    }
    else if(c == '$') {
      if(detector.empty() || !detector_ptr) {
        std::ostringstream text;
        text << "no detector defined for subfolder " << buf << " on line " << line;
        throw daq::ResourcesInfo::BadFile(ERS_HERE, file.c_str(), text.str().c_str());
      }
      folder = val;
      folder_ptr = &(*detector_ptr)[folder];
    }
    else if(c == '-') {
      if(detector.empty() || !detector_ptr) {
        std::ostringstream text;
        text << "no detector defined for resource " << buf << " on line " << line;
        throw daq::ResourcesInfo::BadFile(ERS_HERE, file.c_str(), text.str().c_str());
      }
      if(folder.empty() || !folder_ptr) {
        std::ostringstream text;
        text << "no folder defined for resource " << buf << " on line " << line;
        throw daq::ResourcesInfo::BadFile(ERS_HERE, file.c_str(), text.str().c_str());
      }
      
      if(char * class_name = strchr(val, '@')) {
        class_name[0] = 0; class_name++;
        (*folder_ptr)[val]=class_name;
      }
      else {
        std::ostringstream text;
        text << "bad resource format " << buf << " (no \'@\' separator) on line " << line;
        throw daq::ResourcesInfo::BadFile(ERS_HERE, file.c_str(), text.str().c_str());
      }
    }
    else if(!c) {
      ERS_DEBUG( 2 , "skip empty line " << line);
    }
    else {
      std::ostringstream text;
      text << "bad input \'" << buf << "\' on line " << line;
      throw daq::ResourcesInfo::BadFile(ERS_HERE, file.c_str(), text.str().c_str());
    }

    line++;
  }
}

void
daq::ResourcesInfo::Archiver::get_folders(
  const std::string& partition,
  std::map< std::string , std::set<std::string> >& cool_folders
)
{
        // read detectors of partition

  std::string partition_folder_set_name(daq::ResourcesInfo::RootFolder + partition);
  cool::IFolderSetPtr part_folder_set;

  try {
    ERS_DEBUG( 1, "get partition folder set " << partition_folder_set_name << " ...");
    part_folder_set = p_db->getFolderSet( partition_folder_set_name );
  }
  catch(std::exception& ex) {
    std::ostringstream text;
    text << "COOL getFolderSet(" << partition_folder_set_name << ") failed: " << ex.what();
    throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
  }

  std::vector<std::string> detectors;

  try {
    ERS_DEBUG( 1, "get detector folder sets ...");
    detectors = part_folder_set->listFolderSets();
  }
  catch(std::exception& ex) {
    std::ostringstream text;
    text << "COOL listFolderSets(" << partition_folder_set_name << ") failed: " << ex.what();
    throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
  }


    // read folders of detectors

  for(std::vector<std::string>::const_iterator i = detectors.begin(); i != detectors.end(); ++i) {
    cool::IFolderSetPtr det_folder_set;

    try {
      ERS_DEBUG( 1, "get partition folder set " << *i << " ...");
      det_folder_set = p_db->getFolderSet( *i );
    }
    catch(std::exception& ex) {
      std::ostringstream text;
      text << "COOL getFolderSet(" << *i << ") failed: " << ex.what();
      throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
    }

    std::vector<std::string> det_folders;

    try {
      ERS_DEBUG( 1, "get detector " << *i << " folders ...");
      det_folders = det_folder_set->listFolders();
    }
    catch(std::exception& ex) {
      std::ostringstream text;
      text << "COOL listFolders(" << *i << ") failed: " << ex.what();
      throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
    }


    std::string detector_name = (*i).substr((*i).rfind('/')+1);
    std::set<std::string>& dset(cool_folders[detector_name]);
    for(std::vector<std::string>::const_iterator j = det_folders.begin(); j != det_folders.end(); ++j) {
      std::string folder_name = (*j).substr((*j).rfind('/')+1);
      dset.insert(folder_name);
      ERS_DEBUG( 2 , "add " << folder_name << " to " << detector_name );
    }
  }
}


  // archive list of files

void
daq::ResourcesInfo::Archiver::archive_file(const std::string& file)
{
    // data to be archived

  std::string partition;
  uint64_t timestamp(0);
  std::map<std::string, std::map<std::string, std::map<std::string,std::string> > > resources;  // detectors -> folders -> resources (id -> class)


    // read data from file

  read_file(file, partition, timestamp, resources);


    // print what data were read from file, if needed

  if(ers::debug_level() >= 2) {
    std::ostringstream s;
    s <<
      "timestamp  : \'" << timestamp << "\'\n"
      "partition  : \'" << partition << "\'\n";

    for(std::map<std::string, std::map<std::string, std::map<std::string,std::string> > >::const_iterator i = resources.begin(); i != resources.end(); ++i) {
      s << "# detector " << i->first << std::endl;
      for(std::map<std::string, std::map<std::string,std::string> >::const_iterator j = i->second.begin(); j != i->second.end(); ++j) {
        s << "  $ folder " << j->first << std::endl;
        for(std::map<std::string,std::string>::const_iterator x = j->second.begin(); x != j->second.end(); ++x) {
          s << "    - resource " << x->first << " from class " << x->second << std::endl;
        }
      }
    }

    ERS_DEBUG( 2, "Read from file \'" << file << "\':\n" << s.str() );
  }


    // check COOL database

  try {
    check_db_open();
  }
  catch(daq::ResourcesInfo::Exception& ex) {
    throw daq::ResourcesInfo::FileArchivationFailed(ERS_HERE, file.c_str(), timestamp, partition.c_str(), "cannot open COOL database", ex);
  }


    // now we try to read COOL database, reset the counter

  reset_idle_counter();


    // get folders existing in COOL for given partition

  std::map< std::string , std::set<std::string> > existing_folders;  // detector -> folders

  try {
    get_folders(partition, existing_folders);
  }
  catch(ers::Issue& ex) {
    throw daq::ResourcesInfo::FileArchivationFailed(ERS_HERE, file.c_str(), timestamp, partition.c_str(), "got COOL error", ex);
  }


    // print what folders were read from COOL, if needed

  if(ers::debug_level() >= 2) {
    std::ostringstream s;

    for(std::map< std::string , std::set<std::string> >::const_iterator i = existing_folders.begin(); i != existing_folders.end(); ++i) {
      s << " * detector " << i->first << " has " << i->second.size() << " subfolder(s):\n";
      for(std::set<std::string>::const_iterator j = i->second.begin(); j != i->second.end(); ++j) {
        s << "   - " << *j << std::endl;
      }
    }

    ERS_DEBUG( 2, "Read existing folders from COOL for partition \'" << partition << "\':\n" << s.str() );
  }



    // process resources

  cool::ValidityKey key(timestamp);
  cool::ChannelSelection all;

  try {
    for(std::map< std::string , std::set<std::string> >::const_iterator i = existing_folders.begin(); i != existing_folders.end(); ++i) {

          // find this detector in new data
      std::map<std::string, std::map<std::string,std::string> > * detector_data(0);

      {
        std::map<std::string, std::map<std::string, std::map<std::string,std::string> > >::iterator x = resources.find(i->first);
        if(x != resources.end()) detector_data = &x->second;
      }

      for(std::set<std::string>::const_iterator j = i->second.begin(); j != i->second.end(); ++j) {
        std::string short_folder_name(partition + '/' + i->first + '/' + *j);
        std::string folder_name(daq::ResourcesInfo::RootFolder + short_folder_name);

          // find this folder in new data

        std::map<std::string,std::string> * folder_data(0);

        if(detector_data) {
          std::map<std::string, std::map<std::string,std::string> >::iterator x = detector_data->find(*j);
          if(x != detector_data->end()) folder_data = &x->second;
        }

        cool::IFolderPtr folder;

        try {
          folder = p_db->getFolder( folder_name );
        }
        catch(std::exception& ex) {
          std::ostringstream text;
          text << "COOL getFolder(" << folder_name << ") failed: " << ex.what();
          throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
        }

        std::map<cool::ChannelId,std::string> channels_id2name;

        try {
          channels_id2name = folder->listChannelsWithNames();
        }
        catch(std::exception& ex) {
          std::ostringstream text;
          text << "COOL listChannelsWithNames(" << folder_name << ") failed: " << ex.what();
          throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
        }


        cool::IObjectIteratorPtr p;

        try {
          p = folder->findObjects(key, all);
        }
        catch(std::exception& ex) {
          std::ostringstream text;
          text << "COOL findObjects(ValidityKey: " << key << ") for folder " << folder_name << " failed: " << ex.what();
          throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
        }

        while ( p->goToNext() ) {
          const cool::IObject& obj(p->currentRef());
          std::map<cool::ChannelId,std::string>::iterator x = channels_id2name.find(obj.channelId());
          if(x == channels_id2name.end()) {
            std::ostringstream text;
            text << "cannot find name for channel " << obj.channelId() << " from folder " << folder_name;
            throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
          }

          bool truncate_iov(false);

          if(!folder_data) {
            truncate_iov = true;
            ERS_DEBUG( 1 , "truncate IoV of resource " << x->second << " from folder \"" << short_folder_name << "\": there is no such folder in active resources");
          }
          else {
            std::map<std::string,std::string>::iterator y = folder_data->find(x->second);

            if(y == folder_data->end()) {
              truncate_iov = true;
              ERS_DEBUG( 1 , "truncate IoV of resource " << x->second << " from folder \"" << short_folder_name << "\": there is no such active resource");
            }
            else {
              folder_data->erase(y);
              ERS_DEBUG( 1 , "erase resource " << x->second << " from folder \"" << short_folder_name << "\": there is no need to add / re-open such resource");
            }
          }

          if(truncate_iov) {
            ERS_LOG("Resource " << x->second << " from folder \"" << short_folder_name << "\" is not active for key " << key << ", closing IoV ...");
            try {
              cool::ChannelSelection rx(obj.channelId());
              folder->truncateObjectValidity(key, rx);
            }
            catch(std::exception& ex) {
              std::ostringstream text;
              text << "COOL truncateObjectValidity(folder:" << folder_name << ", object: " << &obj << ", key: " << key << ") failed: " << ex.what();
              throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
            }
          }
        }


          // check, if there are resources to be re-enabled or created

        if(folder_data && !folder_data->empty()) {
          std::map<std::string,cool::ChannelId> channels_name2id;
          for(std::map<cool::ChannelId,std::string>::const_iterator x = channels_id2name.begin(); x != channels_id2name.end(); ++x) {
            channels_name2id[x->second] = x->first;
          }

          cool::ChannelId new_idx = 1;

          {
            std::map<cool::ChannelId,std::string>::reverse_iterator x = channels_id2name.rbegin();
            if(x != channels_id2name.rend()) new_idx = x->first + 1;
          }

          ERS_DEBUG( 1 , "new calculated channel ID for folder \"" << short_folder_name << " is " << new_idx);

          const cool::IRecordSpecification& payloadSpec(folder->payloadSpecification());
          cool::Record payload( payloadSpec );

          for(std::map<std::string,std::string>::iterator x = folder_data->begin(); x != folder_data->end(); ++x) {
            std::map<std::string,cool::ChannelId>::const_iterator y = channels_name2id.find(x->first);

            cool::ChannelId id(0);

            if(y == channels_name2id.end()) {
              id = new_idx++;
              ERS_LOG("Create new channel " << id << " for resource " << x->first << " in folder \"" << short_folder_name << "\" and store resource with IoV since " << key << " ...");
              try {
                folder->createChannel(id, x->first);
              }
              catch(std::exception& ex) {
                std::ostringstream text;
                text << "COOL createChannel(folder:" << folder_name << ", id: " << id << ", name: " << x->first << ") failed: " << ex.what();
                throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
              }
            }
            else {
              id = y->second;
              ERS_LOG("Store new resource " << x->first << " in channel " << id << " with IoV since " << key << " in folder \"" << short_folder_name << "\" ...");
            }

            payload["class"].setValue( x->second );
            folder->storeObject( key, cool::ValidityKeyMax, payload, id );
          }
          
          folder_data->clear();
        }
      }
    }

      // error check: all resources have to be removed by one or another way; left ones indicate an error

    {
      std::ostringstream text;
      text << "There are resources left in folder(s) not defined in partition \"" << partition << "\":\n";
    
      bool found_errors(false);
      for(std::map<std::string, std::map<std::string, std::map<std::string,std::string> > >::iterator i = resources.begin(); i != resources.end(); ++i) {
        for(std::map<std::string, std::map<std::string,std::string> >::iterator j = i->second.begin(); j != i->second.end(); ++j) {
          if(!j->second.empty()) {
            text << " - \"" << i->first + '/' + j->first << "\"\n";
            found_errors = true;
          }
        }
      }

      if(found_errors) {
        text << "Possibly ResourcesInfo configuration was updated.\nIn such case run res_info_create_db with schema owner account.";
        throw daq::ResourcesInfo::CoolError(ERS_HERE, text.str().c_str());
      }
    }
  }
  catch(ers::Issue& ex) {
    throw daq::ResourcesInfo::FileArchivationFailed(ERS_HERE, file.c_str(), timestamp, partition.c_str(), "got COOL error", ex);
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace po = boost::program_options;

int main(int argc, char *argv[])
{

    // initialize IPC core to report ERS messages on MRS

  try {
    IPCCore::init(argc, argv);
  }
  catch(ers::Issue & ex) {
    ers::warning(ers::Message(ERS_HERE, ex));
  }


    // parse command line

  std::string cool_db, dir;
  unsigned long idle = 180; // default timeout is 3 hours

  try {
    po::options_description desc(
      "This program archives information about active resources on COOL. "
      "The input files are created by the res_info_provider processes."
    );

    desc.add_options()
      ("cool-db,c"         , po::value<std::string>(&cool_db)  , "name of COOL database")
      ("dir,d"             , po::value<std::string>(&dir)      , "name of directory with files to be archived")
      ("idle-timeout,t"    , po::value<unsigned long>(&idle)   , "close COOL database connection after given period of inactivity (in minutes)")
      ("help,h"                                                , "Print help message")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if(vm.count("help")) {
      std::cout << desc << std::endl;
      return EXIT_SUCCESS;
    }

    if(dir.empty()) {
      throw std::runtime_error("Missing directory name" );
    }

    if(cool_db.empty()) {
      throw std::runtime_error("Missing COOL db" );
    }

    if(idle < 1) {
      ers::warning(daq::ResourcesInfo::CommandLineError(ERS_HERE, "the idle-timeout cannot be 0; reset the timeout = 1 minute"));
      idle = 1;
    }
    else if (idle >= 60 * 48) {
      ers::warning(daq::ResourcesInfo::CommandLineError(ERS_HERE, "the idle-timeout is too big; reset the timeout = 2 days"));
      idle = 60 * 48;
    }
  }
  catch(std::exception& ex) {
    ers::fatal(daq::ResourcesInfo::CommandLineError(ERS_HERE, ex.what()));
    return EXIT_FAILURE;
  }


    // register handlers for user's signals

  signal(SIGINT,signal_handler);
  signal(SIGTERM,signal_handler);

    // run archiving

  try {

    ERS_LOG("Starting res_info_archiver...");

    daq::ResourcesInfo::Archiver obj(dir, idle);

    // validate COOL database
    obj.check_db_open(cool_db);

    obj.run();

    ERS_LOG("Exiting res_info_archiver...");

  }

  catch ( ers::Issue& ex ) {
    ers::fatal(daq::ResourcesInfo::ApplicationError(ERS_HERE, "", ex));
    return EXIT_FAILURE;
  }

  catch ( std::exception& ex ) {
    ers::fatal(daq::ResourcesInfo::ApplicationError(ERS_HERE, ex.what()));
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
