#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <algorithm>
#include <fstream>
#include <set>
#include <string>
#include <stdexcept>

#include <boost/program_options.hpp>

#include <ers/ers.h>

#include <dal/util.h>
#include <dal/Detector.h>
#include <dal/Partition.h>
#include <dal/Segment.h>
#include <dal/ResourceSet.h>
#include <dal/ResourceBase.h>
#include <dal/ResourcesInfoConfig.h>
#include <dal/ResourcesInfoDetectorConfig.h>
#include <dal/ResourcesInfoDetectorFolderConfig.h>
#include <DFdal/ROS.h>
#include <DFdal/InputChannel.h>

#include <ipc/core.h>

#include <is/infodictionary.h>
#include <is/infoiterator.h>

#include <eformat/SourceIdentifier.h>

#include <rc/RunParams.h>
#include <rc/RCStateInfo.h>
#include <RunControl/RunControl.h>

#include "ResourcesInfo/utils.h"
#include "ResourcesInfo/errors.h"
#include "ResourcesInfoUtils.h"
#include "EnabledResource.h"
#include "InfoResource.h"
#include "UpdateResourceInfo.h"


namespace daq {
  namespace ResourcesInfo {

    class Provider: public daq::rc::Controllable
    {

      public:

        Provider(const std::string& dir, bool restore_from_backup) :
          daq::rc::Controllable(),
          m_out_dir(dir),
          m_archive2cool(false),
          m_backup_mode(restore_from_backup),
          m_last_file_num(0),
          m_last_timestamp(0)
        {
          ;
        }

        void onExit(daq::rc::FSM_STATE state) noexcept override {
	  ERS_LOG("Exiting ResInfoProvider " << daq::rc::OnlineServices::instance().applicationName() << " while in state \"" << daq::rc::FSMStates::stateToString(state) << "\"");
        }

      /** Read partition resources and configuration of Resources Info service. */

      void configure(const daq::rc::TransitionCmd & /*args*/) override;

      void do_configure(const daq::core::Partition& partition);
      void do_publish_info(uint64_t timestamp, const daq::core::Partition& partition, bool no_infrastrcuture = false);


      /** Publish information at start of new run. */

      void prepareForRun(const daq::rc::TransitionCmd & /*args*/) override {
          if(m_backup_mode) {
            m_backup_mode = false;
            ERS_LOG( "do not publish resources since was restored from backup in running state" );
          }
          else {
            publish_info(0);
          }
        }


      /** Destroy any configuration information on unconfigure. */
      
      void unconfigure(const daq::rc::TransitionCmd & /*args*/) override {
	ERS_DEBUG(1,"Got unconfig command");
	release_config();
	clear_stopless_recovery_resources(daq::rc::OnlineServices::instance().getPartition());
      }


      /** The only supported user command is RESET. See ResourcesInfo/utils.h for string format. */

      void user(const daq::rc::UserCmd & cmd) override { process_user_command(cmd); }

      /** Read state of enabled/disabled resources from backup. */

      void read_backup();

    private:
      
      void release_config();
      void publish_info(uint64_t timestamp);
      void process_user_command(const daq::rc::UserCmd & cmd);
      uint64_t get_run_number_timestamp_from_is();
      void create_res_info_archiver_file(uint64_t timestamp, const daq::core::Partition& partition);
      void publish_res_info_in_IS() const;
      void IS_update_stopless_recovery_resources(uint64_t timestamp) const;
      void clear_stopless_recovery_resources(const daq::core::Partition& partition);

    private:

      std::string m_out_dir;
      bool m_archive2cool;
      bool m_backup_mode;
      uint64_t m_last_file_num;

      std::set<std::string> m_user_disabled;
      std::set<std::string> m_user_enabled;

      uint64_t m_last_timestamp;
      std::set<std::string> m_last_lb_disabled;
      std::set<std::string> m_last_lb_enabled;
      
    };
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace po = boost::program_options;


int
main(int argc, char **argv)
{
  try
    {
      IPCCore::init(argc, argv);
    }
  catch (ers::Issue &ex)
    {
      ers::warning(ers::Message(ERS_HERE, ex));
    }

  po::options_description desc("This program publishes in IS and stores enabled resources for the Resources Info archiver.");

  try
    {
      std::string outDir;
      uint64_t ts;
      bool backupMode;

      desc.add_options()
        ("out-directory,o" , po::value<std::string>(&outDir)->required() , "Name of directory to store resources info")
        ("restore-from-backup,r" , "Restore state from backup")
        ("get-file,f", po::value<uint64_t>(&ts), "Produce resources info file with given timestamp for debug purposes and exit")
      ;

      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
      po::notify(vm);

      backupMode = vm.count("restore-from-backup");

      // Parser for the ItemCtrl
      daq::rc::CmdLineParser cmdParser(argc, argv, true);

      daq::ResourcesInfo::Provider *provider = new daq::ResourcesInfo::Provider(outDir, backupMode);

      if (vm.count("get-file"))
        {
          ERS_DEBUG (0, "produce res-info-archiver file and exit");

          Configuration db(cmdParser.database());

          if (const daq::core::Partition *partition = daq::core::get_partition(db, cmdParser.partitionName()))
            {
              provider->do_configure(*partition);
              provider->do_publish_info(ts, *partition, true);
            }
          else
            {
              std::cerr << "ERROR: cannot find partition \"" << cmdParser.partitionName() << '\"' << std::endl;
              return EXIT_FAILURE;
            }

          return EXIT_SUCCESS;
        }

      daq::rc::ItemCtrl itemCtrl(cmdParser, std::shared_ptr<daq::rc::Controllable>(provider));

      // Can read from backup only when itemCtrl is built, since backup needs to know which IPC partition to use
      if (backupMode)
        provider->read_backup();

      itemCtrl.init();
      itemCtrl.run();
    }
  catch (daq::rc::CmdLineHelp &ex)
    {
      std::cout << desc << std::endl;
      std::cout << ex.message() << std::endl;
    }
  catch (ers::Issue &ex)
    {
      ers::fatal(ex);
      return EXIT_FAILURE;
    }
  catch (boost::program_options::error &ex)
    {
      ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
      return EXIT_FAILURE;
    }
  catch (std::exception &ex)
    {
      ers::fatal(daq::ResourcesInfo::CommandLineError(ERS_HERE, ex.what()));
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /** Resources Info Service configuration */

struct DetectorFolderInfo
{
  DetectorFolderInfo(const daq::core::ResourcesInfoDetectorConfig *o) :
      m_config(o)
  {
    ;
  }

  const daq::core::ResourcesInfoDetectorConfig *m_config;
  std::set<const daq::core::Segment*> m_segment_parents;
  std::set<const daq::core::ResourceSet*> m_resource_sets_parents;
  std::set<const daq::core::ResourceBase*> m_resources;

  std::map<std::string, std::set<const daq::core::ResourceBase*> > m_sub_folders;

  bool
  put_resource(const daq::core::ResourceBase *res, const std::vector<const daq::core::ResourcesInfoDetectorFolderConfig*> &config);

  const std::string&
  get_name() const
  {
    return m_config->get_FolderName();
  }

  static DetectorFolderInfo*
  get_folder(eformat::SubDetector id, const daq::core::Component *obj);

  static DetectorFolderInfo*
  get_common_folder();

  static void
  clear();
};

static std::vector<DetectorFolderInfo> df_info;

void
DetectorFolderInfo::clear()
{
  for (auto &info : df_info)
    {
      info.m_segment_parents.clear();
      info.m_resource_sets_parents.clear();
      info.m_resources.clear();
      info.m_sub_folders.clear();
    }
}

DetectorFolderInfo*
DetectorFolderInfo::get_folder(eformat::SubDetector id, const daq::core::Component *obj)
{
  const std::size_t size2(df_info.size()-1);

  for (std::size_t i = 0; i < size2; ++i)
    for (auto &x : df_info[i].m_config->get_SubDetectorIDs())
      if (id == x)
        return &df_info[i];

  std::ostringstream text;
  text << std::hex << std::showbase << "the detector with id " << id << " linked with " << obj << " is out of ranges defined by resources info configuration";
  ers::warning(daq::ResourcesInfo::ConfigError(ERS_HERE, text.str().c_str()));

  return get_common_folder();
}

DetectorFolderInfo* DetectorFolderInfo::get_common_folder()
{
  return &(*df_info.rbegin());
}

bool
DetectorFolderInfo::put_resource(const daq::core::ResourceBase *res, const std::vector<const daq::core::ResourcesInfoDetectorFolderConfig*> &config)
{
  for (auto &i : config)
    for (auto &j : i->get_BaseClasses())
      if (res->castable(j))
        {
          m_sub_folders[i->get_FolderName()].insert(res);
          return true;
        }

  return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void daq::ResourcesInfo::Provider::configure(const daq::rc::TransitionCmd & /*args*/)
{
  const daq::core::Partition &partition(daq::rc::OnlineServices::instance().getPartition());

  if (!m_backup_mode)
    clear_stopless_recovery_resources(partition);

  df_info.clear();

  do_configure(partition);
}


void daq::ResourcesInfo::Provider::do_configure(const daq::core::Partition& partition)
{
  try
    {
      if (const daq::core::ResourcesInfoConfig * ri_conf = partition.get_ResourcesInfoConfiguration())
        {
          m_archive2cool = ri_conf->get_ArchiveInCOOL();
          const std::vector<const daq::core::ResourcesInfoDetectorConfig*>& folders = ri_conf->get_DetectorFolders();
          df_info.reserve(folders.size()+1);
          std::ostringstream log_msg;
          log_msg << std::hex << std::showbase << "ResourcesInfoConfiguration of " << &partition << " contains:\n";
          for (auto & x : folders)
            {
              DetectorFolderInfo& info = df_info.emplace_back(x);
              log_msg << " * folder " << info.get_name() << " for event format source identifiers in range ";

              bool first = true;
              for (auto & x : info.m_config->get_SubDetectorIDs())
                {
                  if (first)
                    first = false;
                  else
                    log_msg << ',';
                  log_msg << static_cast<int>(x);
                }

              log_msg << '\n';
            }

          auto& global_folder_info = df_info.emplace_back(ri_conf->get_GlobalFolder());
          log_msg << " * folder " << global_folder_info.get_name() << " for shared resources\n";
          ERS_LOG(log_msg.str());
        }
      else
        {
          ERS_LOG("ResourcesInfoConfiguration of " << &partition << " is empty");
        }
    }
  catch(ers::Issue& ex)
    {
      daq::ResourcesInfo::ConfigError issue(ERS_HERE, "exception", ex);
      ers::error(issue);
      release_config();
      throw issue;
   }
  catch(std::exception& ex)
    {
      daq::ResourcesInfo::ConfigError issue(ERS_HERE, ex.what());
      ers::error(issue);
      release_config();
      throw issue;
    }
}

void daq::ResourcesInfo::Provider::release_config()
{
  df_info.clear();
  m_archive2cool = false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// add all resources of given resource (including nested if it is resource set)
static void
add_resources(const daq::core::ResourceBase *res, DetectorFolderInfo &info)
{
  info.m_resources.insert(res->cast<daq::core::ResourceBase>());

  if (const daq::core::ResourceSet *rs_obj = res->cast<daq::core::ResourceSet>())
    for (auto &i : rs_obj->get_Contains())
      add_resources(i, info);
}

// add all resources of given segment
static void
add_resources(const daq::core::Segment *seg, DetectorFolderInfo &info)
{
  for (auto &i : seg->get_Segments())
    add_resources(i, info);

  for (auto &i : seg->get_Resources())
    add_resources(i, info);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void
str2obj(Configuration& db, const std::set<std::string>& in, std::set<const daq::core::Component *>& out)
{
  for (auto & i : in)
    {
      try
        {
          if (const daq::core::Component * c = db.get<daq::core::Component>(i))
            {
              out.insert(c);
            }
          else
            {
              std::ostringstream text;
              text << "failed to find component database object \'" << i << '\'';
              ers::error(daq::ResourcesInfo::ConfigError(ERS_HERE, text.str().c_str()));
            }
        }
      catch(ers::Issue & ex)
        {
          std::ostringstream text;
          text << "trying to get component \'" << i << "\' caught " << ex;
          ers::error(daq::ResourcesInfo::ConfigError(ERS_HERE, text.str().c_str()));
        }
    }
}


void
daq::ResourcesInfo::Provider::clear_stopless_recovery_resources(const daq::core::Partition &partition)
{
  m_user_disabled.clear();
  m_user_enabled.clear();

  std::set<const daq::core::Component*> empty;

  partition.set_disabled(empty);
  partition.set_enabled(empty);

  IS_update_stopless_recovery_resources(0);

  m_last_timestamp = 0;
  m_last_lb_disabled.clear();
  m_last_lb_enabled.clear();
}

void
daq::ResourcesInfo::Provider::publish_info(uint64_t timestamp)
{
  Provider::do_publish_info(timestamp, daq::rc::OnlineServices::instance().getPartition());
}

// for every found component store map of detect ids and component defining them
typedef std::map<const daq::core::Component *, std::map<eformat::SubDetector, const daq::core::Component *>> ComponentMap;

struct ParentsInfo
{
  enum
  {
    p_limit = 128
  };

  ParentsInfo() :
      p_index(0)
  {
    ;
  }

  std::size_t p_index;

  const daq::core::Component *m_parents[p_limit];

  void
  pop()
  {
    p_index--;
  }

  void
  push(const daq::core::Component *object)
  {
    if (p_index < p_limit)
      {
        m_parents[p_index++] = object;
      }
    else
      {
        std::ostringstream s;
        for (std::size_t i = 0; i < p_index; ++i)
          {
            if (i != 0)
              s << ", ";
            s << m_parents[i];
          }

        throw daq::ResourcesInfo::MaxInclusionReached(ERS_HERE, p_limit, s.str());
      }
  }

  void
  add_detector_id(ComponentMap& id_info, eformat::SubDetector id, const daq::core::Component * c)
  {
    for (std::size_t idx = 0; idx < p_index; ++idx)
      id_info[m_parents[idx]].emplace(id, c);
  }
};

static void
fill_id_info(const daq::core::ResourceBase *res, ParentsInfo &parents, ComponentMap &id_info, bool has_ros_parent)
{
  const daq::core::Component *c = res->cast<daq::core::Component>();

  parents.push(c);

  if (const daq::df::ROS *ros = res->cast<daq::df::ROS>())
    {
      static std::string bad_sid("UNKNOWN");

      const eformat::SubDetector id(static_cast<eformat::SubDetector>(ros->get_Detector()->get_LogicalId()));
      const eformat::helper::SourceIdentifier sid(id, 0);

      if (id != 0 && sid.human_detector() != bad_sid)
        parents.add_detector_id(id_info, id, c);

      has_ros_parent = true;
    }
  else if (has_ros_parent)
    {
      if (const daq::df::InputChannel *input_channel = res->cast<daq::df::InputChannel>())
        if (auto rdc_det_id = eformat::helper::SourceIdentifier(input_channel->get_Id()).subdetector_id())
          parents.add_detector_id(id_info, rdc_det_id, c);
    }

  if (const daq::core::ResourceSet *rset = res->cast<daq::core::ResourceSet>())
    for (const auto &x : rset->get_Contains())
      fill_id_info(x, parents, id_info, has_ros_parent);

  parents.pop();
}

static void
fill_id_info(const daq::core::Segment *seg, ParentsInfo& parents, ComponentMap& id_info, std::ostringstream &warn)
{
  const daq::core::Component * c = seg->cast<daq::core::Component>();

  parents.push(c);

  for (auto &i : seg->get_Segments())
    fill_id_info(i, parents, id_info, warn);

  for (auto &i : seg->get_Resources())
    fill_id_info(i, parents, id_info, false);

  for (auto &i : seg->get_Applications())
    if (const daq::core::ResourceBase * r = i->cast<daq::core::ResourceBase>())
      {
        warn << " * resource application " << i << " included by " << seg << " via \"Applications\" relationship instead of \"Resources\" one\n";
        fill_id_info(r, parents, id_info, false);
      }

  parents.pop();
}

void
daq::ResourcesInfo::Provider::do_publish_info(uint64_t timestamp, const daq::core::Partition &partition, bool no_infrastrcuture)
{
  if (df_info.empty())
    {
      ERS_DEBUG(1, "there is no Resources Info configuration => nothing to publish");
      return;
    }


  DetectorFolderInfo::clear();

    // apply user disabled and enabled components

  if (!no_infrastrcuture)
    {
      if (timestamp == 0)
        {
          clear_stopless_recovery_resources(partition);
        }
      else
        {
          std::set<const daq::core::Component*> user_disabled;
          std::set<const daq::core::Component*> user_enabled;

          str2obj(partition.configuration(), m_user_disabled, user_disabled);
          str2obj(partition.configuration(), m_user_enabled, user_enabled);

          partition.set_disabled(user_disabled);
          partition.set_enabled(user_enabled);

          IS_update_stopless_recovery_resources(timestamp);
        }
    }

  ERS_DEBUG (0, "Create components - detector mapping");

  ComponentMap id_info;
  ParentsInfo parent_info;

  std::ostringstream warn;
  warn << std::endl;

  for (const auto &x : partition.get_Segments())
    fill_id_info(x, parent_info, id_info, warn);

  if (timestamp == 0 && warn.str().size() > 10)
    ers::warning(daq::ResourcesInfo::ConfigError(ERS_HERE, warn.str().c_str()));

  ERS_DEBUG (0, "Split components by folders");

  DetectorFolderInfo * common_folder = DetectorFolderInfo::get_common_folder();

  for (const auto &x : id_info)
    {
      DetectorFolderInfo * folder = nullptr;

      for (auto &id : x.second)
        if (folder == nullptr)
          {
            folder = DetectorFolderInfo::get_folder(id.first, id.second);

            if (folder == common_folder)
              break;
          }
        else
          {
            DetectorFolderInfo * folder2 = DetectorFolderInfo::get_folder(id.first, id.second);

            if (folder2 != folder)
              {
                ERS_DEBUG( 1, std::hex << std::showbase << "select \"GLOBAL\" folder for " << x.first << " referencing "
                           << (*x.second.cbegin()).second << " (detector id: " << (*x.second.cbegin()).first << " => \"" << folder->get_name() << "\") and "
                           << id.second << " (detector id: " << id.first << " => \"" << folder2->get_name() << "\")");
                folder = common_folder;
                break;
              }
          }

      if (const daq::core::ResourceBase *r = x.first->cast<daq::core::ResourceBase>())
        {
          if (const daq::core::ResourceSet *rs = x.first->cast<daq::core::ResourceSet>())
            {
              ERS_DEBUG( 2, "put resource set " << rs << " to " << folder->get_name());
              folder->m_resource_sets_parents.insert(rs);
            }
          else
            {
              ERS_DEBUG( 2, "put resource " << r << " to " << folder->get_name());
              folder->m_resources.insert(r);
            }
        }
      else if (const daq::core::Segment *s = x.first->cast<daq::core::Segment>())
        {
          ERS_DEBUG( 2, "put segment " << s << " to " << folder->get_name());
          folder->m_segment_parents.insert(s);
        }
    }


  // remove segments and resources sets referenced by parents in every folder

  for (auto& info : df_info)
    {
      std::set<const daq::core::ResourceSet*> rs2removed;
      std::set<const daq::core::Segment*> seg2removed;

      for (const auto &j : info.m_resource_sets_parents)
        for (const auto &x : j->get_Contains())
          if (const daq::core::ResourceSet *rs = x->cast<daq::core::ResourceSet>())
            rs2removed.insert(rs);

      for (const auto &j : info.m_segment_parents)
        {
          for (const auto &x : j->get_Resources())
            if (const daq::core::ResourceSet *rs = x->cast<daq::core::ResourceSet>())
              rs2removed.insert(rs);

          for (const auto &x : j->get_Segments())
            seg2removed.insert(x);
        }

      for (const auto& x : rs2removed)
        info.m_resource_sets_parents.erase(x);

      for (const auto& x : seg2removed)
        info.m_segment_parents.erase(x);
    }

  // add all resources of top-level segments and resource sets

  for (auto& info : df_info)
    {
      for (auto &j : info.m_segment_parents)
        add_resources(j, info);

      for (auto &j : info.m_resource_sets_parents)
        add_resources(j, info);
    }

  std::ostringstream log_text;
  log_text << "Top-level segments [and/or resource sets] per detectors:\n";

  for (auto& info : df_info)
    {
      log_text << " - detector folder \"" << info.get_name() << "\" contains " << info.m_resources.size() << " resources from:" << std::endl;

      for (const auto& j : info.m_segment_parents)
        log_text << "    * " << j << std::endl;

      for (const auto& j : info.m_resource_sets_parents)
        log_text << "    # " << j << std::endl;
    }

  ers::log(ers::Message(ERS_HERE, log_text.str()));

  const std::size_t df_info_size2(df_info.size()-1);

  // range resources by detector sub-folders
  for (std::size_t i = 0; i < df_info_size2; ++i)
    {
      DetectorFolderInfo &info = df_info[i];

      for (const auto &j : info.m_resources)
        if (!j->disabled(partition))
          if (!info.put_resource(j, partition.get_ResourcesInfoConfiguration()->get_DefaultDetectorSubFolders()))
            info.put_resource(j, info.m_config->get_SubFolders());
    }

  // special processing for GLOBAL folder is required (skip resources put into detector folders)
  for (const auto &j : common_folder->m_resources)
    if (!j->disabled(partition))
      {
        bool found(false);

        for (std::size_t i = 0; i < df_info_size2; ++i)
          {
            const DetectorFolderInfo &info = df_info[i];
            if (info.m_resources.find(j) != info.m_resources.end())
              {
                found = true;
                ERS_DEBUG(1, "skip resource " << j << " assigned to " << common_folder->get_name() << " folder, since it is found in " << info.get_name() << " folder");
                break;
              }
          }

        if (!found)
          if (!common_folder->put_resource(j, partition.get_ResourcesInfoConfiguration()->get_DefaultDetectorSubFolders()))
            common_folder->put_resource(j, common_folder->m_config->get_SubFolders());
      }

  // create file for res_info_archiver

  if (m_archive2cool || no_infrastrcuture)
    {
      if (timestamp == 0)
        timestamp = get_run_number_timestamp_from_is();

      create_res_info_archiver_file(timestamp, partition);
    }
  else
    {
      ERS_LOG("\"ArchiveInCOOL@ResourcesInfoConfiguration\" is false, do not create file for res_info_archiver");
    }

  // publish in IS

  if (!no_infrastrcuture)
    publish_res_info_in_IS();
}

void
daq::ResourcesInfo::Provider::create_res_info_archiver_file(uint64_t num, const daq::core::Partition &partition)
{
  std::string file, tfile;

    {
      if (num <= m_last_file_num)
        {
          ers::warning(daq::ResourcesInfo::TimestampWasUsed(ERS_HERE, num));
          num = m_last_file_num + 1;
        }

      m_last_file_num = num;

      std::ostringstream text, text2;
      text << m_out_dir << '/' << num;
      file = text.str();
      text2 << m_out_dir << "/." << num;
      tfile = text2.str();
    }

  long file_len = 0;

  try
    {
      std::ofstream f(tfile.c_str());
      f.exceptions(std::ostream::eofbit | std::ostream::failbit | std::ostream::badbit);

      if (!f)
        throw std::runtime_error("failed to open file in write mode");

      f << num << std::endl << partition.UID() << std::endl;

      for (const auto &info : df_info)
        if (!info.m_sub_folders.empty())
          {
            f << '#' << info.get_name() << std::endl; // detector folder
            for (const auto &j : info.m_sub_folders)
              {
                f << '$' << j.first << std::endl; // detector sub-folder
                for (const auto &x : j.second)
                  f << '-' << x->UID() << '@' << x->class_name() << std::endl; // sub-folder channel
              }
          }

      file_len = f.tellp();
      f.close();
    }
  catch (std::exception &ex)
    {
      const std::string ex_text(daq::ResourcesInfo::ex2text(ex));
      daq::ResourcesInfo::FileSystemError issue(ERS_HERE, "write", "file", tfile.c_str(), ex_text.c_str());
      ers::fatal(issue);
      throw issue;
    }

  try
    {
      std::ifstream f(tfile.c_str());

      if (!f)
        throw std::runtime_error("failed to open file in read mode");

      f.seekg(0, std::ios::end);
      long written_len = static_cast<std::streamoff>(f.tellg());

      if (written_len != file_len)
        {
          unlink(tfile.c_str());
          std::ostringstream text;
          text << "write error: " << written_len << " bytes have been written instead of " << file_len;
          throw std::runtime_error(text.str().c_str());
        }
    }
  catch (std::exception &ex)
    {
      const std::string ex_text(daq::ResourcesInfo::ex2text(ex));
      daq::ResourcesInfo::FileSystemError issue(ERS_HERE, "read", "file", tfile.c_str(), ex_text.c_str());
      ers::fatal(issue);
      throw issue;
    }

  if (int code = rename(tfile.c_str(), file.c_str()))
    {
      std::ostringstream text;
      text << "cannot rename file \'" << tfile << "\' to \'" << file << "\': rename() failed with code " << code << ", reason = \'" << strerror(errno) << '\'';
      throw std::runtime_error(text.str().c_str());
    }

  ERS_LOG("created file \'" << file << "\' (" << file_len << " bytes) for res_info_archiver\n");
}

void
daq::ResourcesInfo::Provider::publish_res_info_in_IS() const
{
  ISInfoDictionary dict(daq::rc::OnlineServices::instance().getIPCPartition());

  // get existing data from IS server

  std::set<std::string> is_info;

  try
    {
      ISInfoIterator it(daq::rc::OnlineServices::instance().getIPCPartition(), "Resources", EnabledResource::type());

      ERS_DEBUG(2, "found " << it.entries() << " objects");

      while (it())
        is_info.insert(it.name());
    }
  catch (ers::Issue &ex)
    {
      ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot read information from IS server", ex));
    }

  // iterate resources and publish missing resources

  try
    {
      for (const auto &info : df_info)
        if (!info.m_sub_folders.empty())
          for (const auto &j : info.m_sub_folders)
            {
              const std::string folder(daq::ResourcesInfo::s_enabled_resoures + info.get_name() + '/' + j.first + '/');

              for (const auto &x : j.second)
                {
                  const std::string info_name(folder + x->UID());

                  std::set<std::string>::iterator i = is_info.find(info_name);

                  if (i == is_info.end())
                    {
                      ERS_DEBUG(2, "information about resources " << info_name << " was not published in IS");

                      EnabledResource obj;
                      obj.Class = x->class_name();

                      dict.checkin(info_name, obj);

                      ERS_DEBUG(1, "insert \"" << info_name << "\" => \"" << x->class_name() << "\"");
                    }
                  else
                    {
                      ERS_DEBUG(2, "information about resources " << info_name << " was published in IS");
                      is_info.erase(i);
                    }
                }
            }
    }
  catch (std::exception &ex)
    {
      ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot store information on IS server", ex));
    }

  // remove disabled resources

  try
    {
      for (std::set<std::string>::iterator i = is_info.begin(); i != is_info.end(); ++i)
        if ((*i).find(daq::ResourcesInfo::s_enabled_resoures) == 0)
          {
            ERS_LOG("removing info " << *i << " ...");
            dict.remove(*i);
          }
    }
  catch (std::exception &ex)
    {
      ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot remove information from IS server", ex));
    }
}


void
daq::ResourcesInfo::Provider::read_backup()
{

  ISInfoDictionary dict(daq::rc::OnlineServices::instance().getIPCPartition());

  // read from IS resources disabled and enabled by stopless recovery

  try
    {
      ISInfoIterator it(daq::rc::OnlineServices::instance().getIPCPartition(), "Resources", InfoResource::type());

      ERS_DEBUG(2, "found " << it.entries() << " objects");

      while (it())
        {
          daq::ResourcesInfo::add_stopless_recovery_name(daq::ResourcesInfo::s_stopless_recovery_disabled_resoures,
                                                         daq::ResourcesInfo::s_stopless_recovery_disabled_resoures.size(), it.name(), m_user_disabled);
          daq::ResourcesInfo::add_stopless_recovery_name(daq::ResourcesInfo::s_stopless_recovery_enabled_resoures, daq::ResourcesInfo::s_stopless_recovery_enabled_resoures.size(),
                                                         it.name(), m_user_enabled);
        }
    }
  catch (ers::Issue &ex)
    {
      throw daq::ResourcesInfo::ISError(ERS_HERE, "cannot read information from \"Resources\" IS server", ex);
    }

  // read state of run:
  //
  // 1. if RootController is in RUNNING state
  //    then do not publish information when go to prepareForRun state first time
  // 2. else assume application died not in running state and
  //    then publish information when go to prepareForRun state
  //

  RCStateInfo stateInfo;

  try
    {
      dict.findValue("RunCtrl.RootController", stateInfo);
      ERS_DEBUG(1, "RootController is in \"" << stateInfo.state << "\" state");
    }
  catch (ers::Issue &ex)
    {
      throw daq::ResourcesInfo::ISError(ERS_HERE, "cannot read information from \"RunCtrl\" IS server", ex);
    }

  m_backup_mode = false;

  if (stateInfo.state == "RUNNING")
    {
      ERS_LOG("provider was running (RootController is in \"" << stateInfo.state << "\" state)");
      m_backup_mode = true;
    }
}


static void
publish_missing_info(ISInfoDictionary &dict, const std::set<std::string> &s_user, const std::set<std::string> &s_is, const std::string &prefix, uint64_t timestamp)
{
  for (const auto& i : s_user)
    {
      const std::string info_name(prefix + i);
      if (s_is.find(info_name) == s_is.end())
        {
          try
            {
              ERS_LOG("check in IS stopless recovery resource " << info_name << " ...");
              InfoResource obj;
              obj.Timestamp = timestamp;
              dict.checkin(info_name, obj);
            }
          catch (std::exception &ex)
            {
              ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot publish information on \"Resources\" IS server", ex));
            }
        }
    }
}

static void
remove_obsolete_info(ISInfoDictionary &dict, const std::set<std::string> &s_user, const std::set<std::string> &s_is, std::string::size_type prefix_len)
{
  for (const auto& i : s_is)
    {
      const std::string name(i, prefix_len);
      if (s_user.find(name) == s_user.end())
        {
          try
            {
              ERS_LOG("removing from IS stopless recovery resource " << i << " ...");
              dict.remove(i);
            }
          catch (std::exception &ex)
            {
              ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot remove information from \"Resources\" IS server", ex));
            }
        }
    }
}

void
daq::ResourcesInfo::Provider::IS_update_stopless_recovery_resources(uint64_t timestamp) const
{
  ISInfoDictionary dict(daq::rc::OnlineServices::instance().getIPCPartition());

  // read what we have in IS

  std::set<std::string> SLR_enabled;
  std::set<std::string> SLR_disabled;

  try
    {
      ISInfoIterator it(daq::rc::OnlineServices::instance().getIPCPartition(), "Resources", InfoResource::type());

      ERS_DEBUG(2, "found " << it.entries() << " objects");

      while (it())
        {
          daq::ResourcesInfo::add_stopless_recovery_name(daq::ResourcesInfo::s_stopless_recovery_disabled_resoures, 0, it.name(), SLR_disabled);
          daq::ResourcesInfo::add_stopless_recovery_name(daq::ResourcesInfo::s_stopless_recovery_enabled_resoures, 0, it.name(), SLR_enabled);
        }
    }
  catch (ers::Issue &ex)
    {
      ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot read information from \"Resources\" IS server", ex));
    }

  // publish what is not in IS

  publish_missing_info(dict, m_user_disabled, SLR_disabled, daq::ResourcesInfo::s_stopless_recovery_disabled_resoures, timestamp);
  publish_missing_info(dict, m_user_enabled, SLR_enabled, daq::ResourcesInfo::s_stopless_recovery_enabled_resoures, timestamp);

  remove_obsolete_info(dict, m_user_disabled, SLR_disabled, daq::ResourcesInfo::s_stopless_recovery_disabled_resoures.size());
  remove_obsolete_info(dict, m_user_enabled, SLR_enabled, daq::ResourcesInfo::s_stopless_recovery_enabled_resoures.size());

  // sign the finish of updates to allow disabled() algorithm read IS information

  try
    {
      ERS_LOG("checkin " << daq::ResourcesInfo::s_stopless_recovery_updated_resoures << " ...");
      UpdateResourceInfo obj;
      obj.Timestamp = timestamp;
      dict.checkin(daq::ResourcesInfo::s_stopless_recovery_updated_resoures, obj);
    }
  catch (std::exception &ex)
    {
      ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot publish information on \"Resources\" IS server", ex));
    }
}

static bool
find_new(const std::set<std::string> &s_old, const std::set<std::string> &s_new)
{
  for (const auto& i : s_new)
    if (s_old.find(i) == s_old.end())
      return true;

  return false;
}

static bool
find_old(const std::set<std::string> &s_old, const std::set<std::string> &s_new)
{
  for (const auto& i : s_new)
    if (s_old.find(i) != s_old.end())
      return true;

  return false;
}

static void
put_new(std::set<std::string> &s_old, const std::set<std::string> &s_new, const char *name)
{
  for (std::set<std::string>::const_iterator i = s_new.begin(); i != s_new.end(); ++i)
    {
      s_old.insert(*i);
      ERS_LOG("add \"" << *i << "\" to user-" << name);
    }
}

static void
remove_old(std::set<std::string> &s_old, const std::set<std::string> &s_new, const char *name)
{
  for (std::set<std::string>::const_iterator i = s_new.begin(); i != s_new.end(); ++i)
    {
      if (s_old.erase(*i) != 0)
        {
          ERS_LOG("remove \"" << *i << "\" from user-" << name);
        }
    }
}

static void
validate_user_names(Configuration &db, std::set<std::string> &names, const std::string &args)
{
  std::set<std::string>::iterator i = names.begin();
  while (i != names.end())
    {
      try
        {
          if (db.get<daq::core::Component>(*i) == 0)
            {
              std::ostringstream text;
              text << "failed to find component database object \'" << *i << "\'  in user command RESET:\n" << args;
              ers::error(daq::ResourcesInfo::CannotProcessUserCommand(ERS_HERE, text.str().c_str()));
              names.erase(i++);
            }
          else
            {
              ++i;
            }
        }
      catch (ers::Issue &ex)
        {
          std::ostringstream text;
          text << "trying to get component database object \'" << *i << "\' caught " << ex;
          ers::error(daq::ResourcesInfo::ConfigError(ERS_HERE, text.str().c_str()));
        }
    }
}

//static void report_not_allowed_error(const char * why, const std::string& args)
//{
//  std::ostringstream text;
//  text << why << ", the user command RESET is not allowed in such state.\nThe command was:\n" << args;
//  ers::error(daq::ResourcesInfo::CannotProcessUserCommand(ERS_HERE, text.str().c_str()));
//}

static std::string
set2str(const std::set<std::string> &val)
{
  std::string s;
  for (std::set<std::string>::const_iterator i = val.begin(); i != val.end(); ++i)
    {
      if (i != val.begin())
        s.append(", ");
      s.append(*i);
    }
  return s;
}

void
daq::ResourcesInfo::Provider::process_user_command(const daq::rc::UserCmd &cmd)
{

  std::string args;
  for (const auto &a : cmd.commandParameters())
    {
      args.append(a);
      args.append(" ");
    }
  if (cmd.commandName() == "RESET")
    {
      uint64_t timestamp;
      std::set<std::string> user_disabled, user_enabled;

      // FIXME
//    if(!m_run_number) {
//      report_not_allowed_error("the application is not in the running state", args);
//      return;
//    }

      // decode user command

      try
        {
          daq::ResourcesInfo::decode_user_cmd(args, timestamp, user_disabled, user_enabled);
        }
      catch (std::exception &ex)
        {
          std::ostringstream text;
          text << "failed to parse arguments of user command RESET:\n" << ex.what();
          throw daq::ResourcesInfo::CannotProcessUserCommand(ERS_HERE, text.str().c_str());
        }

      if (timestamp == 0)
        {
          std::ostringstream text;
          text << "timestamp cannot be 0 in user command RESET:\n" << args;
          throw daq::ResourcesInfo::CannotProcessUserCommand(ERS_HERE, text.str().c_str());
        }

      // validate existence of resources in user input

      validate_user_names(daq::rc::OnlineServices::instance().getConfiguration(), user_disabled, args);
      validate_user_names(daq::rc::OnlineServices::instance().getConfiguration(), user_enabled, args);

      if (user_disabled.empty() && user_enabled.empty())
        {
          ERS_LOG("ignore empty user command RESET");
          return;
        }

      // check if there are no re-enabled or re-disabled resources in the same timestamp

      std::set<std::string> bad_re_enabled, bad_re_disabled;  // store re-set resources to be thrown as exception at the end

      if (timestamp == m_last_timestamp)
        {
          std::set<std::string>::const_iterator it;

          while ((it = std::find_first_of(user_disabled.begin(), user_disabled.end(), m_last_lb_enabled.begin(), m_last_lb_enabled.end())) != user_disabled.end())
            {
              bad_re_disabled.insert(*it);
              user_disabled.erase(it);
            }

          while ((it = std::find_first_of(user_enabled.begin(), user_enabled.end(), m_last_lb_disabled.begin(), m_last_lb_disabled.end())) != user_enabled.end())
            {
              bad_re_enabled.insert(*it);
              user_enabled.erase(it);
            }

          m_last_lb_disabled.insert(user_disabled.begin(), user_disabled.end());
          m_last_lb_enabled.insert(user_enabled.begin(), user_enabled.end());
        }
      else
        {
          m_last_timestamp = timestamp;
          m_last_lb_disabled = user_disabled;
          m_last_lb_enabled = user_enabled;
        }

      // check that there is something new disabled or enabled

      if (find_new(m_user_disabled, user_disabled) || find_new(m_user_enabled, user_enabled) || find_old(m_user_disabled, user_enabled) || find_old(m_user_enabled, user_disabled))
        {
          put_new(m_user_disabled, user_disabled, "disabled");
          put_new(m_user_enabled, user_enabled, "enabled");
          remove_old(m_user_disabled, user_enabled, "disabled");
          remove_old(m_user_enabled, user_disabled, "enabled");
          publish_info(timestamp);
        }
      else if (bad_re_enabled.empty() && bad_re_disabled.empty())
        {
          std::ostringstream text;
          text << "there are no newly disabled or enabled components in user command RESET:\n" << args;
          ers::error(daq::ResourcesInfo::CannotProcessUserCommand(ERS_HERE, text.str().c_str()));
        }

      if (!bad_re_enabled.empty() || !bad_re_disabled.empty())
        {
          throw daq::ResourcesInfo::TimestampResourcesReset(ERS_HERE, timestamp, ::set2str(bad_re_enabled), ::set2str(bad_re_disabled));
        }
    }
  else
    {
      ERS_LOG("user command \'" << cmd.commandName() << "\' is not supported");
    }
}

uint64_t
daq::ResourcesInfo::Provider::get_run_number_timestamp_from_is()
{
  ISInfoDictionary isInfoDict(daq::rc::OnlineServices::instance().getIPCPartition());

  RunParams runParameter;

  try
    {
      isInfoDict.findValue("RunParams.RunParams", runParameter);
      uint64_t ts = runParameter.timeSOR.total_mksec_utc() * 1000;
      ERS_DEBUG(1, "Read from Information Service (partition \'" << daq::rc::OnlineServices::instance().getIPCPartition().name() << "\') run number timestamp " << ts);
      return ts;
    }
  catch (daq::is::Exception &ex)
    {
      daq::ResourcesInfo::NoRunNumber issue(ERS_HERE, daq::rc::OnlineServices::instance().getIPCPartition().name().c_str(), ex);
      ers::error(issue);
      throw issue; // raise error
    }
}
