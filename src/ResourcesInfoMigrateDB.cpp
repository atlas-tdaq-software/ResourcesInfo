#include <stdexcept>
#include <iostream>
#include <string>


#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/time_parsers.hpp>

#include <CoralBase/Attribute.h>
#include <CoralBase/AttributeList.h>
#include <CoralBase/Exception.h>

#include <RelationalAccess/ConnectionService.h>
#include <RelationalAccess/ISessionProxy.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ICursor.h>

#include <CoolKernel/FolderSpecification.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IFolderSet.h>
#include <CoolKernel/IObject.h>
#include <CoolKernel/IObjectIterator.h>
#include <CoolApplication/Application.h>


#include <ers/ers.h>

#include <rn/rn.h>

#include "ResourcesInfo/errors.h"
#include "ResourcesInfoUtils.h"

namespace po = boost::program_options;

class Copy;

inline uint32_t
iov2rn(uint64_t key)
{
  return static_cast<uint32_t>(key >> 32);
}

inline uint32_t
iov2lumi(uint64_t key)
{
  return static_cast<uint32_t>(key & 0xffffffff);
}

class Run
{
  friend class Copy;

public:
  Run(int argc, char **argv);

  void
  read_lumi_block_db();

  void
  read_run_number_db();

  void
  copy_db();

private:
  std::map<cool::ValidityKey, cool::ValidityKey> m_lumi2ts;
  cool::Application m_cool_app;
  std::string m_old_db_name;
  std::string m_new_db_name;
  std::string m_lumi_db_name;
  std::string m_run_number_db_connection_name;
  std::string m_run_number_db_schema_name;
  std::string m_old_root_folder_name;

  std::string create_new_name(const std::string&);
};

class Copy
{
public:

  Copy(Run&);

private:

  Run& m_run_object;
  cool::IDatabasePtr m_old_db;
  cool::IDatabasePtr m_new_db;

  void process(cool::IFolderSetPtr);
};

Run::Run(int argc, char **argv) :
    m_old_db_name("oracle://ATLR;schema=ATLAS_COOLONL_TDAQ;dbname=COMP200"),
    m_lumi_db_name("oracle://ATLR;schema=ATLAS_COOLONL_TRIGGER;dbname=COMP200"),
    m_run_number_db_connection_name("oracle://atlr/rn_r"),
    m_run_number_db_schema_name("atlas_run_number"),
    m_old_root_folder_name("/TDAQ/EnabledResources")

{
  po::options_description desc(
      "This program migrates COOL folders used by Resources Info archiver from "
      "old run-number/lumi-block based IoV to new timestamp-based IoV.");

  desc.add_options()
    (
      "old-db,o",
      po::value<std::string>(&m_old_db_name),
      "name of COOL database containing data in old (run-number/lumi-block based) IoV format"
    )
    (
      "old-root-folder-name,r",
      po::value<std::string>(&m_old_root_folder_name),
      "name of the old root folder"
    )
    (
      "new-db,n",
      po::value<std::string>(&m_new_db_name)->required(),
      "name of new COOL database"
    )
    (
      "lumi-db,l",
      po::value<std::string>(&m_lumi_db_name),
      "name of COOL luminosity database"
    )
    (
      "run-number-db-connection,r",
      po::value<std::string>(&m_run_number_db_connection_name),
      "run number database"
    )
    (
      "run-number-db-working-schema,w",
      po::value<std::string>(&m_run_number_db_schema_name),
      "run number database"
    )
    (
      "help,h",
      "Print help message"
    )
  ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);

  if(vm.count("help")) {
    std::cout << desc << std::endl;
    exit(EXIT_SUCCESS);
  }

  po::notify(vm);
}

void
Run::read_run_number_db()
{
  static boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));

  try
    {
      coral::ConnectionService * connection = 0;
      std::unique_ptr<coral::ISessionProxy> session(tdaq::RunNumber::get_session(m_run_number_db_connection_name, static_cast<int>(coral::ReadOnly), connection));

      coral::ITable& table = session->schema(m_run_number_db_schema_name).tableHandle(tdaq::RunNumber::s_table_name);

      ERS_LOG("open run-number database \'" << m_run_number_db_connection_name << '\'');

      std::unique_ptr<coral::IQuery> query(table.newQuery());
      query->setRowCacheSize(10);

      std::string conditions_str = (std::string(tdaq::RunNumber::s_partition_name_column) + " =" + " :prt");

      coral::AttributeList conditions;
      conditions.extend<std::string>("prt");
      conditions["prt"].data<std::string>() = "ATLAS";

      query->setCondition(conditions_str, conditions);

      coral::ICursor& c = query->execute();

      unsigned int count(0);

      //std::cout << "RN CVT TABLE:\n";

      while (c.next())
        {
          const coral::AttributeList& row = c.currentRow();

          int run_number = row[tdaq::RunNumber::s_run_number_column].data<int>();
          std::string started = row[tdaq::RunNumber::s_start_at_column].data<std::string>();
          boost::posix_time::ptime t;

          try
            {
              t = boost::posix_time::from_iso_string(started);
            }
          catch (std::exception& ex)
            {
              std::ostringstream text;
              text << "cannot parse time \'" << started << "\': \"" << ex.what() << '\"';
              throw std::runtime_error(text.str().c_str());
            }

          count++;
          uint64_t value = (t - epoch).total_nanoseconds();
          uint64_t key = daq::ResourcesInfo::mk_uint64_t(run_number, 0);

          m_lumi2ts[key] = value;

          //std::cout << run_number << '(' << key << ')' << " => " << value <<'\n';
        }
      //std::cout << "END RN CVT TABLE\n";

      ERS_LOG("read information about " << count << " IOVs from run number database");

  }
  catch ( const coral::Exception& ex )
  {
    std::ostringstream text;
    text << "caught CORAL exception: " << ex.what();
    throw std::runtime_error(text.str().c_str());
  }
}

void
Run::read_lumi_block_db()
{
  cool::IDatabasePtr db;

  const std::string lumi_db_root("/TRIGGER/LUMI/LBLB");

  try
    {
      db = m_cool_app.databaseService().openDatabase(m_lumi_db_name);
      ERS_LOG("open lumi-block database \'" << m_lumi_db_name << '\'');
    }
  catch (cool::DatabaseDoesNotExist& ex)
    {
      std::ostringstream text;
      text << "database \'" << m_lumi_db_name << "\' does not exist";
      throw std::runtime_error(text.str().c_str());
    }

  if (db->existsFolder(lumi_db_root) == false)
    {
      std::ostringstream text;
      text << "database \'" << m_lumi_db_name << "\' has no folder \'" << lumi_db_root << "\'";
      throw std::runtime_error(text.str().c_str());
    }

  cool::IFolderPtr folder = db->getFolder(lumi_db_root);

  cool::IObjectIteratorPtr p = folder->browseObjects(cool::ValidityKeyMin, cool::ValidityKeyMax, 0);
  //std::cout << "CVT TABLE:\n";
  while (p->goToNext())
    {
      const cool::IObject& obj(p->currentRef());
      m_lumi2ts[obj.since()] = obj.payload()["StartTime"].data<cool::ValidityKey>();
      //std::cout << obj.since() << " (rn: " << iov2rn(obj.since()) << ", lb: " << iov2lumi(obj.since()) << ")=> " << obj.payload()["StartTime"].data<cool::ValidityKey>() <<'\n';
    }
  //std::cout << "END CVT TABLE\n\n";

  ERS_LOG("read information about " << m_lumi2ts.size() << " IOVs from lumi-block database");

  m_lumi2ts[cool::ValidityKeyMax] = cool::ValidityKeyMax;
}

void
Run::copy_db()
{
  Copy obj(*this);
}

std::string
Run::create_new_name(const std::string& old)
{
  std::string s(daq::ResourcesInfo::RootFolder);
  s.append(old.substr(m_old_root_folder_name.size()+1));
  return s;
}

Copy::Copy(Run& obj) : m_run_object(obj)
{
  try
    {
      m_old_db = m_run_object.m_cool_app.databaseService().openDatabase(m_run_object.m_old_db_name, true);
      ERS_LOG("open old database \'" << m_run_object.m_old_db_name << '\'');
    }
  catch (cool::DatabaseDoesNotExist& ex)
    {
      std::ostringstream text;
      text << "database \'" << m_run_object.m_old_db_name << "\' does not exist";
      throw std::runtime_error(text.str().c_str());
    }

  if (m_old_db->existsFolderSet(m_run_object.m_old_root_folder_name) == false)
    {
      std::ostringstream text;
      text << "database \'" << m_run_object.m_old_db_name << "\' has no folder set \'" << m_run_object.m_old_root_folder_name << "\'";
      throw std::runtime_error(text.str().c_str());
    }

  try
    {
      m_new_db = m_run_object.m_cool_app.databaseService().openDatabase(m_run_object.m_new_db_name, false);
      ERS_LOG("open old database \'" << m_run_object.m_new_db_name << '\'');
    }
  catch (cool::DatabaseDoesNotExist& ex)
    {
      std::ostringstream text;
      text << "database \'" << m_run_object.m_new_db_name << "\' does not exist";
      throw std::runtime_error(text.str().c_str());
    }

  process(m_old_db->getFolderSet( m_run_object.m_old_root_folder_name ));
}


void
Copy::process(cool::IFolderSetPtr fs_ptr)
{
  std::vector<std::string> folder_sets = fs_ptr->listFolderSets();
  std::vector<std::string> folders = fs_ptr->listFolders();

  for (auto & f : folder_sets)
    {
      cool::IFolderSetPtr old_fs_ptr = m_old_db->getFolderSet( f );
      const std::string new_fs_name(m_run_object.create_new_name(f));

      std::cout << " - folder set \'" << f << "\' => \'" << new_fs_name << "\'\n";

      if (m_new_db->existsFolderSet(new_fs_name) == false)
        {
          ERS_LOG("create folder set \"" << new_fs_name << "\": " << old_fs_ptr->description());
          m_new_db->createFolderSet(new_fs_name, old_fs_ptr->description(), false);
        }

      process(old_fs_ptr);
    }

  for (auto & f : folders)
    {
      cool::IFolderPtr old_f_ptr = m_old_db->getFolder( f );
      const std::string new_f_name(m_run_object.create_new_name(f));

      std::cout << " - folder \'" << f << "\' => \'" << new_f_name << "\'\n";

      if (m_new_db->existsFolder(new_f_name) == true)
        {
          ERS_LOG("remove existing folder " << new_f_name);
          m_new_db->dropNode(new_f_name);
        }

      if (m_new_db->existsFolder(new_f_name) == false)
        {
          ERS_LOG("create folder \"" << new_f_name << '\"');
          m_new_db->createFolder(new_f_name, old_f_ptr->folderSpecification(), daq::ResourcesInfo::FolderDescription);

          cool::IFolderPtr new_f_ptr = m_new_db->getFolder( new_f_name );

          std::map<cool::ChannelId,std::string> channels_id2name = old_f_ptr->listChannelsWithNames();

          new_f_ptr->setupStorageBuffer();

          for(auto & x : channels_id2name)
            {
              unsigned int count(0);
              unsigned int bad_count(0);

              new_f_ptr->createChannel(x.first, x.second);

              cool::IObjectIteratorPtr p = old_f_ptr->browseObjects(cool::ValidityKeyMin, cool::ValidityKeyMax, x.first);

              while (p->goToNext())
                {
                  const cool::IObject& obj(p->currentRef());

                  cool::ValidityKey since, until;

                  try
                    {
                      since = m_run_object.m_lumi2ts.at(obj.since());
                    }
                  catch (const std::out_of_range& ex)
                    {
                      std::ostringstream text;
                      text << "failed to convert IoV since " << obj.since() << " (rn: " << iov2rn(obj.since()) << ", lb: " << iov2lumi(obj.since()) << "): " << ex.what();
                      std::cout << "ERROR: " << text.str() << std::endl;
                      bad_count++;
                      continue;
                      //throw std::runtime_error(text.str().c_str());
                    }

                  try
                    {
                      until = m_run_object.m_lumi2ts.at(obj.until());
                    }
                  catch (const std::out_of_range& ex)
                    {
                      std::ostringstream text;
                      text << "failed to convert IoV until " << obj.until() << " (rn: " << iov2rn(obj.until()) << ", lb: " << iov2lumi(obj.until()) << "): " << ex.what();
                      std::cout << "ERROR: " << text.str() << std::endl;
                      bad_count++;
                      continue;
                      //throw std::runtime_error(text.str().c_str());
                    }

                  if(since == until)
                    {
                      std::cout << "ERROR: since == until " << "(" << since << ") from obj.since: " <<  obj.since() << ", obj.until: " << obj.until() << std::endl;
                      until++;
                    }

                  new_f_ptr->storeObject( since, until, obj.payload(), x.first ); // FIXME : fast read test
                  count++;

                  //std::cout << " * convert [" << obj.since() << ',' << obj.until() << "] to [" << since << ',' << until << "]\n";
                }

              ERS_LOG("store " << count << " objects in channel " << x.second << " (" << x.first << ')');
              if(bad_count) ERS_LOG("skip " << bad_count << " objects (bad lumi db)");
              new_f_ptr->flushStorageBuffer();
            }

          //new_f_ptr->flushStorageBuffer();
        }
    }
}


int main (int argc, char **argv)
{
  try
    {
      Run obj(argc, argv);

      obj.read_lumi_block_db();

      obj.read_run_number_db();

      obj.copy_db();
    }
  catch (cool::Exception& ex)
    {
      std::cerr << "COOL error: " << ex.what() << std::endl;
      return EXIT_FAILURE;
    }
  catch (std::exception& ex)
    {
      std::cerr << "error: " << ex.what() << std::endl;
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
