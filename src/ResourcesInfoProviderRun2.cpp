#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <algorithm>
#include <fstream>
#include <set>
#include <list>
#include <string>
#include <stdexcept>

#include <boost/program_options.hpp>

#include <ers/ers.h>

#include <dal/util.h>
#include <dal/Detector.h>
#include <dal/Partition.h>
#include <dal/Segment.h>
#include <dal/ResourceSet.h>
#include <dal/ResourceBase.h>
#include <dal/ResourcesInfoConfig.h>
#include <dal/ResourcesInfoDetectorConfig.h>
#include <dal/ResourcesInfoDetectorFolderConfig.h>
#include <DFdal/ROS.h>
#include <DFdal/InputChannel.h>

#include <ipc/core.h>

#include <is/infodictionary.h>
#include <is/infoiterator.h>

#include <eformat/SourceIdentifier.h>

#include <rc/RunParams.h>
#include <rc/RCStateInfo.h>
#include <RunControl/RunControl.h>

#include "ResourcesInfo/utils.h"
#include "ResourcesInfo/errors.h"
#include "ResourcesInfoUtils.h"
#include "EnabledResource.h"
#include "InfoResource.h"
#include "UpdateResourceInfo.h"


namespace daq {
  namespace ResourcesInfo {

    class Provider: public daq::rc::Controllable
    {

      public:

        Provider(const std::string& dir, bool restore_from_backup) :
          daq::rc::Controllable(),
          m_out_dir(dir),
          m_archive2cool(false),
          m_backup_mode(restore_from_backup),
          m_last_file_num(0),
          m_last_timestamp(0)
        {
          ;
        }

        void onExit(daq::rc::FSM_STATE state) noexcept override {
	  ERS_LOG("Exiting ResInfoProvider " << daq::rc::OnlineServices::instance().applicationName() << " while in state \"" << daq::rc::FSMStates::stateToString(state) << "\"");
        }

      /** Read partition resources and configuration of Resources Info service. */

      void configure(const daq::rc::TransitionCmd & /*args*/) override;

      void do_configure(const daq::core::Partition& partition);
      void do_publish_info(uint64_t timestamp, const daq::core::Partition& partition, bool no_infrastrcuture = false);


      /** Publish information at start of new run. */

      void prepareForRun(const daq::rc::TransitionCmd & /*args*/) override {
          if(m_backup_mode) {
            m_backup_mode = false;
            ERS_LOG( "do not publish resources since was restored from backup in running state" );
          }
          else {
            publish_info(0);
          }
        }


      /** Destroy any configuration information on unconfigure. */
      
      void unconfigure(const daq::rc::TransitionCmd & /*args*/) override {
	ERS_DEBUG(1,"Got unconfig command");
	release_config();
	clear_stopless_recovery_resources(daq::rc::OnlineServices::instance().getPartition());
      }


      /** The only supported user command is RESET. See ResourcesInfo/utils.h for string format. */

      void user(const daq::rc::UserCmd & cmd) override { process_user_command(cmd); }

      /** Read state of enabled/disabled resources from backup. */

      void read_backup();

    private:
      
      void release_config();
      void publish_info(uint64_t timestamp);
      void process_user_command(const daq::rc::UserCmd & cmd);
      uint64_t get_run_number_timestamp_from_is();
      void create_res_info_archiver_file(uint64_t timestamp, const daq::core::Partition& partition);
      void publish_res_info_in_IS() const;
      void IS_update_stopless_recovery_resources(uint64_t timestamp) const;
      void clear_stopless_recovery_resources(const daq::core::Partition& partition);

    private:

      std::string m_out_dir;
      bool m_archive2cool;
      bool m_backup_mode;
      uint64_t m_last_file_num;

      std::set<std::string> m_user_disabled;
      std::set<std::string> m_user_enabled;

      uint64_t m_last_timestamp;
      std::set<std::string> m_last_lb_disabled;
      std::set<std::string> m_last_lb_enabled;
      
    };
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace po = boost::program_options;


int
main(int argc, char **argv)
{
  try
    {
      IPCCore::init(argc, argv);
    }
  catch (ers::Issue &ex)
    {
      ers::warning(ers::Message(ERS_HERE, ex));
    }

  po::options_description desc("This program publishes in IS and stores enabled resources for the Resources Info archiver.");

  try
    {
      std::string outDir;
      uint64_t ts = 1;
      bool backupMode;

      desc.add_options()
        ("out-directory,o" , po::value<std::string>(&outDir)->required() , "Name of directory to store resources info")
        ("restore-from-backup,r" , "Restore state from backup")
        ("get-file,t", po::value<uint64_t>(&ts)->default_value(ts), "Produce resources info file with given timestamp for debug purposes and exit")
      ;

      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
      po::notify(vm);

      backupMode = vm.count("restore-from-backup");

      // Parser for the ItemCtrl
      daq::rc::CmdLineParser cmdParser(argc, argv, true);

      daq::ResourcesInfo::Provider *provider = new daq::ResourcesInfo::Provider(outDir, backupMode);

      if (vm.count("get-file"))
        {
          Configuration db(cmdParser.database());

          if (const daq::core::Partition *partition = daq::core::get_partition(db, cmdParser.partitionName()))
            {
              provider->do_configure(*partition);
              provider->do_publish_info(ts, *partition, true);
            }
          else
            {
              std::cerr << "ERROR: cannot find partition \"" << cmdParser.partitionName() << '\"' << std::endl;
              return EXIT_FAILURE;
            }

          return EXIT_SUCCESS;
        }

      daq::rc::ItemCtrl itemCtrl(cmdParser, std::shared_ptr<daq::rc::Controllable>(provider));

      // Can read from backup only when itemCtrl is built, since backup needs to know which IPC partition to use
      if (backupMode)
        provider->read_backup();

      itemCtrl.init();
      itemCtrl.run();
    }
  catch (daq::rc::CmdLineHelp &ex)
    {
      std::cout << desc << std::endl;
      std::cout << ex.message() << std::endl;
    }
  catch (ers::Issue &ex)
    {
      ers::fatal(ex);
      return EXIT_FAILURE;
    }
  catch (boost::program_options::error &ex)
    {
      ers::fatal(daq::rc::CmdLineError(ERS_HERE, ex.what(), ex));
      return EXIT_FAILURE;
    }
  catch (std::exception &ex)
    {
      ers::fatal(daq::ResourcesInfo::CommandLineError(ERS_HERE, ex.what()));
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  /** Resources Info Service configuration */

struct DetectorFolderInfo {

  DetectorFolderInfo(const daq::core::ResourcesInfoDetectorConfig * o) : m_config(o) {;}
  const daq::core::ResourcesInfoDetectorConfig * m_config;
  std::set<const daq::core::Segment *> m_segment_parents;
  std::set<const daq::core::ResourceSet *> m_resource_sets_parents;
  std::set<const daq::core::ResourceBase *> m_resources;

  std::map<std::string, std::set<const daq::core::ResourceBase *> > m_sub_folders;

  bool put_resource(const daq::core::ResourceBase * res, const std::vector<const daq::core::ResourcesInfoDetectorFolderConfig*>& config);
  const std::string& get_name() const;
  static DetectorFolderInfo& get_folder(eformat::SubDetector id, const daq::df::ROS * obj, const daq::df::InputChannel * ic_obj);
  static void clear();

};

static std::vector<DetectorFolderInfo> df_info;

void DetectorFolderInfo::clear()
{
  const unsigned int df_info_size(df_info.size());

  for(unsigned int i = 0; i < df_info_size; ++i) {
    DetectorFolderInfo& info = df_info[i];
    info.m_segment_parents.clear();
    info.m_resource_sets_parents.clear();
    info.m_resources.clear();
    info.m_sub_folders.clear();
  }
}

DetectorFolderInfo& DetectorFolderInfo::get_folder(eformat::SubDetector id, const daq::df::ROS * ros_obj, const daq::df::InputChannel * ic_obj)
{
  const unsigned int df_info_size2(df_info.size()-1);

  for(unsigned int i = 0; i < df_info_size2; ++i) {
    DetectorFolderInfo& info = df_info[i];
    for(auto & x : info.m_config->get_SubDetectorIDs())
      {
        if(id == x) return info;
      }
  }

  {
    std::ostringstream text;
    text << "the detector with id " << id << " linked with " << ros_obj;
    if(ic_obj) text << " using " << ic_obj;
    text << " is out of ranges defined by resources info configuration";
    ers::warning(daq::ResourcesInfo::ConfigError(ERS_HERE, text.str().c_str()));
  }

  return df_info[df_info_size2];
}

const std::string& DetectorFolderInfo::get_name() const
{
  return m_config->get_FolderName();
}

bool DetectorFolderInfo::put_resource(
  const daq::core::ResourceBase * res,
  const std::vector<const daq::core::ResourcesInfoDetectorFolderConfig*>& config
)
{
  for (auto & i : config)
    {
      for(auto & j : i->get_BaseClasses())
        {
          if(res->castable(j))
            {
              m_sub_folders[i->get_FolderName()].insert(res);
              return true;
            }
        }
    }

  return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void daq::ResourcesInfo::Provider::configure(const daq::rc::TransitionCmd & /*args*/)
{
  const daq::core::Partition &partition(daq::rc::OnlineServices::instance().getPartition());

  if (!m_backup_mode)
    clear_stopless_recovery_resources(partition);

  df_info.clear();

  do_configure(partition);
}


void daq::ResourcesInfo::Provider::do_configure(const daq::core::Partition& partition)
{
  try
    {
      if (const daq::core::ResourcesInfoConfig * ri_conf = partition.get_ResourcesInfoConfiguration())
        {
          m_archive2cool = ri_conf->get_ArchiveInCOOL();
          const std::vector<const daq::core::ResourcesInfoDetectorConfig*>& folders = ri_conf->get_DetectorFolders();
          df_info.reserve(folders.size()+1);
          std::ostringstream log_msg;
          log_msg << "ResourcesInfoConfiguration of " << &partition << " contains:\n";
          log_msg.setf(std::ios::hex, std::ios::basefield);
          log_msg.setf(std::ios::showbase);
          for (auto & x : folders)
            {
              DetectorFolderInfo& info = df_info.emplace_back(x);
              log_msg << " * folder " << info.get_name() << " for event format source identifiers in range ";

              bool first = true;
              for (auto & x : info.m_config->get_SubDetectorIDs())
                {
                  if (first)
                    first = false;
                  else
                    log_msg << ',';
                  log_msg << static_cast<int>(x);
                }

              log_msg << '\n';
            }

          auto& global_folder_info = df_info.emplace_back(ri_conf->get_GlobalFolder());
          log_msg << " * folder " << global_folder_info.get_name() << " for shared resources\n";
          ERS_LOG(log_msg.str());
        }
      else
        {
          ERS_LOG("ResourcesInfoConfiguration of " << partition << " is empty");
        }
    }
  catch(ers::Issue& ex)
    {
      daq::ResourcesInfo::ConfigError issue(ERS_HERE, "exception", ex);
      ers::error(issue);
      release_config();
      throw issue;
   }
  catch(std::exception& ex)
    {
      daq::ResourcesInfo::ConfigError issue(ERS_HERE, ex.what());
      ers::error(issue);
      release_config();
      throw issue;
    }
}

void daq::ResourcesInfo::Provider::release_config()
{
  df_info.clear();
  m_archive2cool = false;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  // add resource if it is ROS application

static void
add_ros_applications(const daq::core::ResourceBase *r, std::set<const daq::df::ROS*> &apps)
{
  if (const daq::df::ROS *ros = r->cast<daq::df::ROS>())
    {
      ERS_DEBUG(2, "add application " << ros << " (" << apps.size()+1 << ')');
      apps.insert(ros);
    }
}


  // process resources of resource set and add ROS applications

static void
add_ros_applications(const daq::core::ResourceSet *rs, std::set<const daq::df::ROS*> &apps)
{
  for (auto &i : rs->get_Contains())
    if (const daq::core::ResourceSet *rset = i->cast<daq::core::ResourceSet>())
      add_ros_applications(rset, apps);
    else
      add_ros_applications(i, apps);

  add_ros_applications(rs->cast<daq::core::ResourceBase>(), apps);
}


  // process resources of segment and add ROS applications

static void
add_ros_applications(const daq::core::Segment &seg, std::set<const daq::df::ROS*> &apps, std::ostringstream &warn)
{

  for (auto &i : seg.get_Segments())
    add_ros_applications(*i, apps, warn);

  for (auto &i : seg.get_Resources())
    if (const daq::core::ResourceSet *rs = i->cast<daq::core::ResourceSet>())
      add_ros_applications(rs, apps);
    else
      add_ros_applications(i, apps);

  for (auto &i : seg.get_Applications())
    if (const daq::core::ResourceBase *r = i->cast<daq::core::ResourceBase>())
      {
        warn << " * resource application " << i << " included by " << &seg << " via \"Applications\" relationship instead of \"Resources\" one\n";
        add_ros_applications(r, apps);
      }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  // get parents of segment (using partition path) and add to the DetectorFolder information

static void
get_parents(const daq::core::Segment *seg, DetectorFolderInfo &dfi, std::set<const daq::core::Component*> &valid_components,
            std::map<const daq::core::Component*, std::set<const daq::core::Component*> > &bad_parents)
{
  std::vector<const daq::core::Segment*> parents;

  seg->configuration().referenced_by(*seg, parents, "Segments");

  const daq::core::Segment *seg_parent(nullptr);

  for (const auto &p : parents)
    {
      const daq::core::Component *c(p->cast<daq::core::Component>());

      if (valid_components.find(c) == valid_components.end())
        {
          bad_parents[c].insert(seg->cast<daq::core::Component>());
        }
      else
        {
          ERS_DEBUG(2, "parent of " << seg << " is segment " << p);

          seg_parent = p;

          if (dfi.m_segment_parents.insert(p).second)
            get_parents(p, dfi, valid_components, bad_parents);
          else
            ERS_DEBUG(2, "parent " << p << " of " << seg << " was already processed");
        }
    }

  if (!seg_parent)
    ERS_DEBUG(2, "segment object " << seg << " has no segment parent");
}


  // get parents of resource set (using partition path) and add to the DetectorFolder information

static void
get_parents(const daq::core::ResourceSet *rs, DetectorFolderInfo &dfi, std::set<const daq::core::Component*> &valid_components,
            std::map<const daq::core::Component*, std::set<const daq::core::Component*> > &bad_parents)
{
  // find Resource Set parent

  const daq::core::ResourceSet *rs_parent(nullptr);

    {
      std::vector<const daq::core::ResourceSet*> parents;

      rs->configuration().referenced_by(*rs, parents, "Contains");

      for (const auto& p : parents)
        {
          const daq::core::Component *c(p->cast<daq::core::Component>());

          if (valid_components.find(c) == valid_components.end())
            {
              bad_parents[c].insert(rs->cast<daq::core::Component>());
            }
          else
            {
              ERS_DEBUG(2, "parent of " << rs << " is resource set " << p);

              rs_parent = p;

              if (dfi.m_resource_sets_parents.insert(p).second)
                get_parents(p, dfi, valid_components, bad_parents);
              else
                ERS_DEBUG(2, "parent " << p << " of " << rs << " was already processed");
            }
        }
    }

  // find Segment parent

  const daq::core::Segment *seg_parent(nullptr);

    {
      std::vector<const daq::core::Segment*> parents;

      rs->configuration().referenced_by(*rs, parents, "Resources");
      for (const auto& p : parents)
        {
          const daq::core::Component *c(p->cast<daq::core::Component>());

          if (valid_components.find(c) == valid_components.end())
            {
              ERS_DEBUG( 1, "parent " << p << " of " << rs << " is not referenced by partition, skip..." );
            }
          else
            {
              ERS_DEBUG(2, "add parent of " << rs << " => " << p);

              seg_parent = p;

              if (dfi.m_segment_parents.insert(p).second)
                get_parents(p, dfi, valid_components, bad_parents);
              else
                ERS_DEBUG(2, "parent " << p << " of " << rs << " was already processed");
            }
        }
    }

  if (!rs_parent && !seg_parent)
    {
      std::ostringstream text;
      text << "resource object " << rs << " has no parent";
      ers::error(daq::ResourcesInfo::ConfigError(ERS_HERE, text.str().c_str()));
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // return true, if DetectorFolder information contains parent of given resource set

static bool
find_parent(const daq::core::ResourceSet * rs, DetectorFolderInfo& dfi)
{
  std::vector<ConfigObject> parents;

  rs->config_object().referenced_by(parents, "Contains");

  for (auto &i : parents)
    if (const daq::core::ResourceSet *p = rs->configuration().get<daq::core::ResourceSet>(i.UID()))
      if (dfi.m_resource_sets_parents.find(p) != dfi.m_resource_sets_parents.end())
        {
          ERS_DEBUG(1, "found parent resource set of " << rs);
          return true;
        }

  rs->config_object().referenced_by(parents, "Resources");

  for (auto & i : parents)
    {
      if (const daq::core::Segment * p = rs->configuration().get<daq::core::Segment>(i.UID()))
        {
          if (dfi.m_segment_parents.find(p) != dfi.m_segment_parents.end())
            {
              ERS_DEBUG(1, "found parent segment of " << rs);
              return true;
            }
        }
    }

  ERS_DEBUG(1, "found no parent of " << rs);

  return false;
} 


  // return true, if DetectorFolder information contains parent of given segment

static bool
find_parent(const daq::core::Segment * seg, DetectorFolderInfo& dfi)
{
  std::vector<ConfigObject> parents;

  seg->config_object().referenced_by(parents, "Segments");

  for (auto & i : parents)
    {
      if (const daq::core::Segment * p = seg->configuration().get<daq::core::Segment>(i.UID()))
        {
          if (dfi.m_segment_parents.find(p) != dfi.m_segment_parents.end())
            {
              ERS_DEBUG(1, "found parent segment of " << seg);
              return true;
            }
        }
    }

  ERS_DEBUG(1, "found no parent of " << seg);

  return false;
} 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// add all resources of given resource (including nested if it is resource set)
static void
add_resources(const daq::core::ResourceBase *res, DetectorFolderInfo &info)
{
  info.m_resources.insert(res->cast<daq::core::ResourceBase>());

  if (const daq::core::ResourceSet *rs_obj = res->cast<daq::core::ResourceSet>())
    for (auto &i : rs_obj->get_Contains())
      add_resources(i, info);
}

// add all resources of given segment
static void
add_resources(const daq::core::Segment *seg, DetectorFolderInfo &info)
{
  for (auto &i : seg->get_Segments())
    add_resources(i, info);

  for (auto &i : seg->get_Resources())
    add_resources(i, info);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void
get_valid(const daq::core::ResourceBase *res, std::set<const daq::core::Component*> &valid_components)
{
  valid_components.insert(res->cast<daq::core::Component>());

  if (const daq::core::ResourceSet *rs = res->cast<daq::core::ResourceSet>())
    for (auto &i : rs->get_Contains())
      get_valid(i, valid_components);
}

static void
get_valid(const daq::core::Segment *seg, std::set<const daq::core::Component*> &valid_components, std::set<const daq::core::Segment*> &valid_segments)
{
  valid_segments.insert(seg);
  valid_components.insert(seg->cast<daq::core::Component>());

  for (auto &i : seg->get_Segments())
    get_valid(i, valid_components, valid_segments);

  for (auto &i : seg->get_Resources())
    get_valid(i, valid_components);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////

static void
str2obj(Configuration& db, const std::set<std::string>& in, std::set<const daq::core::Component *>& out)
{
  for (auto & i : in)
    {
      try
        {
          if (const daq::core::Component * c = db.get<daq::core::Component>(i))
            {
              out.insert(c);
            }
          else
            {
              std::ostringstream text;
              text << "failed to find component database object \'" << i << '\'';
              ers::error(daq::ResourcesInfo::ConfigError(ERS_HERE, text.str().c_str()));
            }
        }
      catch(ers::Issue & ex)
        {
          std::ostringstream text;
          text << "trying to get component \'" << i << "\' caught " << ex;
          ers::error(daq::ResourcesInfo::ConfigError(ERS_HERE, text.str().c_str()));
        }
    }
}


void
daq::ResourcesInfo::Provider::clear_stopless_recovery_resources(const daq::core::Partition &partition)
{
  m_user_disabled.clear();
  m_user_enabled.clear();

  std::set<const daq::core::Component*> empty;

  partition.set_disabled(empty);
  partition.set_enabled(empty);

  IS_update_stopless_recovery_resources(0);

  m_last_timestamp = 0;
  m_last_lb_disabled.clear();
  m_last_lb_enabled.clear();
}

void
daq::ResourcesInfo::Provider::publish_info(uint64_t timestamp)
{
  Provider::do_publish_info(timestamp, daq::rc::OnlineServices::instance().getPartition());
}

void
daq::ResourcesInfo::Provider::do_publish_info(uint64_t timestamp, const daq::core::Partition &partition, bool no_infrastrcuture)
{
  if (df_info.empty())
    {
      ERS_DEBUG(1, "there is no Resources Info configuration => nothing to publish");
      return;
    }


  DetectorFolderInfo::clear();

    // apply user disabled and enabled components

  if (!no_infrastrcuture)
    {
      if (timestamp == 0)
        {
          clear_stopless_recovery_resources(partition);
        }
      else
        {
          std::set<const daq::core::Component*> user_disabled;
          std::set<const daq::core::Component*> user_enabled;

          str2obj(partition.configuration(), m_user_disabled, user_disabled);
          str2obj(partition.configuration(), m_user_enabled, user_enabled);

          partition.set_disabled(user_disabled);
          partition.set_enabled(user_enabled);

          IS_update_stopless_recovery_resources(timestamp);
        }
    }



  //
  // get set of partition components and segments
  //
  // the components set is used to test and eliminate parents of resource sets
  // and segments, which may come from different partitions
  //

  std::set<const daq::core::Component*> valid_components;
  std::set<const daq::core::Segment*> valid_segments;

  for (auto &i : partition.get_Segments())
    get_valid(i, valid_components, valid_segments);


  // get ROS applications

  std::set<const daq::df::ROS*> ros_applications;

    {
      std::ostringstream warn;
      warn << std::endl;

      for (auto &i : partition.get_Segments())
        add_ros_applications(*i, ros_applications, warn);

      // only report warning (if any) at start of new run (i.e. timestamp == 0)

      if (timestamp == 0 && warn.str().size() > 10)
        ers::warning(daq::ResourcesInfo::ConfigError(ERS_HERE, warn.str().c_str()));
    }

    // find parents (segments and resource sets) of ROS applications

    {
      std::map<const daq::core::Component*, std::set<const daq::core::Component*> > bad_parents;

      for (auto &i : ros_applications)
        {
          static std::string bad_sid("UNKNOWN");
          const daq::df::InputChannel *good_input_channel = nullptr;
          eformat::SubDetector id((eformat::SubDetector) (i->get_Detector()->get_LogicalId()));
          eformat::helper::SourceIdentifier sid(id, 0);

          // if ROS is not linked with a valid detector, get value from first InputChannel
          if (id == 0 || sid.human_detector() == bad_sid)
            {
              for (auto &x : i->get_Contains())
                {
                  if (const daq::core::ResourceSet *rset = x->cast<daq::core::ResourceSet>())
                    {
                      for (const auto &o : rset->get_Contains())
                        {
                          if (const daq::df::InputChannel *input_channel = o->cast<daq::df::InputChannel>())
                            {
                              eformat::helper::SourceIdentifier rdc_sid(input_channel->get_Id());
                              unsigned int rdc_det_id = rdc_sid.subdetector_id();

                              if (rdc_det_id == 0)
                                {
                                  ERS_DEBUG(3, "skip " << input_channel << " (bad sub-detector ID)");
                                }
                              else
                                {
                                  id = static_cast<eformat::SubDetector>(rdc_det_id);
                                  good_input_channel = input_channel;
                                  break;
                                }
                            }
                          else
                            {
                              ERS_DEBUG(3, "skip " << o << " (not an input-channel)");
                            }
                        }

                      if (good_input_channel)
                        break;
                    }
                  else
                    {
                      ERS_DEBUG(3, "skip " << x << " (not a resource set)");
                    }
                }
            }

          if (id == 0)
            {
              std::ostringstream text;
              text << "skip object " << i << " referencing detector " << i->get_Detector() << " with invalid logical id = " << std::showbase << std::hex
                  << (uint32_t) i->get_Detector()->get_LogicalId() << " and not including any InputChannel with valid sub-detector ID";
              ers::error(daq::ResourcesInfo::ConfigError(ERS_HERE, text.str().c_str()));
            }
          else
            {
              get_parents(i->cast<daq::core::ResourceSet>(), DetectorFolderInfo::get_folder(id, i, good_input_channel), valid_components, bad_parents);
            }
        }

      // only report warning (if there are bad parents) at start of new run (i.e. timestamp == 0)

      if (timestamp == 0 && !bad_parents.empty())
        {
          std::ostringstream bad_parents_warn;

          bad_parents_warn << "ignore " << bad_parents.size() << " component(s) not referenced by partition, but including partition segments and/or resources:\n";

          for (auto &i : bad_parents)
            {
              bad_parents_warn << " * " << i.first << " includes " << i.second.size() << " component(s): ";
              for (std::set<const daq::core::Component*>::const_iterator j = i.second.begin(); j != i.second.end(); ++j)
                {
                  if (j != i.second.begin())
                    bad_parents_warn << ", ";
                  bad_parents_warn << *j;
                }
              bad_parents_warn << std::endl;
            }

          ers::warning(daq::ResourcesInfo::ConfigError(ERS_HERE, bad_parents_warn.str().c_str()));
        }
    }

  // remove parents referencing different ROSes from different folders

  const unsigned int df_info_size(df_info.size());
  const unsigned int df_info_size2(df_info_size - 1);

  for (unsigned int i = 0; i < df_info_size; ++i)
    {
      DetectorFolderInfo &info = df_info[i];

      for (std::set<const daq::core::Segment*>::iterator j = info.m_segment_parents.begin(); j != info.m_segment_parents.end();)
        {
          bool is_shared(false);
          for (unsigned int i2 = 0; i2 < df_info_size2; ++i2)
            {
              if (i2 != i)
                {
                  DetectorFolderInfo &info2 = df_info[i2];
                  std::set<const daq::core::Segment*>::iterator j2 = info2.m_segment_parents.find(*j);
                  if (j2 != info2.m_segment_parents.end())
                    {
                      ERS_DEBUG(0, *j << " is shared by \"" << info.get_name() << "\" and \"" << info2.get_name() << "\" folders");
                      info2.m_segment_parents.erase(j2);
                      is_shared = true;
                    }
                }
            }

          if (is_shared)
            {
              df_info[df_info_size2].m_segment_parents.insert(*j);
              info.m_segment_parents.erase(j++);
            }
          else
            {
              ++j;
            }
        }

      for (std::set<const daq::core::ResourceSet*>::iterator j = info.m_resource_sets_parents.begin(); j != info.m_resource_sets_parents.end();)
        {
          bool is_shared(false);
          for (unsigned int i2 = 0; i2 < df_info_size2; ++i2)
            {
              if (i2 != i)
                {
                  DetectorFolderInfo &info2 = df_info[i2];
                  std::set<const daq::core::ResourceSet*>::iterator j2 = info2.m_resource_sets_parents.find(*j);
                  if (j2 != info2.m_resource_sets_parents.end())
                    {
                      ERS_DEBUG(0, *j << " is shared by \"" << info.get_name() << "\" and \"" << info2.get_name() << "\" folders");
                      info2.m_resource_sets_parents.erase(j2);
                      is_shared = true;
                    }
                }
            }

          if (is_shared)
            {
              df_info[df_info_size2].m_resource_sets_parents.insert(*j);
              info.m_resource_sets_parents.erase(j++);
            }
          else
            {
              ++j;
            }
        }
    }


  // remove nested parents (e.g. a segment or a resource set references top-level segment)

  for (unsigned int i = 0; i < df_info_size; ++i)
    {
      DetectorFolderInfo &info = df_info[i];

      std::list<const daq::core::ResourceSet*> rs2removed;
      std::list<const daq::core::Segment*> seg2removed;

      for (const auto &j : info.m_resource_sets_parents)
        if (find_parent(j, info))
          rs2removed.push_back(j);

      for (const auto &j : info.m_segment_parents)
        if (find_parent(j, info))
          seg2removed.push_back(j);

      for (const auto &j : rs2removed)
        info.m_resource_sets_parents.erase(j);

      for (const auto &j : seg2removed)
        info.m_segment_parents.erase(j);
    }

  // add all resources of top-level segments and resource sets

  for (unsigned int i = 0; i < df_info_size; ++i)
    {
      DetectorFolderInfo &info = df_info[i];

      for (auto &j : info.m_segment_parents)
        add_resources(j, info);

      for (auto &j : info.m_resource_sets_parents)
        add_resources(j, info);
    }

  std::cout << "Top-level segments [and/or resource sets] per detectors:\n";

  for (unsigned int i = 0; i < df_info_size; ++i)
    {
      const DetectorFolderInfo &info = df_info[i];

      std::cout << " - detector folder \"" << info.get_name() << "\" contains " << info.m_resources.size() << " resources from:" << std::endl;

      for (const auto& j : info.m_segment_parents)
        std::cout << "    * " << j << std::endl;

      for (const auto& j : info.m_resource_sets_parents)
        std::cout << "    # " << j << std::endl;
    }

  // range resources by detector sub-folders

  for (unsigned int i = 0; i < df_info_size2; ++i)
    {
      DetectorFolderInfo &info = df_info[i];

      for (const auto &j : info.m_resources)
        if (!j->disabled(partition))
          if (!info.put_resource(j, partition.get_ResourcesInfoConfiguration()->get_DefaultDetectorSubFolders()))
            info.put_resource(j, info.m_config->get_SubFolders());
    }

  // special processing for GLOBAL folder is required (skip resources put into detector folders)

    {
      DetectorFolderInfo &info = df_info[df_info_size2];

      for (std::set<const daq::core::ResourceBase*>::const_iterator j = info.m_resources.begin(); j != info.m_resources.end(); ++j)
        {
          if (!(*j)->disabled(partition))
            {
              bool found(false);
              for (unsigned int i2 = 0; i2 < df_info_size2; ++i2)
                {
                  DetectorFolderInfo &info2 = df_info[i2];
                  if (info2.m_resources.find(*j) != info2.m_resources.end())
                    {
                      found = true;
                      ERS_DEBUG(1, "skip resource " << *j << " assigned to " << info.get_name() << " folder, since it is found in " << info2.get_name() << " folder");
                      break;
                    }
                }

              if (found)
                continue;

              if (!info.put_resource(*j, partition.get_ResourcesInfoConfiguration()->get_DefaultDetectorSubFolders()))
                info.put_resource(*j, info.m_config->get_SubFolders());
            }
        }
    }

  // create file for res_info_archiver

  if (m_archive2cool || no_infrastrcuture)
    {
      if (timestamp == 0)
        timestamp = get_run_number_timestamp_from_is();

      create_res_info_archiver_file(timestamp, partition);
    }
  else
    {
      ERS_LOG("\"ArchiveInCOOL@ResourcesInfoConfiguration\" is false, do not create file for res_info_archiver");
    }

  // publish in IS

  if (!no_infrastrcuture)
    publish_res_info_in_IS();
}

void
daq::ResourcesInfo::Provider::create_res_info_archiver_file(uint64_t num, const daq::core::Partition &partition)
{
  std::string file, tfile;

    {
      if (num <= m_last_file_num)
        {
          ers::warning(daq::ResourcesInfo::TimestampWasUsed(ERS_HERE, num));
          num = m_last_file_num + 1;
        }

      m_last_file_num = num;

      std::ostringstream text, text2;
      text << m_out_dir << '/' << num;
      file = text.str();
      text2 << m_out_dir << "/." << num;
      tfile = text2.str();
    }

  long file_len = 0;

  try
    {
      std::ofstream f(tfile.c_str());
      f.exceptions(std::ostream::eofbit | std::ostream::failbit | std::ostream::badbit);

      if (!f)
        {
          throw std::runtime_error("failed to open file in write mode");
        }

      f << num << std::endl << partition.UID() << std::endl;

      const unsigned int df_info_size(df_info.size());

      for (unsigned int i = 0; i < df_info_size; ++i)
        {
          const DetectorFolderInfo &info = df_info[i];
          if (!info.m_sub_folders.empty())
            {
              f << '#' << info.get_name() << std::endl; // detector folder
              for (const auto& j : info.m_sub_folders)
                {
                  f << '$' << j.first << std::endl; // detector sub-folder
                  for (const auto& x : j.second)
                    f << '-' << x->UID() << '@' << x->class_name() << std::endl; // sub-folder channel
                }
            }
        }

      file_len = f.tellp();
      f.close();
    }
  catch (std::exception &ex)
    {
      const std::string ex_text(daq::ResourcesInfo::ex2text(ex));
      daq::ResourcesInfo::FileSystemError issue(ERS_HERE, "write", "file", tfile.c_str(), ex_text.c_str());
      ers::fatal(issue);
      throw issue;
    }

  try
    {
      std::ifstream f(tfile.c_str());

      if (!f)
        throw std::runtime_error("failed to open file in read mode");

      f.seekg(0, std::ios::end);
      long written_len = static_cast<std::streamoff>(f.tellg());

      if (written_len != file_len)
        {
          unlink(tfile.c_str());
          std::ostringstream text;
          text << "write error: " << written_len << " bytes have been written instead of " << file_len;
          throw std::runtime_error(text.str().c_str());
        }
    }
  catch (std::exception &ex)
    {
      const std::string ex_text(daq::ResourcesInfo::ex2text(ex));
      daq::ResourcesInfo::FileSystemError issue(ERS_HERE, "read", "file", tfile.c_str(), ex_text.c_str());
      ers::fatal(issue);
      throw issue;
    }

  if (int code = rename(tfile.c_str(), file.c_str()))
    {
      std::ostringstream text;
      text << "cannot rename file \'" << tfile << "\' to \'" << file << "\': rename() failed with code " << code << ", reason = \'" << strerror(errno) << '\'';
      throw std::runtime_error(text.str().c_str());
    }

  ERS_LOG("created file \'" << file << "\' (" << file_len << " bytes) for res_info_archiver\n");
}

void
daq::ResourcesInfo::Provider::publish_res_info_in_IS() const
{
  ISInfoDictionary dict(daq::rc::OnlineServices::instance().getIPCPartition());

  // get existing data from IS server

  std::set<std::string> is_info;

  try
    {
      ISInfoIterator it(daq::rc::OnlineServices::instance().getIPCPartition(), "Resources", EnabledResource::type());

      ERS_DEBUG(2, "found " << it.entries() << " objects");

      while (it())
        {
          is_info.insert(it.name());
        }
    }
  catch (ers::Issue &ex)
    {
      ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot read information from IS server", ex));
    }

  // iterate resources and publish missing resources

  try
    {
      const unsigned int df_info_size(df_info.size());

      for (unsigned int i = 0; i < df_info_size; ++i)
        {
          const DetectorFolderInfo &info = df_info[i];
          if (!info.m_sub_folders.empty())
            {
              for (const auto& j : info.m_sub_folders)
                {
                  const std::string folder(daq::ResourcesInfo::s_enabled_resoures + info.get_name() + '/' + j.first + '/');

                  for (const auto& x : j.second)
                    {
                      const std::string info_name(folder + x->UID());

                      std::set<std::string>::iterator i = is_info.find(info_name);

                      if (i == is_info.end())
                        {
                          ERS_DEBUG(2, "information about resources " << info_name << " was not published in IS");

                          EnabledResource obj;
                          obj.Class = x->class_name();

                          dict.checkin(info_name, obj);

                          ERS_DEBUG(1, "insert \"" << info_name << "\" => \"" << x->class_name() << "\"");
                        }
                      else
                        {
                          ERS_DEBUG(2, "information about resources " << info_name << " was published in IS");
                          is_info.erase(i);
                        }
                    }
                }
            }
        }
    }
  catch (std::exception &ex)
    {
      ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot store information on IS server", ex));
    }

  // remove disabled resources

  try
    {
      for (std::set<std::string>::iterator i = is_info.begin(); i != is_info.end(); ++i)
        {
          if ((*i).find(daq::ResourcesInfo::s_enabled_resoures) == 0)
            {
              ERS_LOG("removing info " << *i << " ...");
              dict.remove(*i);
            }
        }
    }
  catch (std::exception &ex)
    {
      ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot remove information from IS server", ex));
    }
}


void
daq::ResourcesInfo::Provider::read_backup()
{

  ISInfoDictionary dict(daq::rc::OnlineServices::instance().getIPCPartition());

  // read from IS resources disabled and enabled by stopless recovery

  try
    {
      ISInfoIterator it(daq::rc::OnlineServices::instance().getIPCPartition(), "Resources", InfoResource::type());

      ERS_DEBUG(2, "found " << it.entries() << " objects");

      while (it())
        {
          daq::ResourcesInfo::add_stopless_recovery_name(daq::ResourcesInfo::s_stopless_recovery_disabled_resoures,
                                                         daq::ResourcesInfo::s_stopless_recovery_disabled_resoures.size(), it.name(), m_user_disabled);
          daq::ResourcesInfo::add_stopless_recovery_name(daq::ResourcesInfo::s_stopless_recovery_enabled_resoures, daq::ResourcesInfo::s_stopless_recovery_enabled_resoures.size(),
                                                         it.name(), m_user_enabled);
        }
    }
  catch (ers::Issue &ex)
    {
      throw daq::ResourcesInfo::ISError(ERS_HERE, "cannot read information from \"Resources\" IS server", ex);
    }

  // read state of run:
  //
  // 1. if RootController is in RUNNING state
  //    then do not publish information when go to prepareForRun state first time
  // 2. else assume application died not in running state and
  //    then publish information when go to prepareForRun state
  //

  RCStateInfo stateInfo;

  try
    {
      dict.findValue("RunCtrl.RootController", stateInfo);
      ERS_DEBUG(1, "RootController is in \"" << stateInfo.state << "\" state");
    }
  catch (ers::Issue &ex)
    {
      throw daq::ResourcesInfo::ISError(ERS_HERE, "cannot read information from \"RunCtrl\" IS server", ex);
    }

  m_backup_mode = false;
  if (stateInfo.state == "RUNNING")
    {
      ERS_LOG("provider was running (RootController is in \"" << stateInfo.state << "\" state)");
      m_backup_mode = true;
    }
}


static void
publish_missing_info(ISInfoDictionary &dict, const std::set<std::string> &s_user, const std::set<std::string> &s_is, const std::string &prefix, uint64_t timestamp)
{
  for (const auto& i : s_user)
    {
      const std::string info_name(prefix + i);
      if (s_is.find(info_name) == s_is.end())
        {
          try
            {
              ERS_LOG("check in IS stopless recovery resource " << info_name << " ...");
              InfoResource obj;
              obj.Timestamp = timestamp;
              dict.checkin(info_name, obj);
            }
          catch (std::exception &ex)
            {
              ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot publish information on \"Resources\" IS server", ex));
            }
        }
    }
}

static void
remove_obsolete_info(ISInfoDictionary &dict, const std::set<std::string> &s_user, const std::set<std::string> &s_is, std::string::size_type prefix_len)
{
  for (const auto& i : s_is)
    {
      const std::string name(i, prefix_len);
      if (s_user.find(name) == s_user.end())
        {
          try
            {
              ERS_LOG("removing from IS stopless recovery resource " << i << " ...");
              dict.remove(i);
            }
          catch (std::exception &ex)
            {
              ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot remove information from \"Resources\" IS server", ex));
            }
        }
    }
}

void
daq::ResourcesInfo::Provider::IS_update_stopless_recovery_resources(uint64_t timestamp) const
{
  ISInfoDictionary dict(daq::rc::OnlineServices::instance().getIPCPartition());

  // read what we have in IS

  std::set<std::string> SLR_enabled;
  std::set<std::string> SLR_disabled;

  try
    {
      ISInfoIterator it(daq::rc::OnlineServices::instance().getIPCPartition(), "Resources", InfoResource::type());

      ERS_DEBUG(2, "found " << it.entries() << " objects");

      while (it())
        {
          daq::ResourcesInfo::add_stopless_recovery_name(daq::ResourcesInfo::s_stopless_recovery_disabled_resoures, 0, it.name(), SLR_disabled);
          daq::ResourcesInfo::add_stopless_recovery_name(daq::ResourcesInfo::s_stopless_recovery_enabled_resoures, 0, it.name(), SLR_enabled);
        }
    }
  catch (ers::Issue &ex)
    {
      ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot read information from \"Resources\" IS server", ex));
    }

  // publish what is not in IS

  publish_missing_info(dict, m_user_disabled, SLR_disabled, daq::ResourcesInfo::s_stopless_recovery_disabled_resoures, timestamp);
  publish_missing_info(dict, m_user_enabled, SLR_enabled, daq::ResourcesInfo::s_stopless_recovery_enabled_resoures, timestamp);

  remove_obsolete_info(dict, m_user_disabled, SLR_disabled, daq::ResourcesInfo::s_stopless_recovery_disabled_resoures.size());
  remove_obsolete_info(dict, m_user_enabled, SLR_enabled, daq::ResourcesInfo::s_stopless_recovery_enabled_resoures.size());

  // sign the finish of updates to allow disabled() algorithm read IS information

  try
    {
      ERS_LOG("checkin " << daq::ResourcesInfo::s_stopless_recovery_updated_resoures << " ...");
      UpdateResourceInfo obj;
      obj.Timestamp = timestamp;
      dict.checkin(daq::ResourcesInfo::s_stopless_recovery_updated_resoures, obj);
    }
  catch (std::exception &ex)
    {
      ers::error(daq::ResourcesInfo::ISError(ERS_HERE, "cannot publish information on \"Resources\" IS server", ex));
    }
}

static bool
find_new(const std::set<std::string> &s_old, const std::set<std::string> &s_new)
{
  for (std::set<std::string>::const_iterator i = s_new.begin(); i != s_new.end(); ++i)
    {
      if (s_old.find(*i) == s_old.end())
        return true;
    }

  return false;
}

static bool
find_old(const std::set<std::string> &s_old, const std::set<std::string> &s_new)
{
  for (std::set<std::string>::const_iterator i = s_new.begin(); i != s_new.end(); ++i)
    {
      if (s_old.find(*i) != s_old.end())
        return true;
    }

  return false;
}

static void
put_new(std::set<std::string> &s_old, const std::set<std::string> &s_new, const char *name)
{
  for (std::set<std::string>::const_iterator i = s_new.begin(); i != s_new.end(); ++i)
    {
      s_old.insert(*i);
      ERS_LOG("add \"" << *i << "\" to user-" << name);
    }
}

static void
remove_old(std::set<std::string> &s_old, const std::set<std::string> &s_new, const char *name)
{
  for (std::set<std::string>::const_iterator i = s_new.begin(); i != s_new.end(); ++i)
    {
      if (s_old.erase(*i) != 0)
        {
          ERS_LOG("remove \"" << *i << "\" from user-" << name);
        }
    }
}

static void
validate_user_names(Configuration &db, std::set<std::string> &names, const std::string &args)
{
  std::set<std::string>::iterator i = names.begin();
  while (i != names.end())
    {
      try
        {
          if (db.get<daq::core::Component>(*i) == 0)
            {
              std::ostringstream text;
              text << "failed to find component database object \'" << *i << "\'  in user command RESET:\n" << args;
              ers::error(daq::ResourcesInfo::CannotProcessUserCommand(ERS_HERE, text.str().c_str()));
              names.erase(i++);
            }
          else
            {
              ++i;
            }
        }
      catch (ers::Issue &ex)
        {
          std::ostringstream text;
          text << "trying to get component database object \'" << *i << "\' caught " << ex;
          ers::error(daq::ResourcesInfo::ConfigError(ERS_HERE, text.str().c_str()));
        }
    }
}

//static void report_not_allowed_error(const char * why, const std::string& args)
//{
//  std::ostringstream text;
//  text << why << ", the user command RESET is not allowed in such state.\nThe command was:\n" << args;
//  ers::error(daq::ResourcesInfo::CannotProcessUserCommand(ERS_HERE, text.str().c_str()));
//}

static std::string
set2str(const std::set<std::string> &val)
{
  std::string s;
  for (std::set<std::string>::const_iterator i = val.begin(); i != val.end(); ++i)
    {
      if (i != val.begin())
        s.append(", ");
      s.append(*i);
    }
  return s;
}

void
daq::ResourcesInfo::Provider::process_user_command(const daq::rc::UserCmd &cmd)
{

  std::string args;
  for (const auto &a : cmd.commandParameters())
    {
      args.append(a);
      args.append(" ");
    }
  if (cmd.commandName() == "RESET")
    {
      uint64_t timestamp;
      std::set<std::string> user_disabled, user_enabled;

      // FIXME
//    if(!m_run_number) {
//      report_not_allowed_error("the application is not in the running state", args);
//      return;
//    }

      // decode user command

      try
        {
          daq::ResourcesInfo::decode_user_cmd(args, timestamp, user_disabled, user_enabled);
        }
      catch (std::exception &ex)
        {
          std::ostringstream text;
          text << "failed to parse arguments of user command RESET:\n" << ex.what();
          throw daq::ResourcesInfo::CannotProcessUserCommand(ERS_HERE, text.str().c_str());
        }

      if (timestamp == 0)
        {
          std::ostringstream text;
          text << "timestamp cannot be 0 in user command RESET:\n" << args;
          throw daq::ResourcesInfo::CannotProcessUserCommand(ERS_HERE, text.str().c_str());
        }

      // validate existence of resources in user input

      validate_user_names(daq::rc::OnlineServices::instance().getConfiguration(), user_disabled, args);
      validate_user_names(daq::rc::OnlineServices::instance().getConfiguration(), user_enabled, args);

      if (user_disabled.empty() && user_enabled.empty())
        {
          ERS_LOG("ignore empty user command RESET");
          return;
        }

      // check if there are no re-enabled or re-disabled resources in the same timestamp

      std::set<std::string> bad_re_enabled, bad_re_disabled;  // store re-set resources to be thrown as exception at the end

      if (timestamp == m_last_timestamp)
        {
          std::set<std::string>::const_iterator it;

          while ((it = std::find_first_of(user_disabled.begin(), user_disabled.end(), m_last_lb_enabled.begin(), m_last_lb_enabled.end())) != user_disabled.end())
            {
              bad_re_disabled.insert(*it);
              user_disabled.erase(it);
            }

          while ((it = std::find_first_of(user_enabled.begin(), user_enabled.end(), m_last_lb_disabled.begin(), m_last_lb_disabled.end())) != user_enabled.end())
            {
              bad_re_enabled.insert(*it);
              user_enabled.erase(it);
            }

          m_last_lb_disabled.insert(user_disabled.begin(), user_disabled.end());
          m_last_lb_enabled.insert(user_enabled.begin(), user_enabled.end());
        }
      else
        {
          m_last_timestamp = timestamp;
          m_last_lb_disabled = user_disabled;
          m_last_lb_enabled = user_enabled;
        }

      // check that there is something new disabled or enabled

      if (find_new(m_user_disabled, user_disabled) || find_new(m_user_enabled, user_enabled) || find_old(m_user_disabled, user_enabled) || find_old(m_user_enabled, user_disabled))
        {
          put_new(m_user_disabled, user_disabled, "disabled");
          put_new(m_user_enabled, user_enabled, "enabled");
          remove_old(m_user_disabled, user_enabled, "disabled");
          remove_old(m_user_enabled, user_disabled, "enabled");
          publish_info(timestamp);
        }
      else if (bad_re_enabled.empty() && bad_re_disabled.empty())
        {
          std::ostringstream text;
          text << "there are no newly disabled or enabled components in user command RESET:\n" << args;
          ers::error(daq::ResourcesInfo::CannotProcessUserCommand(ERS_HERE, text.str().c_str()));
        }

      if (!bad_re_enabled.empty() || !bad_re_disabled.empty())
        {
          throw daq::ResourcesInfo::TimestampResourcesReset(ERS_HERE, timestamp, ::set2str(bad_re_enabled), ::set2str(bad_re_disabled));
        }
    }
  else
    {
      ERS_LOG("user command \'" << cmd.commandName() << "\' is not supported");
    }
}

uint64_t
daq::ResourcesInfo::Provider::get_run_number_timestamp_from_is()
{
  ISInfoDictionary isInfoDict(daq::rc::OnlineServices::instance().getIPCPartition());

  RunParams runParameter;

  try
    {
      isInfoDict.findValue("RunParams.RunParams", runParameter);
      uint64_t ts = runParameter.timeSOR.total_mksec_utc() * 1000;
      ERS_DEBUG(1, "Read from Information Service (partition \'" << daq::rc::OnlineServices::instance().getIPCPartition().name() << "\') run number timestamp " << ts);
      return ts;
    }
  catch (daq::is::Exception &ex)
    {
      daq::ResourcesInfo::NoRunNumber issue(ERS_HERE, daq::rc::OnlineServices::instance().getIPCPartition().name().c_str(), ex);
      ers::error(issue);
      throw issue; // raise error
    }
}
