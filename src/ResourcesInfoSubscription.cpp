#include <boost/thread/mutex.hpp>

#include <is/infodictionary.h>
#include <is/inforeceiver.h>
#include <is/infoiterator.h>
#include <is/infoT.h>

#include <dal/Component.h>
#include <dal/Partition.h>
#include <dal/disabled-components.h>

#include <ResourcesInfo/utils.h>

#include "ResourcesInfoUtils.h"
#include "InfoResource.h"


////////////////////////////////////////////////////////////////////////////////////////////////////

typedef std::map<uint32_t,daq::ResourcesInfo::notify_disabled_changes > SubscriptionMap;

static SubscriptionMap s_subscriptions;
static boost::mutex s_mutex;
static std::string s_partition_name;
static Configuration * s_db(0);
static const daq::core::Partition * s_partition(0);
static ISInfoReceiver * s_info_receiver(0);

////////////////////////////////////////////////////////////////////////////////////////////////////

static void callback(ISCallbackInfo * isc)
{
  boost::mutex::scoped_lock scoped_lock(s_mutex);

  ISInfoT<uint32_t> isi;
  isc->value(isi);
  ERS_DEBUG( 0 , "CALLBACK:: " << isc->name() << " reason code: " << isc->reason() << " new value: " << isi);

    // read information from IS and set user-disabled resources

  try {
    IPCPartition p(s_partition_name);

    ISInfoIterator it( p, "Resources", InfoResource::type() );

    ERS_DEBUG( 2 , "found " << it.entries() << " objects");

    std::set<std::string> SLR_enabled;
    std::set<std::string> SLR_disabled;

    while(it()) {
      daq::ResourcesInfo::add_stopless_recovery_name(daq::ResourcesInfo::s_stopless_recovery_disabled_resoures, daq::ResourcesInfo::s_stopless_recovery_disabled_resoures.size(), it.name(), SLR_disabled);
      daq::ResourcesInfo::add_stopless_recovery_name(daq::ResourcesInfo::s_stopless_recovery_enabled_resoures, daq::ResourcesInfo::s_stopless_recovery_disabled_resoures.size(), it.name(), SLR_enabled);
    }

    ERS_DEBUG( 2 ,
      "daq::core::get_num_of_slr_resources() = " << daq::core::DisabledComponents::get_num_of_slr_resources(*s_partition) << "\n"
      "SLR_enabled.empty() = " << SLR_enabled.empty() << "\n"
      "SLR_disabled.empty() = " << SLR_disabled.empty()
    );

    if(daq::core::DisabledComponents::get_num_of_slr_resources(*s_partition) == 0 && SLR_enabled.empty() && SLR_disabled.empty()) {
      ERS_DEBUG( 0 , "there are no any changes in resources disabling, skip this IS callback" );
      return;
    }


    std::set<const daq::core::Component *> user_disabled;
    std::set<const daq::core::Component *> user_enabled;

    for(std::set<std::string>::const_iterator i = SLR_disabled.begin(); i != SLR_disabled.end(); ++i) {
      try {
        if(const daq::core::Component * c = s_db->get<daq::core::Component>(*i)) {
          user_disabled.insert(c);
        }
      }
      catch(ers::Issue & ex) {
        std::ostringstream s;
        s << "caught exception trying to get component \'" << *i << '\'';
        ers::error(daq::ResourcesInfo::SubscriptionError(ERS_HERE, "notify", s.str().c_str(), ex));
      }
    }

    for(std::set<std::string>::const_iterator i = SLR_enabled.begin(); i != SLR_enabled.end(); ++i) {
      try {
        if(const daq::core::Component * c = s_db->get<daq::core::Component>(*i)) {
          user_enabled.insert(c);
        }
      }
      catch(ers::Issue & ex) {
        std::ostringstream s;
        s << "caught exception trying to get component \'" << *i << '\'';
        ers::error(daq::ResourcesInfo::SubscriptionError(ERS_HERE, "notify", s.str().c_str(), ex));
      }
    }

    s_partition->set_disabled(user_disabled);
    s_partition->set_enabled(user_enabled);
  }
  catch(ers::Issue & ex) {
    ers::error(daq::ResourcesInfo::SubscriptionError(ERS_HERE, "notify", "caught exception", ex));
  }


    // notify subscribers

  for(SubscriptionMap::const_iterator i = s_subscriptions.begin(); i != s_subscriptions.end(); ++i) {

    ERS_DEBUG( 0 , "invoke callback " << i->first);

    try {
      (i->second)();
    }
    catch(ers::Issue& ex) {
      std::ostringstream s;
      s << "caught exception from subscriber " << i->first;
      ers::error(daq::ResourcesInfo::SubscriptionError(ERS_HERE, "notify", s.str().c_str(), ex));
    }
    catch(std::exception& ex) {
      std::ostringstream s;
      s << "caught std::exception from subscriber " << i->first << ": " << ex.what();
      ers::error(daq::ResourcesInfo::SubscriptionError(ERS_HERE, "notify", s.str().c_str()));
    }
    catch(...) {
      std::ostringstream s;
      s << "caught unknown exception from subscriber " << i->first;
      ers::error(daq::ResourcesInfo::SubscriptionError(ERS_HERE, "notify", s.str().c_str()));
    }

  }
}

uint32_t daq::ResourcesInfo::subscribe(const std::string& partition_name, Configuration& db, const daq::core::Partition& p, const notify_disabled_changes &user_cb)
{
  boost::mutex::scoped_lock scoped_lock(s_mutex);

  ERS_DEBUG( 0, "user parameters: IPC partition is \"" << partition_name << "\", partition object is " << &p );

  if(!s_partition_name.empty()) {
    if(partition_name != s_partition_name) {
      std::ostringstream s;
      s << "cannot subscribe on resources changes in partition \"" << partition_name << "\" since there is already a subscription on partition \"" << s_partition_name << '\"';
      throw daq::ResourcesInfo::SubscriptionError(ERS_HERE, "subscribe", s.str().c_str());
    }
  }
  else {
    s_partition_name = partition_name;
    s_db = &db;
    s_partition = &p;
  }

  if(s_subscriptions.empty()) {
    ERS_LOG( "subscribe IS changes \"" << daq::ResourcesInfo::s_stopless_recovery_updated_resoures << "\" in partition \"" << s_partition_name << '\"' );

    try {
      IPCPartition pt(s_partition_name);
      s_info_receiver = new ISInfoReceiver(pt);
      s_info_receiver->subscribe( daq::ResourcesInfo::s_stopless_recovery_updated_resoures, callback );
    }
    catch(ers::Issue& ex) {
      throw daq::ResourcesInfo::SubscriptionError(ERS_HERE, "subscribe", "ISInfoReceiver::subscribe() has failed", ex);
    }
  }

  static uint32_t s_count(0);

  s_count++;
  s_subscriptions[s_count] = user_cb;

  ERS_DEBUG( 0, "subscription count is " << s_count );

  return s_count;
}

void daq::ResourcesInfo::unsubscribe(uint32_t h)
{
  boost::mutex::scoped_lock scoped_lock(s_mutex);

  ERS_DEBUG( 0, "user parameter: handler is " << h );

  SubscriptionMap::iterator i = s_subscriptions.find(h);

  if(i == s_subscriptions.end()) {
    std::ostringstream s;
    s << "there is no subscription " << h;
    throw daq::ResourcesInfo::SubscriptionError(ERS_HERE, "unsubscribe", s.str().c_str());
  }

  s_subscriptions.erase(i);

  if(s_subscriptions.empty()) {
    ERS_LOG("unsubscribe IS changes \"" << daq::ResourcesInfo::s_stopless_recovery_updated_resoures << '\"');

    try {
      s_info_receiver->unsubscribe( daq::ResourcesInfo::s_stopless_recovery_updated_resoures );
      delete s_info_receiver;
      s_info_receiver = 0;
    }
    catch(ers::Issue& ex) {
      throw daq::ResourcesInfo::SubscriptionError(ERS_HERE, "unsubscribe", "ISInfoReceiver::unsubscribe() failed", ex);
    }

    s_partition->set_disabled(std::set<const daq::core::Component *>());
    s_partition->set_enabled(std::set<const daq::core::Component *>());

    s_db = 0;
    s_partition = 0;
    s_partition_name.clear();
  }
}
