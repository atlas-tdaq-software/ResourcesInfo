#ifndef RESOURCE_INFO_UTILS_H
#define RESOURCE_INFO_UTILS_H

#include <stdint.h>

#include <set>
#include <sstream>
#include <string>

#include <boost/function/function0.hpp>

#include <config/Configuration.h>
#include <dal/Partition.h>

#include <ResourcesInfo/errors.h>


namespace daq {

  namespace ResourcesInfo {

      /**
       *  The user notification callback function that
       *  is invoked in case of changes in disabled resources
       *  caused by Run Control stopless recovery.
       */

    typedef boost::function0<void> notify_disabled_changes;


      /**
       *  \brief Subscribe on dynamic resources disabling and enabling notification.
       *
       *  The method is used to make a subscription and notify user about dynamic changes
       *  of disabled components (performed by Run Control stopless recovery).
       *
       *  When resources are disabled or enabled, the daq::core::Partition::set_disabled() method for
       *  dynamically disabled components and daq::core::Partition::set_enabled() method for dynamically
       *  enabled components are invoked on partition object, so daq::core::Component::disabled() method
       *  will take into account dynamic changes of components disabling. After that the registered user
       *  callbacks are invoked.
       *
       *  \par Implementation remarks
       *
       *  - several subscriptions have to share the same partition name;
       *  - the partition name and object can only be changed, when there is no any subscription left.
       *
       *  \par Parameters
       *
       *  \param partition_name  IPC partition name
       *  \param db              configuration database object
       *  \param p               partition object
       *  \param user_cb         user-defined callback function
       *
       *  \return Return \b callback subscription handler (has to be used in unsubscribe() method)
       *
       *  \throw daq::ResourcesInfo::Exception exception in case of an error.
       */

    uint32_t
    subscribe(const std::string& partition_name, Configuration& db, const daq::core::Partition& p, const notify_disabled_changes &user_cb);


      /**
       *  \brief Remove dynamic resources disabling and enabling subscription.
       *
       *  Remove callback function previously added by the subscribe() method.
       *  Stop notification on dynamic resources disabling and enabling, when no more subscriptions left.
       *
       *  \param cb_handler  callback handler returned by subscribe()
       *
       *  \throw daq::ResourcesInfo::SubscriptionError exception in case of an error.
       */

    void
    unsubscribe(uint32_t cb_handler);


      /** Helper function used by user RESET command to encode sequence to stream */

    void set2str(const std::set<std::string>& val, std::ostringstream& s);

      /** Helper function used by user RESET command to decode sequence to stream */

    void
    str2set(std::set<std::string>& val, std::istringstream& s, const char * name);

      /**
       *  \brief Encode ResourcesInfoProvider RESET command argument.
       *
       *  Note, new line is used as field separator in encoded string.
       *
       *  \param in_timestamp   input timestamp (UTC nanoseconds since 1970)
       *  \param in_disabled    set of resources to be disabled
       *  \param in_enabled     set of resources to be enabled
       *
       *  \return               the encoded string
       */

    std::string
    encode_user_cmd(uint64_t in_timestamp, const std::set<std::string>& in_disabled, const std::set<std::string>& in_enabled) noexcept;

      /**
       *  \brief Decode ResourcesInfoProvider RESET command argument.
       *
       *  Note, new line is used as field separator in encoded string.
       *
       *  \param in             string to decode
       *  \param out_timestamp  output timestamp (UTC nanoseconds since 1970)
       *  \param out_disabled   output set of resources to be disabled
       *  \param out_enabled    output set of resources to be enabled
       *
       *  \throw std::exception in case of an error.
       */

    void
    decode_user_cmd(const std::string& in, uint64_t& out_timestamp, std::set<std::string>& out_disabled, std::set<std::string>& out_enabled);
    
  }

}


inline void
daq::ResourcesInfo::set2str(const std::set<std::string>& val, std::ostringstream& s)
{
  s << val.size() << std::endl;
  for (auto& i : val)
    {
      s << i << std::endl;
    }
}

inline void
daq::ResourcesInfo::str2set(std::set<std::string>& val, std::istringstream& s, const char * name)
{
  size_t num;

  try
    {
      s >> num;
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "cannot read " << name << " sequence length number: " << ex.what();
      throw std::runtime_error(text.str().c_str());
    }

  for (size_t i = 0; i < num; ++i)
    {
      try
        {
          std::string str;
          s >> str;
          val.insert(str);
        }
      catch (std::exception& ex)
        {
          std::ostringstream text;
          text << "cannot read string " << num << " of " << name << " sequence: " << ex.what();
          throw std::runtime_error(text.str().c_str());
        }
    }
}

inline std::string
daq::ResourcesInfo::encode_user_cmd(uint64_t in_timestamp, const std::set<std::string>& in_disabled, const std::set<std::string>& in_enabled) noexcept
{
  std::ostringstream s;
  s << in_timestamp << std::endl;
  set2str(in_disabled, s);
  set2str(in_enabled, s);
  return s.str();
}

inline void
daq::ResourcesInfo::decode_user_cmd(const std::string& in, uint64_t& out_timestamp, std::set<std::string>& out_disabled, std::set<std::string>& out_enabled)
{
  try
    {
      std::istringstream s(in);
      s.exceptions(std::istream::eofbit | std::istream::failbit | std::istream::badbit);

      try
        {
          s >> out_timestamp;
        }
      catch (std::exception& ex)
        {
          std::ostringstream text;
          text << "cannot read timestamp: " << ex.what();
          throw std::runtime_error(text.str().c_str());
        }

      str2set(out_disabled, s, "disabled");
      str2set(out_enabled, s, "enabled");
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "failed to parse user command \'" << in << "\':\n" << ex.what();
      throw std::runtime_error(text.str().c_str());
    }
}

#endif
