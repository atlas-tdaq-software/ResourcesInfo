#ifndef RESOURCES_INFO_EXCEPTION_H_INCLUDED
#define RESOURCES_INFO_EXCEPTION_H_INCLUDED

#include <stdint.h>
#include <ers/ers.h>

namespace daq {

  ERS_DECLARE_ISSUE(
    ResourcesInfo,
    Exception,
    ,
  )

  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    TimeZoneDBError,
    Exception,
    "Time-zone db access failed",
    ,
  )


  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    CommandLineError,
    Exception,
    "Failed to parse command line: " << message,
    ,
    ((const char *)message)
  )


  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    MissingEnvironment,
    Exception,
    "Environment variable " << name << " is not set",
    ,
    ((const char *)name)
  )


  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    ConfigError,
    Exception,
    "Configuration problem: " << message,
    ,
    ((const char *)message)
  )


  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    ISError,
    Exception,
    "Information Service problem: " << message,
    ,
    ((const char *)message)
  )


  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    CoolError,
    Exception,
    "COOL database problem: " << message,
    ,
    ((const char *)message)
  )


  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    TimestampWasUsed,
    Exception,
    "The timestamp " << id << " was already used in previous user request(s)",
    ,
    ((uint64_t)id)
  )


  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    TimestampResourcesReset,
    Exception,
    "Resources reset in timestamp " << ts << "\n"
    "re-enabled resources are: \'" << re_enabled_resources << "\'\n"
    "re-disabled resources are: \'" << re_disabled_resources << '\'',
    ,
    ((uint64_t)ts)
    ((std::string)re_enabled_resources)
    ((std::string)re_disabled_resources)
  )


  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    FileSystemError,
    Exception,
    "Failed to " << op << ' ' << what << " \"" << name << "\": " << message,
    ,
    ((const char *)op)
    ((const char *)what)
    ((const char *)name)
    ((const char *)message)
  )


  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    DirectoryLockError,
    Exception,
    "Failed to " << op << " directory \"" << name << "\": " << message,
    ,
    ((const char *)op)
    ((std::string)name)
    ((std::string)message)
  )

  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    RemoveObsoleteLock,
    Exception,
    "remove obsolete lock \'" << path << "\' created by: " << whom,
    ,
    ((std::string)path)
    ((std::string)whom)
  )

  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    CannotUnlockFile,
    Exception,
    "cannot unlock file \'" << path << '\'',
    ,
    ((std::string)path)
  )

  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    CannotRemoveLockFile,
    Exception,
    "cannot remove file \'" << path << "\', unlink failed with code " << code << ": " << why,
    ,
    ((std::string)path)
    ((int)code)
    ((const char *)why)
  )

  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    NoRunNumber,
    Exception,
    "Failed to read run number from Information Service in partition \"" << partition_name << '\"',
    ,
    ((const char*)partition_name)
  )


  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    CannotProcessUserCommand,
    Exception,
    "Cannot process USER command: " << reason,
    ,
    ((const char*)reason)
  )


  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    BadRunNumber,
    Exception,
    "The run number " << number << " published in partition \"" << partition_name << "\" is bad: " << why,
    ,
    ((const char*)partition_name)
    ((uint32_t)number)
    ((const char*)why)
  )


  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    ApplicationError,
    Exception,
    "The application got exception: " << message,
    ,
    ((const char*)message)
  )

  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    BadFileName,
    Exception,
    "Cannot parse filename \'" << file << "\': " << message,
    ,
    ((const char*)file)
    ((const char*)message)
  )

  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    CannotRemoveFile,
    Exception,
    "Cannot remove file \'" << file << "\': " << message,
    ,
    ((const char*)file)
    ((const char*)message)
  )

  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    BadFile,
    Exception,
    "File \'" << file << "\' is bad: " << message,
    ,
    ((const char*)file)
    ((const char*)message)
  )

  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    FileArchivationFailed,
    Exception,
    "Cannot archive file \'" << file << "\' for timestamp " << ts << " created in partition " << partition << " : " << message,
    ,
    ((const char*)file)
    ((uint64_t)ts)
    ((const char*)partition)
    ((const char*)message)
  )

  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    SubscriptionError,
    Exception,
    "Cannot " << op << ": " << message,
    ,
    ((const char *)op)
    ((const char *)message)
  )

  ERS_DECLARE_ISSUE_BASE(
    ResourcesInfo,
    MaxInclusionReached,
    Exception,
    "Reach maximum allowed inclusion (" << limit << ") during calculation of resource dependencies; possibly there is circular dependency between these objects: " << objects,
    ,
    ((unsigned int)limit)
    ((std::string)objects)
  )
}

#endif
