#!/bin/sh

TNS_ADMIN=/afs/cern.ch/project/oracle/admin
export TNS_ADMIN

# setup tdaq release

source /afs/cern.ch/atlas/project/tdaq/cmt/bin/cmtsetup.sh $1 >/dev/null || (echo "Failed to setup $1 DAQ/HLT-I release"; exit 1)

TDAQ_ERS_DEBUG_LEVEL=-1
export TDAQ_ERS_DEBUG_LEVEL

TDAQ_ERS_LOG="null"
export TDAQ_ERS_LOG

TDAQ_ERS_FATAL="lstderr"
TDAQ_ERS_ERROR="lstderr"
TDAQ_ERS_WARNING="lstderr"

export TDAQ_ERS_FATAL
export TDAQ_ERS_ERROR
export TDAQ_ERS_WARNING

# get table

shift


err_file="/tmp/res_info.$$"
rm -f $err_file

res_info_db_reader  -g -i 'oracle://ATLR;schema=ATLAS_COOLONL_TDAQ;dbname=CONDBR2' $* 2>$err_file #| grep -v '__Warning:'
code=$?

if [ $code -ne 0 ]
then
  echo "<b>res_info_db_reader -g -i 'oracle://ATLR;schema=ATLAS_COOLONL_TDAQ;dbname=CONDBR2' $* failed: $?</b>"
  cat $err_file
fi

rm -f $err_file

exit $code
