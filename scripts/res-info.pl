#!/usr/bin/perl

  use CGI qw/:standard :html3/;

#  $tdaq_release='tdaq-04-00-01';
  $tdaq_release='tdaq-05-05-00';

  @start = times;

 ##################################################################################################################

   # set table config cookies

  %table_details_c = cookie('ResourcesInfo:table_details');

  @TABLE_DETAILS = ('updated in color','updated only','show run number and lumi block','force full lumi-block and run-number DBs scan');    # checkbox_group(-name=>'details')

  if(param('details')) {
    foreach (@TABLE_DETAILS) { $table_details_c{$_} = 'no'; }
    @tbl_d=param('details');
    for $i ( 0 .. $#tbl_d ) { $d=$tbl_d[$i]; $table_details_c{$d}='yes'; }
  }
  foreach (@TABLE_DETAILS) { $table_details_c{$_} = $table_details_c{$_} || 'yes'; }

  $tz_name = param('tmz') || cookie('ResourcesInfo:time-zone') || 'UTC';

  push @COOKIES, cookie(-name=>'ResourcesInfo:table_details', -value=>\%table_details_c, -expires=>'+180d');
  push @COOKIES, cookie(-name=>'ResourcesInfo:time-zone',     -value=>"$tz_name",        -expires=>'+180d');

 ##################################################################################################################

  @TIME_ZONE_NAMES = ('UTC');
  push @TIME_ZONE_NAMES, 'CERN';

  @result = `./rn_table.sh $tdaq_release foo bar -z list-regions`;
  $status=$? >> 8;

  if($status != 0) {
    for $i ( 0 .. $#result ) {
      print span({-style=>'color: red;'},$result[$i],"<br>");
    }
    return 0;
  }

  for $i ( 0 .. $#result ) {
    $z=$result[$i];
    $z =~ s/^\s*(.*?)\s*$/$1/;
    push @TIME_ZONE_NAMES,"$z";
  }

 ##################################################################################################################

  print header(-cookie=>\@COOKIES),
        start_html( -title=>'ATLAS Enabled Resources Info'),
	start_form,
        h1('ATLAS Enabled Resources');


#  print "DEBUG:<p> ", Dump;


  my %_PARTITIONS;
  my %_DETECTORS;
  my %_FOLDERS;

  @DETECTORS;
  @FOLDERS;
  @PARTITIONS;
  

    # get information about array of folders in the following format:
    # "PARTITION,DETECTOR,F1,F2,...,FN"

  @INFO = ();

  if(param('hi')) {
    @INFO=param('hi');
    @PARTITIONS=param('hp');
    @DETECTORS=param('hd');
    @FOLDERS=param('hf');
  }
  else {
    @result = `./res_info_table.sh $tdaq_release -l`;
    $status=$? >> 8;

    if($status != 0) {
      for $i ( 0 .. $#result ) { print span({-style=>'color: red;'},$result[$i],"<br>"), end_form; }
      return 0;
    }

    $p='';
    $d='';
    $f='';

    for $i ( 0 .. $#result ) {
      $result[$i] =~ s/^[\t\n\r\f]*(.*?)\s*$/$1/;

      if($result[$i] =~ /^    .*/) {
        $result[$i] =~ s/^[ ]*(.*?)/$1/;
        $f="$f,$result[$i]";
        $_FOLDERS{ $result[$i] } = '';
        next;
      }
      elsif($result[$i] =~ /^  .*/) {
        $result[$i] =~ s/^[ ]*(.*?)/$1/;
        $_DETECTORS{$result[$i]} = '';
        $d=$result[$i];
        if( $f ne '' ) { push @INFO, $f; }
        $f="$p,$d";
        next;
      }
      else {
        if( $f ne '' ) { push @INFO, $f; }
        $_PARTITIONS{ $result[$i] } = '';
        $p="$result[$i]";
        $d='';
      }
    }

    if( $f ne '' ) { push @INFO, $f; }

    @DETECTORS = ('*');
    @FOLDERS = ('*');
    @PARTITIONS = ();

    push @PARTITIONS, sort keys (%_PARTITIONS); 
    push @DETECTORS, sort keys (%_DETECTORS); 
    push @FOLDERS, sort keys (%_FOLDERS); 
  }

  print hidden(-name=>'hi', -default=> [ @INFO ]);
  print hidden(-name=>'hp', -default=> [ @PARTITIONS ]);
  print hidden(-name=>'hd', -default=> [ @DETECTORS ]);
  print hidden(-name=>'hf', -default=> [ @FOLDERS ]);

  @tabel_details_selected = ();
  foreach (@TABLE_DETAILS) {
    if($table_details_c{$_} eq 'yes') { push @tabel_details_selected, "$_"; }
  }

  print 'Partitions: ', popup_menu(-name=>'partitions', -values=> [ @PARTITIONS ] ), '&nbsp;&nbsp;&nbsp;',
        'Detectors: ', popup_menu(-name=>'detectors', -values=> [ @DETECTORS ], -default=>'*' ), '&nbsp;&nbsp;&nbsp;',
        'Folders: ', popup_menu(-name=>'folders', -values=> [ @FOLDERS ], -default=>'*' ), p,
        'Channels mask: ', textfield('channels','',10), " <i>(reqular expression)</i>", p,
        'IoV since: ', textfield('from','',6,9), textfield('from_lb','',2,9), " until: ", textfield('till','',6,9), textfield('till_lb','',2,9), " <i>(run numbers [luminosity blocks])</i>&nbsp;&nbsp;&nbsp;", p, 
        'Since ', textfield('since_ts',$past,19,20), ' until ', textfield('until_ts','',19,20), " $tz_name$tz_pfx",
        '<br>&nbsp;&nbsp;<font size=-1><i>(leave empty to be ignored or use <a href="http://www.iso.org/iso/iso8601">YYYY-MM-DD HH:MM:SS</a> date-time format to provide a value)</i></font>', p;

  print
    h3('User preferences'), p,
        'Show details: ', checkbox_group(-name=>'details',
                   -values=>[@TABLE_DETAILS],
                   -defaults=>[@tabel_details_selected]), p,
        'Select timezone: ', popup_menu(-name=>'tmz', -values=>\@TIME_ZONE_NAMES, -default=>"$tz_name" ), p;

  if(!param('btn1')) {
    print submit('btn1','Run Query'), end_form, hr;
    &sign();
    return 0;
  }
  
  print submit('btn1','Run Query'), p;
  
  $ap = ''; $x=param('partitions'); if("$x" ne "*") { $ap = "-p $x"; }
  $ad = ''; $x=param('detectors'); if("$x" ne "*") { $ad = "-d $x"; }
  $af = ''; $x=param('folders'); if("$x" ne "*") { $af = "-f $x"; }
  $ac = ''; $x=param('channels'); if("$x" ne "") { $x =~ s/^\s*(.*?)\s*$/$1/; if("$x" ne "") { $ac = "-n \"$x\""; } }
  $ar = ''; $x=param('from'); if("$x" ne "") { $x =~ s/^\s*(.*?)\s*$/$1/; if("$x" ne "") { $ar = "-r \"$x\""; } }
  $ab = ''; $x=param('till'); if("$x" ne "") { $x =~ s/^\s*(.*?)\s*$/$1/; if("$x" ne "") { $ab = "-R \"$x\""; } }
  $aR = ''; $x=param('from_lb'); if("$x" ne "") { $x =~ s/^\s*(.*?)\s*$/$1/; if("$x" ne "") { $aR = "-b \"$x\""; } }
  $aB = ''; $x=param('till_lb'); if("$x" ne "") { $x =~ s/^\s*(.*?)\s*$/$1/; if("$x" ne "") { $aB = "-B \"$x\""; } }
  $as = ''; $x=param('since_ts'); if("$x" ne "") { $x =~ s/^\s*(.*?)\s*$/$1/; if("$x" ne "") { $x =~ s/ /T/g; $as = "-s \"$x\""; } }
  $at = ''; $x=param('until_ts'); if("$x" ne "") { $x =~ s/^\s*(.*?)\s*$/$1/; if("$x" ne "") { $x =~ s/ /T/g; $at = "-t \"$x\""; } }

  $al = ''; if ($table_details_c{'updated in color'} eq "yes") { $al="-C html"; }
  $au = ''; if ($table_details_c{'updated only'} eq "yes") { $au='-u'; }
  $ak = ''; if ($table_details_c{'show run number and lumi block'} eq "yes") { $ak='-U'; }
  $aF = ''; if ($table_details_c{'force full lumi-block and run-number DBs scan'} eq "yes") { $aF='-F'; }
  
  $aZ = '';
  if("$tz_name" eq "" || "$tz_name" eq "UTC") { $aZ = ''; }
  elsif("$tz_name" eq "CERN")                 { $aZ = '-z Europe/Zurich'; }
  else                                        { $aZ = "-z $tz_name"; }


  @result = `./res_info_table.sh $tdaq_release -A $ap $ad $af $ac $ar $aR $ab $aB $as $at $al $au $ak $aF $aZ`;
  $status=$? >> 8;

  if($status != 0) {
    for $i ( 0 .. $#result ) { print span({-style=>'color: red;'},$result[$i],"<br>"), end_form; }
    return 0;
  }

  if($#result <= 4) {
    print span({-style=>'color: darkblue;'},"<i>No data found. Change selection parameters.</i>","<br>")
  }
  else {
    my $sz=$#result-1; # all but last row
  
    my $table_title_bg_color='#99CCFF';

    my $bg_color1='#DADAFF';
    my $bg_color2='#E3E3FF';

    my $bg_color3='#E0E0E0';
    my $bg_color4='#F0F0F0';

    print "\n<table cellpadding=\"2\" cellspacing=\"1\">\n",
          " <tr>\n";

    @tt = split(/\|/,$result[1]);

    my $w=$#tt-1; # table width

    for $j ( 1 .. $w ) {
      $tt[$j] =~ s/^\s*(.*?)\s*$/$1/;
      print "  <th nowrap bgcolor=\"$table_title_bg_color\">$tt[$j]</th>\n",
    }

    print " </tr>\n";

    my $last_p='';
    my $last_d='';
    my $last_f='';
    my $last_c='';

    my $bg1=$bg_color1;
    my $bg2=$bg_color2;
    
    my $bg=$bg1;

    for $i ( 3 .. $sz ) {
      $x=$result[$i];
      if ($x =~ /^=.*/) { next; }
      if ($x =~ /^\+.*/) { next; }
      @row = split(/\|/,$x);
      
      if("$row[1]" ne "$last_p" || "$row[2]" ne "$last_d" || "$row[3]" ne "$last_f") {
        $last_p=$row[1];
        $last_d=$row[2];
        $last_f=$row[3];
        if("$bg1" eq "$bg_color1") {
          $bg1=$bg_color3;
          $bg2=$bg_color4;
        }
        else {
          $bg1=$bg_color1;
          $bg2=$bg_color2;
        }
      }

      if("$row[4]" ne "$last_c") {
        $last_c = $row[4];
        if("$bg" eq "$bg1") {
          $bg=$bg2;
        }
        else {
          $bg=$bg1;
        }
      }

      print " <tr>\n";
      for $j ( 1 .. $w ) {
        $y=$row[$j];
        $a=''; if ($j == 5) { $a = ' align="right"'; }
        print "  <td bgcolor=\"$bg\"$a>$y</td>\n";
      }
      print "</tr>\n";
    }

    print "</table>\n", p, span({-style=>'color: darkblue;'},"$result[$#result]","<br>");
  }

  print end_form, hr;

  &sign();


sub sign {
  $now_string = localtime;
  @end = times;

  printf "<i><font size=-1>Generated automatically at $now_string in %.2f CPU seconds. ", $end[0] - $start[0] + $end[2] - $start[2],
        "Report bugs to <a href=\"http://consult.cern.ch/xwho/people/432778\">author</a>.",
        "</font></i></body></html>\n";
}
