package test_res_info;

public class TestCmdEncoder
  {

    private static int read_list(java.util.LinkedList<String> l, String args[], int idx)
      {
        idx++;

        for (int i = idx; i < args.length; i++)
          {
            if (args[i].charAt(0) != '-')
              {
                l.add(args[i]);
              }
            else
              {
                return (i - 1);
              }
          }

        return (args.length - 1);
      }

    static private void usage()
      {
        System.out.println("Usage: java ... TestCmdEncoder -t [ --timestamp] arg     Action Timestamp");
        System.out.println("                               -d [ --disable-ids ] arg  IDs of objects to be disabled");
        System.out.println("                               -e [ --enable-ids ] arg   IDs of objects to be enabled");
        System.out.println("                               -h [ --help ]             Print help message");
      }

    public static void main(String args[])
      {
        java.util.LinkedList<String> disable = new java.util.LinkedList<String>();
        java.util.LinkedList<String> enable = new java.util.LinkedList<String>();
        long timestamp = 0;

        for (int i = 0; i < args.length; i++)
          {
            if (args[i].equals("-d") || args[i].equals("--disable-ids"))
              {
                i = read_list(disable, args, i);
              }
            else if (args[i].equals("-e") || args[i].equals("--enable-ids"))
              {
                i = read_list(enable, args, i);
              }
            else if (args[i].equals("-t") || args[i].equals("--timestamp"))
              {
                timestamp = java.lang.Long.valueOf(args[++i]).longValue();
              }
            else if (args[i].equals("-h") || args[i].equals("--help"))
              {
                usage();
                System.exit(0);
              }
            else
              {
                System.err.println("ERROR [TestCmdEncoder.main()]: unexpected parameter \'" + args[i] + "\'\n");
                System.exit(1);
              }
          }
        
        if(timestamp == 0)
          {
            System.err.println("ERROR [TestCmdEncoder.main()]: timestamp may not be 0");
            System.exit(1);
          }
        
        if(disable.size() == 0 && enable.size() == 0)
          {
            System.err.println("ERROR [TestCmdEncoder.main()]: disable and enable parameters may not be empty simultaneously");
            System.exit(1);
          }
        
        System.out.print(ResourcesInfo.Utils.encode_user_cmd(timestamp, disable.toArray(new String[0]), enable.toArray(new String[0])));

        System.exit(0);
      }
}
